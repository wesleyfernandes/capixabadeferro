<?php
/**
 * File Type: Events
 */
if( ! class_exists('TG_CoreBase') ) {
	
	class TG_CoreBase {
		
		protected static $instance = null;
		 
		public function __construct() {
			 add_action('save_post', array($this, 'tg_save_meta_data'));	
		}
		
		
		/**
		 * Returns the *Singleton* instance of this class.
		 *
		 * @return Singleton The *Singleton* instance.
		 */
        public static function getInstance() {
            if (is_null(self::$instance)) {
                self::$instance = new self();
            }
            return self::$instance;
        }
		
		/**
		 * Save Meta options
		 *
		 * @return 
		 */
        public function tg_save_meta_data($post_id='') {
            
			if (!is_admin()) { return;}
			
			if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) { return;}
			
			if ( get_post_type() == 'tg_events' ) {
				if (!function_exists('fw_get_db_post_option')) { return;}
				
				if (isset($_POST['fw_options'])) {
					foreach ($_POST['fw_options'] as $key => $value) {
						 if( $key == 'start_date') {
							 $value	= date('Y-m-d H:i:s',strtotime( $value ));
						 } else if( $key == 'end_date') {
							 $value	= date('Y-m-d H:i:s',strtotime( $value ));
						 }
						 
						 update_post_meta($post_id, $key, $value); //exit;
					}
				}
			}
        }

	}
	
  	new TG_CoreBase();	
}