<?php
/**
 * File Type: Videos
 */
if( ! class_exists('TG_Videos') ) {
	
	class TG_Videos {
	
		public function __construct() {
			global $pagenow;
			add_action('init', array(&$this, 'init_videos'));
			add_action( 'init', array(&$this,'init_video_taxonomies') );				
		}
		
		/**
		 * @Init Post Type
		 * @return {post}
		 */
		public function init_videos(){
			$this->prepare_post_type();
		}
		
		/**
		 * @Prepare Post Type
		 * @return {}
		 */
		public function prepare_post_type(){
			$labels = array(
				'name' 				 => esc_html__( 'Videos', 'themographics_core' ),
				'all_items'			 => esc_html__( 'Videos', 'themographics_core' ),
				'singular_name'      => esc_html__( 'Videos', 'themographics_core' ),
				'add_new'            => esc_html__( 'Add Video', 'themographics_core' ),
				'add_new_item'       => esc_html__( 'Add New Video', 'themographics_core' ),
				'edit'               => esc_html__( 'Edit', 'themographics_core' ),
				'edit_item'          => esc_html__( 'Edit Video', 'themographics_core' ),
				'new_item'           => esc_html__( 'New Video', 'themographics_core' ),
				'view'               => esc_html__( 'View Video', 'themographics_core' ),
				'view_item'          => esc_html__( 'View Video', 'themographics_core' ),
				'search_items'       => esc_html__( 'Search Video', 'themographics_core' ),
				'not_found'          => esc_html__( 'No Video found', 'themographics_core' ),
				'not_found_in_trash' => esc_html__( 'No Video found in trash', 'themographics_core' ),
				'parent'             => esc_html__( 'Parent Video', 'themographics_core' ),
			);
			$args = array(
				'labels'			  => $labels,
				'description'         => esc_html__( 'This is where you can add new Video', 'themographics_core' ),
				'public'              => true,
				'supports'            => array( 'title', 'thumbnail', 'editor' ),
				'show_ui'             => true,
				'capability_type'     => 'post',
				'map_meta_cap'        => true,
				'publicly_queryable'  => true,
				'exclude_from_search' => false,
				'hierarchical'        => false,
				'menu_position' 	  => 9,
				'rewrite'			  => array('slug' => 'tg_videos', 'with_front' => true),
				'query_var'           => false,
				'has_archive'         => 'false',
			); 
			register_post_type( 'videos' , $args );
			
		}
		
		/**
		 * @Prepare Video Categories
		 * @return {}
		 */
		public function init_video_taxonomies() {
			$labels = array(
				'name'              => _x( 'Categories', 'taxonomy general name', 'themographics_core' ),
				'singular_name'     => _x( 'Category', 'taxonomy singular name' , 'themographics_core'),
				'search_items'      => esc_html__( 'Search Categories' , 'themographics_core'),
				'all_items'         => esc_html__( 'All Categories' , 'themographics_core'),
				'parent_item'       => esc_html__( 'Parent Category' , 'themographics_core'),
				'parent_item_colon' => esc_html__( 'Parent Category:' , 'themographics_core'),
				'edit_item'         => esc_html__( 'Edit Category' , 'themographics_core'),
				'update_item'       => esc_html__( 'Update Category' , 'themographics_core'),
				'add_new_item'      => esc_html__( 'Add New Category' , 'themographics_core'),
				'new_item_name'     => esc_html__( 'New Category Name', 'themographics_core' ),
				'menu_name'         => esc_html__( 'Categories', 'themographics_core' ),
			);
		
			$args = array(
				'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
				'labels'            => $labels,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categories' ),
			);
		
			register_taxonomy( 'video_categories', array( 'videos' ), $args );
		}
		
		
	}
	
  	new TG_Videos();	
}