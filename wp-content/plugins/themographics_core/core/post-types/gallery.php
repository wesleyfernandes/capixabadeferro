<?php
/**
 * File Type: Gallery
 */
if( ! class_exists('TG_Gallery') ) {
	
	class TG_Gallery {
	
		public function __construct() {
			global $pagenow;
			add_action('init', array(&$this, 'init_gallery'));
			add_action( 'init', array(&$this,'init_gallery_taxonomies') );				
		}
		
		/**
		 * @Init Post Type
		 * @return {post}
		 */
		public function init_gallery(){
			$this->prepare_post_type();
		}
		
		/**
		 * @Prepare Post Type
		 * @return {}
		 */
		public function prepare_post_type(){
			$labels = array(
				'name' 				 => esc_html__( 'Gallery', 'themographics_core' ),
				'all_items'			 => esc_html__( 'Gallery Images', 'themographics_core' ),
				'singular_name'      => esc_html__( 'Gallery Image', 'themographics_core' ),
				'add_new'            => esc_html__( 'Add Gallery Image', 'themographics_core' ),
				'add_new_item'       => esc_html__( 'Add New Gallery Image', 'themographics_core' ),
				'edit'               => esc_html__( 'Edit', 'themographics_core' ),
				'edit_item'          => esc_html__( 'Edit Gallery Image', 'themographics_core' ),
				'new_item'           => esc_html__( 'New Gallery Image', 'themographics_core' ),
				'view'               => esc_html__( 'View Gallery Image', 'themographics_core' ),
				'view_item'          => esc_html__( 'View Gallery Image', 'themographics_core' ),
				'search_items'       => esc_html__( 'Search Gallery Image', 'themographics_core' ),
				'not_found'          => esc_html__( 'No Gallery Image found', 'themographics_core' ),
				'not_found_in_trash' => esc_html__( 'No Gallery Image found in trash', 'themographics_core' ),
				'parent'             => esc_html__( 'Parent Gallery Image', 'themographics_core' ),
			);
			$args = array(
				'labels'			  => $labels,
				'description'         => esc_html__( 'This is where you can add new Gallery', 'themographics_core' ),
				'public'              => true,
				'supports'            => array( 'title', 'thumbnail' ),
				'show_ui'             => true,
				'capability_type'     => 'post',
				'map_meta_cap'        => true,
				'publicly_queryable'  => true,
				'exclude_from_search' => false,
				'hierarchical'        => false,
				'menu_position' 	  => 10,
				'rewrite'			  => array('slug' => 'tg_gallery', 'with_front' => true),
				'query_var'           => false,
				'has_archive'         => 'false',
			); 
			register_post_type( 'tg_gallery' , $args );
			
		}
		
		/**
		 * @Prepare Gallery Categories
		 * @return {}
		 */
		public function init_gallery_taxonomies() {
			$labels = array(
				'name'              => _x( 'Categories', 'taxonomy general name', 'themographics_core' ),
				'singular_name'     => _x( 'Category', 'taxonomy singular name' , 'themographics_core'),
				'search_items'      => esc_html__( 'Search Categories' , 'themographics_core'),
				'all_items'         => esc_html__( 'All Categories' , 'themographics_core'),
				'parent_item'       => esc_html__( 'Parent Category' , 'themographics_core'),
				'parent_item_colon' => esc_html__( 'Parent Category:' , 'themographics_core'),
				'edit_item'         => esc_html__( 'Edit Category' , 'themographics_core'),
				'update_item'       => esc_html__( 'Update Category' , 'themographics_core'),
				'add_new_item'      => esc_html__( 'Add New Category' , 'themographics_core'),
				'new_item_name'     => esc_html__( 'New Category Name', 'themographics_core' ),
				'menu_name'         => esc_html__( 'Categories', 'themographics_core' ),
			);
		
			$args = array(
				'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
				'labels'            => $labels,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categories' ),
			);
		
			register_taxonomy( 'gallery_categories', array( 'tg_gallery' ), $args );
		}
		
		
	}
	
  	new TG_Gallery();	
}