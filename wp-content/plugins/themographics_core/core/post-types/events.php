<?php
/**
 * File Type: Events
 */
if( ! class_exists('TG_Events') ) {
	
	class TG_Events {
	
		public function __construct() {
			global $pagenow;
			add_action('init', array(&$this, 'init_events'));
			add_action( 'init', array(&$this,'init_event_taxonomies') );
			add_filter('manage_events_posts_columns', array(&$this, 'events_columns_add'));
			add_action('manage_events_posts_custom_column', array(&$this, 'events_columns'),10, 2);						
		}
		
		/**
		 * @Init Post Type
		 * @return {post}
		 */
		public function init_events(){
			$this->prepare_post_type();
		}
		
		/**
		 * @Prepare Post Type
		 * @return {}
		 */
		public function prepare_post_type(){
			$labels = array(
				'name' 				 => esc_html__( 'Events', 'themographics_core' ),
				'all_items'			 => esc_html__( 'Events', 'themographics_core' ),
				'singular_name'      => esc_html__( 'Events', 'themographics_core' ),
				'add_new'            => esc_html__( 'Add Event', 'themographics_core' ),
				'add_new_item'       => esc_html__( 'Add New Event', 'themographics_core' ),
				'edit'               => esc_html__( 'Edit', 'themographics_core' ),
				'edit_item'          => esc_html__( 'Edit Event', 'themographics_core' ),
				'new_item'           => esc_html__( 'New Event', 'themographics_core' ),
				'view'               => esc_html__( 'View Event', 'themographics_core' ),
				'view_item'          => esc_html__( 'View Event', 'themographics_core' ),
				'search_items'       => esc_html__( 'Search Event', 'themographics_core' ),
				'not_found'          => esc_html__( 'No Event found', 'themographics_core' ),
				'not_found_in_trash' => esc_html__( 'No Event found in trash', 'themographics_core' ),
				'parent'             => esc_html__( 'Parent Event', 'themographics_core' ),
			);
			$args = array(
				'labels'			  => $labels,
				'description'         => esc_html__( 'This is where you can add new Event', 'themographics_core' ),
				'public'              => true,
				'supports'            => array( 'title', 'thumbnail', 'editor' ),
				'show_ui'             => true,
				'capability_type'     => 'post',
				'map_meta_cap'        => true,
				'publicly_queryable'  => true,
				'exclude_from_search' => false,
				'hierarchical'        => false,
				'menu_position' 	  => 8,
				'rewrite'			  => array('slug' => 'tg_events', 'with_front' => true),
				'query_var'           => false,
				'has_archive'         => 'false',
			); 
			register_post_type( 'tg_events' , $args );
			
		}
		
		/**
		 * @Prepare Event Categories
		 * @return {}
		 */
		public function init_event_taxonomies() {
			$labels = array(
				'name'              => _x( 'Categories', 'taxonomy general name', 'themographics_core' ),
				'singular_name'     => _x( 'Category', 'taxonomy singular name' , 'themographics_core'),
				'search_items'      => esc_html__( 'Search Categories' , 'themographics_core'),
				'all_items'         => esc_html__( 'All Categories' , 'themographics_core'),
				'parent_item'       => esc_html__( 'Parent Category' , 'themographics_core'),
				'parent_item_colon' => esc_html__( 'Parent Category:' , 'themographics_core'),
				'edit_item'         => esc_html__( 'Edit Category' , 'themographics_core'),
				'update_item'       => esc_html__( 'Update Category' , 'themographics_core'),
				'add_new_item'      => esc_html__( 'Add New Category' , 'themographics_core'),
				'new_item_name'     => esc_html__( 'New Category Name', 'themographics_core' ),
				'menu_name'         => esc_html__( 'Categories', 'themographics_core' ),
			);
		
			$args = array(
				'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
				'labels'            => $labels,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categories' ),
			);
		
			register_taxonomy( 'event_categories', array( 'tg_events' ), $args );
		}
		
		/**
		 * @Prepare Columns
		 * @return {post}
		 */
		public function events_columns_add($columns) {
			unset($columns['date']);
			$columns['location'] 		= esc_html__('Phone','themographics_core');
			$columns['dates']			= esc_html__('Start Date / End Date','themographics_core');
			$columns['fee']				= esc_html__('Address','themographics_core');
		 
  			return $columns;
		}
		
		/**
		 * @Get Columns
		 * @return {}
		 */
		public function events_columns($name) {
			global $post;
			
			//$phone		= get_post_meta($post->ID,'location',true);
			//$address	= get_post_meta($post->ID,'dates',true);
			//$email		= get_post_meta($post->ID,'fee',true);
			
			switch ($name) {
				case 'location':
					//echo esc_attr( $phone );
				break;		
				
				case 'dates':
					///echo esc_attr( $email );	
 				break;
				
				case 'fee':
					//echo esc_attr( $address );	
				break;
				
			}
		}
	}
	
  	new TG_Events();	
}