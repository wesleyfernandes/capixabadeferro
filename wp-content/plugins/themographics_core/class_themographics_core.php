<?php defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
/*
Plugin Name: Themographics Core
Plugin URI: http://themographics.com/
Description: Themographics Framework for custom posts and customization
Version: 1.2
Author: ThemoGraphics
Author URI: http://themeforest.net/user/themographics
License: GPL2
*/

if( !class_exists( 'Themographics_Core' ) ) {
    class Themographics_Core {
		public $plugin_url;
		public $plugin_dir;
		
		
		public function __construct(){
			$this->plugin_url  = plugin_dir_url( __FILE__ );
			$this->plugin_dir  = plugin_dir_path( __FILE__ );
			
			require_once('core/class-core.php');
			require_once('core/post-types/gallery.php');
			require_once('core/post-types/promotion_videos.php');
			require_once('core/post-types/events.php');
			
			add_filter('template_include', array(&$this, 'tg_templates'));
			
			//Language Load
			add_action( 'after_setup_theme', array( &$this, 'tg_load_textdomain') );
			
			add_filter( 'post_row_actions', array( &$this, 'triathlon_remove_row_actions'));
		}
		
		/**
		 * @PLugin Scripts
		 * @return {}
		 */
		public function tg_load_textdomain(){
			load_plugin_textdomain( 'themographics_core', false, dirname( plugin_basename(__FILE__) ) . '/languages' );
		}
		
		/**
		 *
		 * @PLugin URl
		 */
		public static function plugin_url(){
			return plugin_dir_url( __FILE__ );
		}
		
		/**
		 *
		 * @Plugin Images Path
		 */
		public static function plugin_img_url(){
			return plugin_dir_url( __FILE__ );
		}
		
		/**
		 *
		 * @Plugin Directory URL
		 */
		public static function plugin_dir(){
			return plugin_dir_path( __FILE__ );
		}
		
		/**
		 *
		 * @Plugin Activation
		 */
		public static function activate(){	
			//do something
		}
		
		/**
		 *
		 * @Plugin deactivation
		 */
		public static function deactivate(){
           //do something
        } 
		
		/**
		 *
		 * @ Include Template
		 */
		public function tg_templates( $single_template )
		{
			global $post;
			$single_path = dirname( __FILE__ );
			if ( get_post_type() == 'tg_events' ) {
				if ( is_single() ) {
					//$single_template = plugin_dir_path( __FILE__ ) . '/templates/single-events.php';
				}
			}
			return $single_template;
		}
		
		/**
		 * @post type view
		 * @return 
		 */
		public function triathlon_remove_row_actions( $actions ){
			if( get_post_type() == 'tg_events' 
				||
				get_post_type() == 'tg_gallery' 
				||
				get_post_type() == 'videos' 
			 ) {
				unset( $actions['view'] );
			}
			return $actions;
		}
		
	}
	
	new Themographics_Core();
	register_activation_hook( __FILE__, array( 'Themographics_Core', 'activate' ));
	register_deactivation_hook( __FILE__, array( 'Themographics_Core', 'deactivate'));
}
