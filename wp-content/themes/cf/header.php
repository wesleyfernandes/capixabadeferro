<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Triathlon
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php if ( function_exists('triathlon_get_favicon') ) { triathlon_get_favicon(); }?>
<?php wp_head(); ?>
</head>
<body <?php body_class()?> <?php if ( function_exists('triathlon_prepare_comingsoon') ) { triathlon_prepare_comingsoon(); }?>>
<?php do_action('triathlong_do_init_headers');?>
<main id="main" class="site-main" role="main">