<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

if( is_single() ){
	$classes	= 'col-md-3 col-sm-6 col-xs-6';
} else{
	
	if(function_exists('fw_get_db_settings_option')){
		$enable_sidebar = fw_get_db_settings_option('enable_sidebar');
		$is_sidebar		= isset( $enable_sidebar ) && $enable_sidebar == 'on' ? 'on' : 'off';
		$classes		= isset( $enable_sidebar ) && $enable_sidebar == 'on' ? 'col-md-4 col-sm-6 col-xs-12' : 'col-md-3 col-sm-6 col-xs-12';
	} else{
		$classes		= 'col-md-3 col-sm-6 col-xs-12';
	}
}

?>
<div class="<?php echo esc_attr( $classes );?>" <?php //post_class( $classes ); ?>>
	<div class="product">
		<?php woocommerce_get_template( 'loop/sale-flash.php' ); ?>
		<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
		<div class="product-img">
			<div class="box">
				<a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail( $post->ID, 'shop_catalog') ?></a>
			</div>
			<a class="btn-custom btn-quickview" href="javascript:;" data-toggle="modal" data-target=".bs-example-modal-sm-<?php echo intval($post->ID);?>"><?php esc_html_e('Quick view', 'triathlon'); ?></a>
			<!-- Modal Box Start -->
			<?php do_action('triathlon_render_quick_view'); // Get Quick View?>
		
		<!-- Modal Box End -->
		</div>

		<span class="product-name"><a href="<?php the_permalink(); ?>" class="main-link"><?php the_title(); ?></a></span>
		<?php
			/**
			 * woocommerce_after_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_template_loop_rating - 5
			 * @hooked woocommerce_template_loop_price - 10
			 */
			do_action( 'woocommerce_after_shop_loop_item_title' );
		?>

		<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
	</div><!--.col-wrapper-->
</div>