<?php
/**
 * Empty cart page
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}



?>
<div class="container content">
 <div class="row">
	<?php wc_print_notices();?>
	<p class="cart-empty"><?php esc_html_e( 'Your cart is currently empty.', 'triathlon' ) ?></p>
	
	<?php do_action( 'woocommerce_cart_is_empty' ); ?>
	<p class="return-to-shop">
	<a href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>" class="btn-theme btn-edit">
		<span class="txt"><?php esc_html_e( 'Return To Shop', 'triathlon' ) ?></span>
		<span class="round">
			<i class="icon-arrow-right-latest-races"></i>
		</span>
	</a>
	</p>
 </div>
</div>