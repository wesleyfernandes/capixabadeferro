<?php
/**
 * Cart totals
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.6
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<div class="cart_totals <?php if ( WC()->customer->has_calculated_shipping() ) echo 'calculated_shipping'; ?>">
	
	<div class="cart-subtotal">
		<?php do_action( 'woocommerce_before_cart_totals' ); ?>
		<span><?php esc_html_e( 'Cart Subtotal', 'triathlon' ); ?></span>
		<?php wc_cart_totals_subtotal_html(); ?>
	</div>
	
	<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
		<div class="cart-subtotal cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
			<span><?php wc_cart_totals_coupon_label( $coupon ); ?></span>
			<?php wc_cart_totals_coupon_html( $coupon ); ?>
		</div>
	<?php endforeach; ?>


	<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>

		<?php do_action( 'woocommerce_cart_totals_before_shipping' ); ?>

		<?php wc_cart_totals_shipping_html(); ?>

		<?php do_action( 'woocommerce_cart_totals_after_shipping' ); ?>

	<?php elseif ( WC()->cart->needs_shipping() ) : ?>
		<div class="cart-subtotal shipping">
			<span><?php esc_html_e( 'Shipping', 'triathlon' ); ?></span>
			<span><?php woocommerce_shipping_calculator(); ?></span>
		</div>
	<?php endif; ?>

	<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
		<div class="cart-subtotal fee">
			<span><?php echo esc_html( $fee->name,'triathlon' ); ?></span>
			<span><?php wc_cart_totals_fee_html( $fee ); ?></span>
		</div>
	<?php endforeach; ?>

		<?php if ( wc_tax_enabled() && WC()->cart->tax_display_cart == 'excl' ) : ?>
			<?php if ( get_option( 'woocommerce_tax_total_display' ) == 'itemized' ) : ?>
				<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
					<div class="cart-subtotal tax-rate tax-rate-<?php echo sanitize_title( $code ); ?>">
						<span><?php echo esc_html( $tax->label,'triathlon' ); ?></span>
						<span><?php echo wp_kses_post( $tax->formatted_amount ); ?></span>
					</div>
				<?php endforeach; ?>
			<?php else : ?>
				<div class="cart-subtotal tax-total">
					<span><?php echo esc_html( WC()->countries->tax_or_vat(),'triathlon' ); ?></span>
					<span><?php wc_cart_totals_taxes_total_html(); ?></span>
				</div>
			<?php endif; ?>
		<?php endif; ?>

		<?php do_action( 'woocommerce_cart_totals_before_order_total' ); ?>

		<div class="cart-subtotal order-total">
			<strong><?php esc_html_e( 'Order Total', 'triathlon' ); ?></strong>
			<?php wc_cart_totals_order_total_html(); ?>
		</div>

		<?php do_action( 'woocommerce_cart_totals_after_order_total' ); ?>

	<?php if ( WC()->cart->get_cart_tax() ) : ?>
		<p class="wc-cart-shipping-notice"><small><?php

			$estimated_text = WC()->customer->is_customer_outside_base() && ! WC()->customer->has_calculated_shipping()
				? sprintf( ' ' . __( ' (taxes estimated for %s)', 'triathlon' ), WC()->countries->estimated_for_prefix() . WC()->countries->countries[ WC()->countries->get_base_country() ] )
				: '';

			printf( __( 'Note: Shipping and taxes are estimated%s and will be updated during checkout based on your billing and shipping information.', 'triathlon' ), $estimated_text );

		?></small></p>
	<?php endif; ?>
	<?php do_action( 'woocommerce_after_cart_totals' ); ?>
</div>
