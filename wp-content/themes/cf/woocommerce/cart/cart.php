<?php
/**
 * Cart Page
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.3.8
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
global $woocommerce;

do_action('woocommerce_before_cart');
?>
<div class="container content">
    <div class="row">
        <?php wc_print_notices(); ?>
        <form action="<?php echo esc_url(WC()->cart->get_cart_url()); ?>" method="post">
            <fieldset>
                <?php do_action('woocommerce_before_cart_table'); ?>
                <table class="shop_table1 table cart-table">
                    <thead>
                        <tr>
                            <th class="product-name"><?php esc_html_e('Product Name', 'triathlon'); ?></th>
                            <th class="product-price"><?php esc_html_e('Unit Price', 'triathlon'); ?></th>
                            <th class="product-quantity"><?php esc_html_e('Quantity', 'triathlon'); ?></th>
                            <th class="product-subtotal"><?php esc_html_e('Total', 'triathlon'); ?></th>
                            <th class="product-remove">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php do_action('woocommerce_before_cart_contents'); ?>
                        <?php
                        foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
                            $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
                            $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

                            if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
                                ?>
                                <tr class="<?php echo esc_attr(apply_filters('woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key)); ?>">

                                    <td data-title="Product Name">
                                        <?php
                                        $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);

                                        if (!$_product->is_visible()) {
                                            echo triathlon_esc_specialchars($thumbnail);
                                        } else {
                                            printf('<a href="%s">%s</a>', esc_url($_product->get_permalink($cart_item)), $thumbnail);
                                        }
                                        ?>
                                        <em>
                                            <?php
                                            if (!$_product->is_visible()) {
                                                echo apply_filters('woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key) . '&nbsp;';
                                            } else {
                                                echo apply_filters('woocommerce_cart_item_name', sprintf('<a href="%s">%s </a>', esc_url($_product->get_permalink($cart_item)), $_product->get_title()), $cart_item, $cart_item_key);
                                            }

                                            // Meta data
                                            echo WC()->cart->get_item_data($cart_item);

                                            // Backorder notification
                                            if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
                                                echo esc_html__('Available on backorder', 'triathlon');
                                            }
                                            ?>
                                        </em>
                                    </td>
                                    <td data-title="Unit Price">
                                        <?php
                                        echo apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key);
                                        ?>
                                    </td>
                                    <td data-title="Quantity">
                                        <span class="quantity-sapn">
                                            <?php
                                            if ($_product->is_sold_individually()) {
                                                $product_quantity = sprintf('1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key);
                                            } else {
                                                $product_quantity = woocommerce_quantity_input(array(
                                                    'input_name' => "cart[{$cart_item_key}][qty]",
                                                    'input_value' => $cart_item['quantity'],
                                                    'max_value' => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
                                                    'min_value' => '0'
                                                        ), $_product, false);
                                            }
                                            echo apply_filters('woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item);
                                            ?>
                                        </span>
                                    </td>
                                    <td data-title="Total">
                                        <?php
                                        echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key);
                                        ?>
                                    </td>
                                    <td data-title="Remove Item">
                                        <?php
                                        echo apply_filters('woocommerce_cart_item_remove_link', sprintf(
                                                        '<i class="btn-delete-item"><a href="%s" class="fa fa-remove" title="%s" data-product_id="%s" data-product_sku="%s"></a></i>', esc_url(WC()->cart->get_remove_url($cart_item_key)), esc_html__('Remove this item', 'triathlon'), esc_attr($product_id), esc_attr($_product->get_sku())
                                                ), $cart_item_key);
                                        ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>

                    </tbody>
                </table>
                <div class="cart-foot row">
                    <?php do_action('woocommerce_cart_contents'); ?>
                    <div class="col-sm-6 coupon">
                        <?php do_action('woocommerce_cart_contents'); ?>
                        <?php if (WC()->cart->coupons_enabled()) { ?>
                            <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e('Coupon code', 'triathlon'); ?>" /> <input type="submit" class="btn-theme"   name="apply_coupon" value="<?php esc_attr_e('Apply Coupon', 'triathlon'); ?>" />
                            <span class="round">
                                <i class="icon-arrow-right-latest-races"></i>
                            </span>
                            <?php do_action('woocommerce_cart_coupon'); ?>
                        <?php } ?>
                    </div>
                    <div class="col-sm-6 cart-total">
                        <div class="cart-btns">
                            <a class="btn-theme black"  href="<?php echo esc_url($woocommerce->cart->get_checkout_url()); ?>">
                                <span class="txt"><?php esc_html_e('Checkout', 'triathlon'); ?></span>
                                <span class="round">
                                    <i class="icon-arrow-right-latest-races"></i>
                                </span>
                            </a>
                            <div class="update-cart-style">
                                <input type="submit" class="btn-theme red" name="update_cart" value="<?php esc_attr_e('Update Cart', 'triathlon'); ?>" /></input>
                                <span class="round">
                                    <i class="icon-arrow-right-latest-races"></i>
                                </span>
                            </div>
                            <?php do_action('woocommerce_cart_actions'); ?>
                            <?php wp_nonce_field('woocommerce-cart'); ?>
                        </div>
                        <?php do_action('woocommerce_cart_collaterals'); ?>
                    </div>
                    <?php do_action('woocommerce_after_cart_contents'); ?>
                </div>
                <?php do_action('woocommerce_after_cart_table'); ?>
            </fieldset>
        </form>
        <?php do_action('woocommerce_after_cart'); ?>
    </div>
</div>