<?php
/**
 * Loop Price
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
?>

<?php if ( $price_html = $product->get_price_html() ) : ?>
	<span class="product-price"><?php echo esc_html_e('Our Price: ','triathlon');?><?php echo force_balance_tags( $price_html); ?></span>
<?php endif; ?>
