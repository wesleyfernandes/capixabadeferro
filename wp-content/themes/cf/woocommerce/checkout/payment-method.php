<?php
/**
 * Output a single payment method
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<li class="payment_method_<?php echo esc_attr( $gateway->id); ?>">
	<label for="payment_method_<?php echo esc_attr( $gateway->id); ?>"><input id="payment_method_<?php echo esc_attr( $gateway->id); ?>" type="radio" class="input-radio" name="payment_method" value="<?php echo esc_attr( $gateway->id ); ?>" <?php checked( $gateway->chosen, true ); ?> data-order_button_text="<?php echo esc_attr( $gateway->order_button_text ); ?>" /> <em><?php echo esc_attr( $gateway->get_title() ); ?> <?php echo triathlon_esc_specialchars( $gateway->get_icon()); ?></em></label>
	<?php if ( $gateway->has_fields() || $gateway->get_description() ) : ?>
		<div class="payment_box payment_method_<?php echo esc_attr( $gateway->id ); ?>" <?php if ( ! $gateway->chosen ) : ?>style="display:none;"<?php endif; ?>>
			<p><?php $gateway->payment_fields(); ?></p>
		</div>
	<?php endif; ?>
</li>
