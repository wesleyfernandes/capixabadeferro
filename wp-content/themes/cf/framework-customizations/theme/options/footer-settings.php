<?php

if (!defined('FW')) {
    die('Forbidden');
}

$options = array(
    'footer' => array(
        'title' => esc_html__('Footer', 'triathlon'),
        'type' => 'tab',
        'options' => array(
            'footer_settings' => array(
                'title' => esc_html__('Footer Settings', 'triathlon'),
                'type' => 'tab',
                'options' => array(
                    'enable_back' => array(
                        'type' => 'switch',
                        'value' => 'on',
                        'attr' => array(),
                        'label' => esc_html__('Enable / Disable back to top', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                        'left-choice' => array(
                            'value' => 'off',
                            'label' => esc_html__('Off', 'triathlon'),
                        ),
                        'right-choice' => array(
                            'value' => 'on',
                            'label' => esc_html__('ON', 'triathlon'),
                        ),
                    ),
                    'enable_newslatter' => array(
                        'type' => 'switch',
                        'value' => 'on',
                        'attr' => array(),
                        'label' => esc_html__('Enable / Disable Newsletter', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                        'left-choice' => array(
                            'value' => 'off',
                            'label' => esc_html__('Off', 'triathlon'),
                        ),
                        'right-choice' => array(
                            'value' => 'on',
                            'label' => esc_html__('ON', 'triathlon'),
                        ),
                    ),
                    'newslatter_title' => array(
                        'type' => 'text',
                        'value' => 'Be the first who signs up to learn about our latest news, events & promotions.',
                        'label' => esc_html__('Newsletter Text', 'triathlon'),
                    ),
                    'footer_copyright' => array(
                        'type' => 'text',
                        'value' => esc_html__('', 'triathlon'),
                        'label' => esc_html__('Footer Copyright', 'triathlon'),
                    ),
                    'payment_image' => array(
                        'type' => 'upload',
                        'label' => esc_html__('Upload Payments Logo Image', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                        'images_only' => true,
                    ),
                )
            ),
            'contact_settings' => array(
                'title' => esc_html__('Contact Settings', 'triathlon'),
                'type' => 'tab',
                'options' => array(
                    'contact-container' => array(
                        'type' => 'box',
                        'options' => array(
                            'contact' => array(
                                'title' => esc_html__('Contact Settings', 'triathlon'),
                                'type' => 'tab',
                                'options' => array(
                                    'subject_message' => array(
                                        'type' => 'text',
                                        'value' => 'New Message',
                                        'label' => esc_html__('Subject Message', 'triathlon'),
                                    ),
                                    'submit_button' => array(
                                        'type' => 'text',
                                        'value' => 'Send',
                                        'label' => esc_html__('Submit Button', 'triathlon'),
                                    ),
                                    'success_message' => array(
                                        'type' => 'text',
                                        'value' => 'Message sent!',
                                        'label' => esc_html__('Success Message', 'triathlon'),
                                    ),
                                    'failure_message' => array(
                                        'type' => 'text',
                                        'value' => 'Oops something went wrong.',
                                        'label' => esc_html__('Failure Message', 'triathlon'),
                                    ),
                                    'email' => array(
                                        'type' => 'text',
                                        'value' => 'info@triathlon.com',
                                        'label' => esc_html__('Email To', 'triathlon'),
                                        'desc' => 'The form will be sent to this email address.',
                                    ),
                                )
                            ),
                            'general' => array(
                                'title' => esc_html__('Genral Settings ', 'triathlon'),
                                'type' => 'tab',
                                'options' => array(
                                    'form_enable' => array(
                                        'type' => 'switch',
                                        'value' => 'enable',
                                        'attr' => array(),
                                        'label' => esc_html__('Enable / Disable Contact Form', 'triathlon'),
                                        'left-choice' => array(
                                            'value' => 'disable',
                                            'label' => esc_html__('Disable', 'triathlon'),
                                        ),
                                        'right-choice' => array(
                                            'value' => 'enable',
                                            'label' => esc_html__('Enable', 'triathlon'),
                                        ),
                                    ),
                                    'bg_image' => array(
                                        'type' => 'upload',
                                        'attr' => array(),
                                        'label' => esc_html__('Background', 'triathlon'),
                                        'desc' => esc_html__('Upload Your Sub Footer Background Image', 'triathlon'),
                                        'images_only' => true,
                                    ),
                                    'form_heading' => array(
                                        'type' => 'text',
                                        'value' => 'Learn more about dwarfing your competitors.',
                                        'label' => esc_html__('Form Heading', 'triathlon'),
                                    ),
                                )
                            )
                        ),
                    ),
                )
            ),
        )
    )
);
