<?php

if (!defined('FW')) {
    die('Forbidden');
}
$options = array(
    'woocommerce_settings' => array(
        'type' => 'tab',
        'title' => esc_html__('Woocommerce Settings', 'triathlon'),
        'options' => array(
            'general' => array(
                'title' => esc_html__('Woocommerce Settings', 'triathlon'),
                'type' => 'tab',
                'options' => array(
                    'show_shop_posts' => array(
                        'type' => 'slider',
                        'value' => 9,
                        'properties' => array(
                            'min' => 0,
                            'max' => 100,
                            'sep' => 1,
                        ),
                        'label' => esc_html__('Show No of Posts', 'triathlon'),
                    ),
                )
            ),
			'shop_settings' => array(
                'title' => esc_html__('Shop Page Settings', 'triathlon'),
                'type' => 'tab',
                'options' => array(
                    'enable_banner' => array(
                        'type' => 'switch',
                        'value' => 'on',
                        'attr' => array(),
                        'label' => esc_html__('Enable / Disable banner', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                        'left-choice' => array(
                            'value' => 'off',
                            'label' => esc_html__('Off', 'triathlon'),
                        ),
                        'right-choice' => array(
                            'value' => 'on',
                            'label' => esc_html__('ON', 'triathlon'),
                        ),
                    ),
                    'product_logo' => array(
                        'type' => 'upload',
                        'label' => esc_html__('Upload Product page banner', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                        'images_only' => true,
                    ),
                    'product_title' => array(
                        'type' => 'text',
                        'label' => esc_html__('Title', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                    ),
                    'product_button_link' => array(
                        'type' => 'text',
                        'value' => '#',
                        'label' => esc_html__('Button Text', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                    ),
                    'product_button_text' => array(
                        'type' => 'text',
                        'value' => 'Shop Now',
                        'label' => esc_html__('Button Link', 'triathlon'),
                        'desc' => esc_html__('Leave empty to hide button', 'triathlon'),
                    ),
					
					'enable_sidebar' => array(
                        'type' => 'switch',
                        'value' => 'on',
                        'attr' => array(),
                        'label' => esc_html__('Shop Page Sidebar ON/OFF', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                        'left-choice' => array(
                            'value' => 'off',
                            'label' => esc_html__('OFF', 'triathlon'),
                        ),
                        'right-choice' => array(
                            'value' => 'on',
                            'label' => esc_html__('ON', 'triathlon'),
                        ),
                    ),
                    'sidebar_position' => array(
                        'type' => 'select',
                        'value' => 'left',
                        'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
                        'label' => esc_html__('Sidebar Position', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                        'help' => esc_html__('', 'triathlon'),
                        'choices' => array(
                            'left' => esc_html__('Left', 'triathlon'),
                            'right' => esc_html__('Right', 'triathlon'),
                        ),
                    ),
                )
            ),
			'archive_settings' => array(
                'title' => esc_html__('Archive Page Settings', 'triathlon'),
                'type' => 'tab',
                'options' => array(
                    'archive_enable_sidebar' => array(
                        'type' => 'switch',
                        'value' => 'on',
                        'attr' => array(),
                        'label' => esc_html__('Archive Page Sidebar ON/OFF', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                        'left-choice' => array(
                            'value' => 'off',
                            'label' => esc_html__('OFF', 'triathlon'),
                        ),
                        'right-choice' => array(
                            'value' => 'on',
                            'label' => esc_html__('ON', 'triathlon'),
                        ),
                    ),
                    'archive_sidebar_position' => array(
                        'type' => 'select',
                        'value' => 'left',
                        'attr' => array(),
                        'label' => esc_html__('Sidebar Position', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                        'help' => esc_html__('', 'triathlon'),
                        'choices' => array(
                            'left' => esc_html__('Left', 'triathlon'),
                            'right' => esc_html__('Right', 'triathlon'),
                        ),
                    ),
                )
            ),
        )
    )
);
