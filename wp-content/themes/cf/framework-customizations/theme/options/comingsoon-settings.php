<?php

if (!defined('FW')) {
    die('Forbidden');
}
$options = array(
    'commingsoon_settings' => array(
        'type' => 'tab',
        'title' => esc_html__('Coming Soon', 'triathlon'),
        'options' => array(
            'commingsoon-box' => array(
                'title' => esc_html__('Coming Soon Settings', 'triathlon'),
                'type' => 'box',
                'options' => array(
                    'maintenance' => array(
                        'type' => 'switch',
                        'value' => 'disable',
                        'label' => esc_html__('Maintenance Mode', 'triathlon'),
                        'left-choice' => array(
                            'value' => 'off',
                            'label' => esc_html__('OFF', 'triathlon'),
                        ),
                        'right-choice' => array(
                            'value' => 'on',
                            'label' => esc_html__('ON', 'triathlon'),
                        ),
                    ),
                    'comming_title' => array(
                        'type' => 'text',
                        'label' => esc_html__('Title', 'triathlon'),
                        'value' => 'Coming Soon!',
                        'label' => esc_html__('', 'triathlon'),
                    ),
                    'comming_description' => array(
                        'type' => 'textarea',
                        'label' => esc_html__('Description', 'triathlon'),
                        'value' => 'We Are Launching Very Soon!',
                        'label' => esc_html__('', 'triathlon'),
                    ),
                    'logo' => array(
                        'type' => 'upload',
                        'label' => esc_html__('Logo', 'triathlon'),
                        'desc' => esc_html__('Upload Your Logo image on coming soon page.', 'triathlon'),
                        'images_only' => true,
                    ),
                    'background' => array(
                        'type' => 'upload',
                        'label' => esc_html__('Background Image', 'triathlon'),
                        'desc' => esc_html__('Upload Your background image on coming soon page.', 'triathlon'),
                        'images_only' => true,
                    ),
                    'date' => array(
                        'type' => 'date-picker',
                        'label' => esc_html__('Choose Date', 'triathlon'),
                        'monday-first' => true, // The week will begin with Monday; for Sunday, set to false
                        'min-date' => date('m,d,Y'), // By default minimum date will be current day. Set a date in format d-m-Y as a start date
                        'max-date' => null, // By default there is not maximum date. Set a date in format d-m-Y as a start date
                    ),
                )
            ),
        )
    )
);
