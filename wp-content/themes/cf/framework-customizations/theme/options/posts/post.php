<?php

if (!defined('FW')) {
    die('Forbidden');
}

$options = array(
    'blog_settings_advanced' => array(
        'title' => 'Blog Post Settings',
        'type' => 'box',
        'options' => array(
            'sub_headers' => array(
                'title' => esc_html__('Sub-Header Settings', 'triathlon'),
                'type' => 'tab',
                'options' => array(
                    'enable_subheader' => array(
                        'type' => 'switch',
                        'value' => 'enable',
                        'label' => esc_html__('Subheader', 'triathlon'),
                        'desc' => esc_html__('Enable or Disable Subheader', 'triathlon'),
                        'left-choice' => array(
                            'value' => 'enable',
                            'label' => esc_html__('Enable', 'triathlon'),
                        ),
                        'right-choice' => array(
                            'value' => 'disable',
                            'label' => esc_html__('Disable', 'triathlon'),
                        ),
                    ),
                    'sub_heading' => array(
                        'type' => 'textarea',
                        'value' => '',
                        'label' => esc_html__('Sub Heading', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                    ),
                    'sub_heading_bg' => array(
                        'type' => 'color-picker',
                        'value' => '#FDB62D',
                        'label' => esc_html__('Sub Heading bg color', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                    ),
                    'sub_heading_text' => array(
                        'type' => 'color-picker',
                        'value' => '#FFF',
                        'label' => esc_html__('Sub Heading Text color', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                    ),
                )
            ),
            'post_settings' => array(
                'title' => esc_html__('Post Settings', 'triathlon'),
                'type' => 'tab',
                'options' => array(
                    'blog_settings' => array(
                        'type' => 'radio',
                        'value' => 'image',
                        'label' => esc_html__('Post Display Settings', 'triathlon'),
                        'desc' => esc_html__('Choose settings for your post. Simple Image, Slider Image and Video', 'triathlon'),
                        'choices' => array(
                            'image' => esc_html__('Image', 'triathlon'),
                            'gallery' => esc_html__('Image Slider', 'triathlon'),
                            'video' => esc_html__('Video', 'triathlon'),
                        ),
                        'inline' => true,
                    ),
                    'post_inner_settings' => array(
                        'type' => 'box',
                        'options' => array(
                            'image-box' => array(
                                'title' => esc_html__('Image', 'triathlon'),
                                'type' => 'tab',
                                'options' => array(
                                    'blog_post_image' => array(
                                        'type' => 'html',
                                        'html' => 'Uplaod Image',
                                        'label' => esc_html__('Blog Detail View Image', 'triathlon'),
                                        'desc' => esc_html__('Upload Your detail blog post image.(Preferred Size is 1139 by 289.)', 'triathlon'),
                                        'help' => esc_html__('Please upload your thumbnail image.', 'triathlon'),
                                        'images_only' => true,
                                    ),
                                )
                            ),
                            'gallery-box' => array(
                                'title' => esc_html__('Gallery', 'triathlon'),
                                'type' => 'tab',
                                'options' => array(
                                    'blog_post_gallery' => array(
                                        'type' => 'multi-upload',
                                        'label' => esc_html__('Add Image Slider', 'triathlon'),
                                        'desc' => esc_html__('Add Image Slider for your post. (Preferred Size is 1314 by 737.)', 'triathlon'),
                                        'help' => esc_html__('Only worked if the post display setting is equal to Image Gallery.', 'triathlon'),
                                        'images_only' => true,
                                    ),
                                )
                            ),
                            'video-box' => array(
                                'title' => esc_html__('Video', 'triathlon'),
                                'type' => 'tab',
                                'options' => array(
                                    'blog_video_link' => array(
                                        'type' => 'text',
                                        'label' => esc_html__('Blog Video Custom Link', 'triathlon'),
                                        'desc' => esc_html__('Only worked if the post display setting is equal to Video and if the video settings is equal to custom.', 'triathlon'),
                                    ),
                                )
                            ),
                        ),
                    )
                )
            ),
            'general_post_settings' => array(
                'title' => esc_html__('General Settings', 'triathlon'),
                'type' => 'tab',
                'options' => array(
                    'related_posts_title' => array(
                        'type' => 'text',
                        'label' => esc_html__('Title', 'triathlon'),
                        'desc' => esc_html__('Enter title for related posts in single blog detail .', 'triathlon'),
                    ),
                    'enable_related_posts' => array(
                        'type' => 'switch',
                        'value' => 'show',
                        'label' => esc_html__('Related Posts', 'triathlon'),
                        'desc' => esc_html__('Enable or Disable Related Posts Section', 'triathlon'),
                        'left-choice' => array(
                            'value' => 'hide',
                            'label' => esc_html__('Hide', 'triathlon'),
                        ),
                        'right-choice' => array(
                            'value' => 'show',
                            'label' => esc_html__('Show', 'triathlon'),
                        ),
                    ),
                )
            ),
        )
    ),
);
