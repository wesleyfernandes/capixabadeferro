<?php

if (!defined('FW')) {
    die('Forbidden');
}

$options = array(
    'settings' => array(
        'type' => 'box',
        'options' => array(
            'sub_headers' => array(
                'title' => esc_html__('Sub-Header Settings', 'triathlon'),
                'type' => 'tab',
                'options' => array(
                    'enable_subheader' => array(
                        'type' => 'switch',
                        'value' => 'enable',
                        'label' => esc_html__('Subheader', 'triathlon'),
                        'desc' => esc_html__('Enable or Disable Subheader', 'triathlon'),
                        'left-choice' => array(
                            'value' => 'enable',
                            'label' => esc_html__('Enable', 'triathlon'),
                        ),
                        'right-choice' => array(
                            'value' => 'disable',
                            'label' => esc_html__('Disable', 'triathlon'),
                        ),
                    ),
                    'sub_heading' => array(
                        'type' => 'textarea',
                        'value' => '',
                        'label' => esc_html__('Sub Heading', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                    ),
                    'sub_heading_bg' => array(
                        'type' => 'color-picker',
                        'value' => '#FDB62D',
                        'label' => esc_html__('Sub Heading bg color', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                    ),
                    'sub_heading_text' => array(
                        'type' => 'color-picker',
                        'value' => '#FFF',
                        'label' => esc_html__('Sub Heading Text color', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                    ),
                )
            ),
            'portfolio_setting' => array(
                'title' => esc_html__('Portfolio Settings', 'triathlon'),
                'type' => 'tab',
                'options' => array(
                    'enable_slider_or_content' => array(
                        'type' => 'switch',
                        'value' => 'enable',
                        'label' => esc_html__('View Type', 'triathlon'),
                        'desc' => esc_html__('Enable to switch slider with content or disable to just view slider.', 'triathlon'),
                        'left-choice' => array(
                            'value' => 'disable',
                            'label' => esc_html__('Disable', 'triathlon'),
                        ),
                        'right-choice' => array(
                            'value' => 'enable',
                            'label' => esc_html__('Enable', 'triathlon'),
                        ),
                    ),
                    'project_contributors' => array(
                        'type' => 'addable-option',
                        'label' => esc_html__('Project Detail', 'triathlon'),
                        'desc' => esc_html__('Enter assignees to this project for eample: ART DIRECTOR : LUNA THOMAS in one line.', 'triathlon'),
                        'option' => array('type' => 'text'),
                    ),
                    'web_url' => array(
                        'type' => 'text',
                        'value' => '',
                        'label' => esc_html__('Enter Web Url', 'triathlon'),
                    ),
                    'view_more' => array(
                        'type' => 'text',
                        'value' => '',
                        'label' => esc_html__('Enter View More Link', 'triathlon'),
                    ),
                    'portfolio_inner_setting' => array(
                        'type' => 'box',
                        'options' => array(
                            'portfolio_gallery_type' => array(
                                'type' => 'radio',
                                'value' => 'image',
                                'label' => esc_html__('Gallery Type', 'triathlon'),
                                'desc' => esc_html__('Choose Your Galley Type Image or Video.', 'triathlon'),
                                'choices' => array(
                                    'image' => esc_html__('Image', 'triathlon'),
                                    'video' => esc_html__('Video', 'triathlon'),
                                ),
                                // Display choices inline instead of list
                                'inline' => true,
                            ),
                            'portfolio_inner_settings_tab1' => array(
                                'type' => 'tab',
                                'title' => 'Image',
                                'options' => array(
                                    'gallery' => array(
                                        'type' => 'multi-upload',
                                        'label' => esc_html__('Gallery', 'triathlon'),
                                        'desc' => esc_html__('Add just Gallery for your portfolio post.', 'triathlon'),
                                        'images_only' => true,
                                    ),
                                    'portfolio_inner_settings_box' => array(
                                        'type' => 'box',
                                        'options' => array(
                                            'portfolio_inner_settings_tab1_bottom_image1' => array(
                                                'type' => 'tab',
                                                'title' => 'Bottom Image 1',
                                                'options' => array(
                                                    'image_or_video1' => array(
                                                        'type' => 'switch',
                                                        'value' => 'image',
                                                        'label' => esc_html__('Image or Video', 'triathlon'),
                                                        'left-choice' => array(
                                                            'value' => 'image',
                                                            'label' => esc_html__('Image', 'triathlon'),
                                                        ),
                                                        'right-choice' => array(
                                                            'value' => 'video',
                                                            'label' => esc_html__('Video', 'triathlon'),
                                                        ),
                                                    ),
                                                    'portfolio_gallery_image1' => array(
                                                        'type' => 'upload',
                                                        'label' => esc_html__('Gallery Bottom Image 1', 'triathlon'),
                                                        'desc' => esc_html__('Add Bottom images for portfolio post which liked to the detail post.', 'triathlon'),
                                                        'images_only' => true,
                                                    ),
                                                    'portfolio_gallery_image1_video' => array(
                                                        'type' => 'text',
                                                        'value' => '',
                                                        'label' => esc_html__('Gallery Bottom Image 1 Video', 'triathlon'),
                                                        'desc' => esc_html__('Add video iframe link if you want to attach video to image 1. <strong>Note: Video will be overwrite if you have define video link at above field.</strong>', 'triathlon'),
                                                    ),
                                                ),
                                            ),
                                            'portfolio_inner_settings_tab1_bottom_image2' => array(
                                                'type' => 'tab',
                                                'title' => 'Bottom Image 2',
                                                'options' => array(
                                                    'image_or_video2' => array(
                                                        'type' => 'switch',
                                                        'value' => 'image',
                                                        'label' => esc_html__('Image or Video', 'triathlon'),
                                                        'left-choice' => array(
                                                            'value' => 'image',
                                                            'label' => esc_html__('Image', 'triathlon'),
                                                        ),
                                                        'right-choice' => array(
                                                            'value' => 'video',
                                                            'label' => esc_html__('Video', 'triathlon'),
                                                        ),
                                                    ),
                                                    'portfolio_gallery_image2' => array(
                                                        'type' => 'upload',
                                                        'label' => esc_html__('Gallery Bottom Image 2', 'triathlon'),
                                                        'desc' => esc_html__('Add Bottom images for portfolio post which liked to the detail post.', 'triathlon'),
                                                        'images_only' => true,
                                                    ),
                                                    'portfolio_gallery_image2_video' => array(
                                                        'type' => 'text',
                                                        'value' => '',
                                                        'label' => esc_html__('Gallery Bottom Image 2 Video', 'triathlon'),
                                                        'desc' => esc_html__('Add video iframe link if you want to attach video to image 2. <strong>Note: Video will be overwrite if you have define video link at above field.</strong>', 'triathlon'),
                                                    ),
                                                ),
                                            ),
                                            'portfolio_inner_settings_tab1_bottom_image3' => array(
                                                'type' => 'tab',
                                                'title' => 'Bottom Image 3',
                                                'options' => array(
                                                    'image_or_video3' => array(
                                                        'type' => 'switch',
                                                        'value' => 'image',
                                                        'label' => esc_html__('Image or Video', 'triathlon'),
                                                        'left-choice' => array(
                                                            'value' => 'image',
                                                            'label' => esc_html__('Image', 'triathlon'),
                                                        ),
                                                        'right-choice' => array(
                                                            'value' => 'video',
                                                            'label' => esc_html__('Video', 'triathlon'),
                                                        ),
                                                    ),
                                                    'portfolio_gallery_image3' => array(
                                                        'type' => 'upload',
                                                        'label' => esc_html__('Gallery Bottom Image 3', 'triathlon'),
                                                        'desc' => esc_html__('Add Bottom images for portfolio post which liked to the detail post.', 'triathlon'),
                                                        'images_only' => true,
                                                    ),
                                                    'portfolio_gallery_image3_video' => array(
                                                        'type' => 'text',
                                                        'value' => '',
                                                        'label' => esc_html__('Gallery Bottom Image 3 Video', 'triathlon'),
                                                        'desc' => esc_html__('Add video iframe link if you want to attach video to image 3. <strong>Note: Video will be overwrite if you have define video link at above field.</strong>', 'triathlon'),
                                                    ),
                                                ),
                                            ),
                                            'portfolio_inner_settings_tab1_bottom_image4' => array(
                                                'type' => 'tab',
                                                'title' => 'Bottom Image 4',
                                                'options' => array(
                                                    'image_or_video4' => array(
                                                        'type' => 'switch',
                                                        'value' => 'image',
                                                        'label' => esc_html__('Image or Video', 'triathlon'),
                                                        'left-choice' => array(
                                                            'value' => 'image',
                                                            'label' => esc_html__('Image', 'triathlon'),
                                                        ),
                                                        'right-choice' => array(
                                                            'value' => 'video',
                                                            'label' => esc_html__('Video', 'triathlon'),
                                                        ),
                                                    ),
                                                    'portfolio_gallery_image4' => array(
                                                        'type' => 'upload',
                                                        'label' => esc_html__('Gallery Bottom Image 4', 'triathlon'),
                                                        'desc' => esc_html__('Add Bottom images for portfolio post which liked to the detail post.', 'triathlon'),
                                                        'images_only' => true,
                                                    ),
                                                    'portfolio_gallery_image4_video' => array(
                                                        'type' => 'text',
                                                        'value' => '',
                                                        'label' => esc_html__('Gallery Bottom Image 4 Video', 'triathlon'),
                                                        'desc' => esc_html__('Add video iframe link if you want to attach video to image 4. <strong>Note: Video will be overwrite if you have define video link at above field.</strong>', 'triathlon'),
                                                    ),
                                                ),
                                            ),
                                            'portfolio_inner_settings_tab1_bottom_image5' => array(
                                                'type' => 'tab',
                                                'title' => 'Bottom Image 5',
                                                'options' => array(
                                                    'image_or_video5' => array(
                                                        'type' => 'switch',
                                                        'value' => 'image',
                                                        'label' => esc_html__('Image or Video', 'triathlon'),
                                                        'left-choice' => array(
                                                            'value' => 'image',
                                                            'label' => esc_html__('Image', 'triathlon'),
                                                        ),
                                                        'right-choice' => array(
                                                            'value' => 'video',
                                                            'label' => esc_html__('Video', 'triathlon'),
                                                        ),
                                                    ),
                                                    'portfolio_gallery_image5' => array(
                                                        'type' => 'upload',
                                                        'label' => esc_html__('Gallery Bottom Image 5', 'triathlon'),
                                                        'desc' => esc_html__('Add Bottom images for portfolio post which liked to the detail post.', 'triathlon'),
                                                        'images_only' => true,
                                                    ),
                                                    'portfolio_gallery_image5_video' => array(
                                                        'type' => 'text',
                                                        'value' => '',
                                                        'label' => esc_html__('Gallery Bottom Image 5 Video', 'triathlon'),
                                                        'desc' => esc_html__('Add video iframe link if you want to attach video to image 5. <strong>Note: Video will be overwrite if you have define video link at above field.</strong>', 'triathlon'),
                                                    ),
                                                ),
                                            ),
                                        ),
                                    ),
                                ),
                            ),
                            'portfolio_inner_settings_tab2' => array(
                                'type' => 'tab',
                                'title' => 'Video',
                                'options' => array(
                                    'video' => array(
                                        'type' => 'text',
                                        'value' => '',
                                        'label' => esc_html__('Iframe Video Link', 'triathlon'),
                                        'desc' => esc_html__('Add video iframe link if you want to use video instead of Gallery. Please check video in gallery type settings.', 'triathlon'),
                                    ),
                                ),
                            )
                        ),
                    ),
                )
            ),
        )
    )
);

