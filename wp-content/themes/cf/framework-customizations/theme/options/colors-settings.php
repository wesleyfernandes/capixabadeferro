<?php

if (!defined('FW')) {
    die('Forbidden');
}
$options = array(
    'colors' => array(
        'title' => esc_html__('Styling Options', 'triathlon'),
        'type' => 'tab',
        'options' => array(
            'general-box' => array(
                'title' => esc_html__('Styling Options', 'triathlon'),
                'type' => 'box',
                'options' => array(
                    'color_settings' => array(
                        'type' => 'multi-picker',
                        'label' => false,
                        'desc' => '',
                        'picker' => array(
                            'gadget' => array(
                                'label' => esc_html__('Styling Options', 'triathlon'),
                                'type' => 'switch',
                                'left-choice' => array(
                                    'value' => 'default',
                                    'label' => esc_html__('Default Color', 'triathlon')
                                ),
                                'right-choice' => array(
                                    'value' => 'custom',
                                    'label' => esc_html__('Custom Color', 'triathlon')
                                ),
                                'value' => 'disbale',
                            )
                        ),
                        'choices' => array(
                            'custom' => array(
                                'primary_color' => array(
                                    'type' => 'color-picker',
                                    'value' => '#f44029',
                                    'attr' => array(),
                                    'label' => esc_html__('Primary Color', 'triathlon'),
                                    'desc' => esc_html__('Add theme primary color.', 'triathlon'),
                                    'help' => esc_html__('', 'triathlon'),
                                ),
								'secondary_color'      => array (
									'type'  => 'color-picker' ,
									'value' => '#fdb62d' ,
									'attr'  => array () ,
									'label' => esc_html__('Secondary Color' , 'triathlon') ,
									'desc'  => esc_html__('Add theme secondary color.' , 'triathlon') ,
									'help'  => esc_html__('' , 'triathlon') ,
								) ,
                            ),
                            'default' => array(),
                        ),
                        'show_borders' => false,
                    ),
                )
            ),
        )
    )
);
