<?php

if (!defined('FW')) {
    die('Forbidden');
}

$options = array(
    'headers' => array(
        'title' => esc_html__('Headers', 'triathlon'),
        'type' => 'tab',
        'options' => array(
            'general-box' => array(
                'title' => esc_html__('Header Settings', 'triathlon'),
                'type' => 'box',
                'options' => array(
                    'main_logo' => array(
                        'type' => 'upload',
                        'label' => esc_html__('Upload Logo', 'triathlon'),
                        'desc' => esc_html__('Upload Your Logo Here the preferred size is 252 by 63.', 'triathlon'),
                        'images_only' => true,
                    ),
                    'logo_margin_top' => array(
                        'type' => 'slider',
                        'value' => 0,
                        'properties' => array(
                            'min' => -200,
                            'max' => 200,
                            'sep' => 1,
                        ),
                        'label' => esc_html__('Logo Margin Top', 'triathlon'),
                    ),
                    'logo_margin_bottom' => array(
                        'type' => 'slider',
                        'value' => 0,
                        'properties' => array(
                            'min' => -200,
                            'max' => 200,
                            'sep' => 1,
                        ),
                        'label' => esc_html__('Logo Margin Bottom', 'triathlon'),
                    ),
                    'enable_search' => array(
                        'type' => 'switch',
                        'value' => 'on',
                        'attr' => array(),
                        'label' => esc_html__('Enable / Disable search', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                        'left-choice' => array(
                            'value' => 'off',
                            'label' => esc_html__('Off', 'triathlon'),
                        ),
                        'right-choice' => array(
                            'value' => 'on',
                            'label' => esc_html__('ON', 'triathlon'),
                        ),
                    ),
                    'enable_top_strip' => array(
                        'type' => 'switch',
                        'value' => 'off',
                        'attr' => array(),
                        'label' => esc_html__('Enable / Disable top strip', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                        'left-choice' => array(
                            'value' => 'off',
                            'label' => esc_html__('Off', 'triathlon'),
                        ),
                        'right-choice' => array(
                            'value' => 'on',
                            'label' => esc_html__('ON', 'triathlon'),
                        ),
                    ),
                    'enable_language' => array(
                        'type' => 'switch',
                        'value' => 'off',
                        'attr' => array(),
                        'label' => esc_html__('Enable / Disable languages', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                        'left-choice' => array(
                            'value' => 'off',
                            'label' => esc_html__('Off', 'triathlon'),
                        ),
                        'right-choice' => array(
                            'value' => 'on',
                            'label' => esc_html__('ON', 'triathlon'),
                        ),
                    ),
                    'enable_top_menu' => array(
                        'type' => 'switch',
                        'value' => 'off',
                        'attr' => array(),
                        'label' => esc_html__('Enable / Disable Top menu', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                        'left-choice' => array(
                            'value' => 'off',
                            'label' => esc_html__('Off', 'triathlon'),
                        ),
                        'right-choice' => array(
                            'value' => 'on',
                            'label' => esc_html__('ON', 'triathlon'),
                        ),
                    ),
                    'enable_social_icons' => array(
                        'type' => 'switch',
                        'value' => 'off',
                        'attr' => array(),
                        'label' => esc_html__('Enable / Disable Social Icons', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                        'left-choice' => array(
                            'value' => 'off',
                            'label' => esc_html__('Off', 'triathlon'),
                        ),
                        'right-choice' => array(
                            'value' => 'on',
                            'label' => esc_html__('ON', 'triathlon'),
                        ),
                    ),
                )
            ),
        )
    )
);
