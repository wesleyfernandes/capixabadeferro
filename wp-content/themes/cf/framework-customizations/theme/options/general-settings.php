<?php

if (!defined('FW')) {
    die('Forbidden');
}

$options = array(
    'general' => array(
        'title' => esc_html__('General', 'triathlon'),
        'type' => 'tab',
        'options' => array(
            'general-box' => array(
                'title' => esc_html__('General Settings', 'triathlon'),
                'type' => 'box',
                'options' => array(
                    'theme_type' => array(
                        'type' => 'select',
                        'value' => 'sport',
                        'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
                        'label' => esc_html__('Theme Skin', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                        'help' => esc_html__('', 'triathlon'),
                        'choices' => array(
                            'sport' => esc_html__('Light', 'triathlon'),
                            'gym' => esc_html__('Dark', 'triathlon'),
                        ),
                    ),
                    'favicon' => array(
                        'label' => esc_html__('Favicon', 'triathlon'),
                        'desc' => esc_html__('Upload a favicon image', 'triathlon'),
                        'type' => 'upload'
                    ),
                    '404_message' => array(
                        'type' => 'text',
                        'value' => esc_html__('', 'triathlon'),
                        'label' => esc_html__('404 Error Message', 'triathlon'),
                    ),
                    'custom_css' => array(
                        'type' => 'textarea',
                        'label' => esc_html__('Custom CSS', 'triathlon'),
                        'desc' => esc_html__('Add your custom css code here if you want to target specifically on different elements.', 'triathlon'),
                    ),
                )
            ),
        )
    )
);
