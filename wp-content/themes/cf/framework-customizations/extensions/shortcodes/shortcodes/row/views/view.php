<?php if (!defined('FW')) die('Forbidden'); ?>
<?php
	$tg_row_start	 	= '<div class="row">';
	$tg_row_end	 	 	= '</div>';
	$tg_sidebar	 	 = 'full';
	if (function_exists('fw_ext_sidebars_get_current_position')) {
		$current_position = fw_ext_sidebars_get_current_position();
		if( $current_position !== 'full' &&  ( $current_position == 'left' || $current_position == 'right' ) ) {
			$tg_row_start	 	= '';
			$tg_row_end	 	 	= '';
		}
	}
?>
<?php echo triathlon_esc_specialchars( $tg_row_start );?>
	<?php echo do_shortcode($content); ?>
<?php echo triathlon_esc_specialchars( $tg_row_end );?>

