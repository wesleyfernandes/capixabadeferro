<?php

if (!defined('FW')) {
    die('Forbidden');
}

$options = array(
    'event_title' => array(
        'type' => 'text',
        'value' => 'Event Listings',
        'label' => esc_html__('Event Title', 'triathlon'),
        'desc' => esc_html__('', 'triathlon'),
    ),
    'show_posts' => array(
        'type' => 'slider',
        'value' => 10,
        'properties' => array(
            'min' => 0,
            'max' => 20,
            'sep' => 1,
        ),
        'label' => esc_html__('Show No of Posts', 'triathlon'),
    ),
    'event_type' => array(
        'type' => 'select',
        'value' => 'upcoming',
        'label' => esc_html__('Show Events', 'triathlon'),
        'desc' => esc_html__('', 'triathlon'),
        'choices' => array(
            'all' => esc_html__('All', 'triathlon'),
            'upcoming' => esc_html__('Upcoming', 'triathlon'),
            'past' => esc_html__('Past', 'triathlon'),
        ),
        'no-validate' => false,
    ),
    'show_pagination' => array(
        'type' => 'select',
        'value' => 'yes',
        'label' => esc_html__('Show Pagination', 'triathlon'),
        'desc' => esc_html__('', 'triathlon'),
        'choices' => array(
            'yes' => esc_html__('Yes', 'triathlon'),
            'no' => esc_html__('No', 'triathlon'),
        ),
        'no-validate' => false,
    ),
    'show_description' => array(
        'type' => 'switch',
        'value' => 'show',
        'label' => esc_html__('Description', 'triathlon'),
        'desc' => esc_html__('', 'triathlon'),
        'left-choice' => array(
            'value' => 'show',
            'label' => esc_html__('Show Description', 'triathlon'),
        ),
        'right-choice' => array(
            'value' => 'hide',
            'label' => esc_html__('Hide Description', 'triathlon'),
        ),
    ),
    'order' => array(
        'type' => 'select',
        'value' => 'DESC',
        'desc' => esc_html__('Select post order', 'triathlon'),
        'label' => esc_html__('Posts order', 'triathlon'),
        'choices' => array(
            'ASC' => esc_html__('ASC', 'triathlon'),
            'DESC' => esc_html__('DESC', 'triathlon'),
        ),
    ),
    'orderby' => array(
        'type' => 'select',
        'value' => 'ID',
        'desc' => esc_html__('Select post order By', 'triathlon'),
        'label' => esc_html__('Posts order By', 'triathlon'),
        'choices' => array(
            'ID' => esc_html__('Order by post id', 'triathlon'),
            'author' => esc_html__('Order by author', 'triathlon'),
            'title' => esc_html__('Order by title', 'triathlon'),
            'name' => esc_html__('Order by post name', 'triathlon'),
            'date' => esc_html__('Order by post date', 'triathlon'),
            'rand' => esc_html__('Random order', 'triathlon'),
            'comment_count' => esc_html__('Order by number of comments', 'triathlon'),
            'meta_value' => esc_html__('Order by event start date', 'triathlon'),
        ),
    ),
);
