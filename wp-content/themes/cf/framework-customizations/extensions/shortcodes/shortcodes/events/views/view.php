<?php
if (!defined('FW'))
    die('Forbidden');
/**
 * @var $atts
 */
$width = '370';
$height = '155';
if (isset($atts['event_title']) && !empty($atts['event_title'])) {
    ?>
    <div class="head-section">
        <div class="border-title"><h2><?php esc_html_e('Event Listings', 'triathlon'); ?></h2></div>
        <?php if (isset($atts['event_type'])) { ?>
            <span class="title"><?php echo ucwords(esc_attr($atts['event_type'])); ?> <?php esc_html_e(' Events', 'triathlon'); ?></span>
        <?php } ?>
    </div>
<?php } ?>
<div class="row">
    <div class="panel-group events" id="accordion" role="tablist" aria-multiselectable="true">
        <?php
        global $paged;
        if (empty($paged))
            $paged = 1;

        // Count Total Pssts
        $show_posts = !empty($atts['show_posts']) ? $atts['show_posts'] : '-1';
        $order = !empty($atts['order']) ? $atts['order'] : 'DESC';
        $orderby = !empty($atts['orderby']) ? $atts['orderby'] : 'ID';

        $meta_query = array('relation' => 'AND',);

        if ($atts['event_type'] == 'upcoming') {
            $meta_query[] = array(
                'key' => 'start_date',
                'value' => date("Y-m-d"),
                'compare' => '>=',
                'type' => 'NUMERIC,'
            );
        } elseif ($atts['event_type'] == 'past') {
            $meta_query[] = array(
                'key' => 'start_date',
                'value' => date("Y-m-d"),
                'compare' => '<=',
                'type' => 'NUMERIC,'
            );
        }

        if (is_array($meta_query) && count($meta_query) > 1) {
            $args['meta_query'] = $meta_query;
        }

        $args = array('posts_per_page' => "-1",
            'post_type' => 'tg_events',
            'order' => $order,
            'orderby' => $orderby,
            'post_status' => 'publish',
            'ignore_sticky_posts' => 1
        );

        $query = new WP_Query($args);
        $count_post = $query->post_count;

        //Main Query	
        $args = array('posts_per_page' => $show_posts,
            'post_type' => 'tg_events',
            'paged' => $paged,
            'order' => $order,
            'orderby' => $orderby,
            'post_status' => 'publish',
            'ignore_sticky_posts' => 1
        );

        if (is_array($meta_query) && count($meta_query) > 1) {
            $args['meta_query'] = $meta_query;
        }

        if (isset($orderby) && $orderby === 'meta_value') {
            $args['meta_key'] = 'start_date';
        }

        //fw_print($args);
        $query = new WP_Query($args);
        $tg_counter = 0;
        while ($query->have_posts()) : $query->the_post();
            global $post;
            $random_id = fw_unique_increment();
            $tg_counter++;
            $country_flag = fw_get_db_post_option($post->ID, 'country_flag', true);
            $book_now_url = fw_get_db_post_option($post->ID, 'book_now_url', true);
            $book_now_text = fw_get_db_post_option($post->ID, 'book_now_text', true);
            $contact_email = fw_get_db_post_option($post->ID, 'contact_email', true);
            $contact_phone = fw_get_db_post_option($post->ID, 'contact_phone', true);
            $event_location = fw_get_db_post_option($post->ID, 'event_location', true);
            $start_date = fw_get_db_post_option($post->ID, 'start_date', true);
            $end_date = fw_get_db_post_option($post->ID, 'end_date', true);
            $booking_last_day = fw_get_db_post_option($post->ID, 'booking_last_day', true);
            $entry_fee = fw_get_db_post_option($post->ID, 'entry_fee', true);
            $prize = fw_get_db_post_option($post->ID, 'prize', true);
            $extras = fw_get_db_post_option($post->ID, 'extras', true);
            $disabled = '';
            if (isset($atts['event_type']) && ( $atts['event_type'] == 'past' || $atts['event_type'] == 'all' )) {
                if (strtotime(date('Y-m-d')) > strtotime($start_date)) {
                    $disabled = 'disabled';
                }
            }

            $tg_active = '';
            if ($tg_counter === 1) {
                $tg_active = 'in';
            }

            $book_now_text = isset($book_now_text) && !empty($book_now_text) ? $book_now_text : 'Book Now';
            ?>


            <div class="panel panel-default event <?php echo esc_attr($disabled); ?>">
                <div class="panel-heading" role="tab" id="headingOne-<?php echo esc_attr($random_id); ?>">
                    <a class="accordion-head" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne-<?php echo esc_attr($random_id); ?>" aria-expanded="true" >

                        <div>
                            <?php if (!empty($start_date)) { ?>
                                <span class="date">
                                    <i class="fa fa-calendar-o"></i>
                                    <em><?php echo date_i18n(get_option('date_format'), strtotime($start_date)); ?></em>
                                </span>
                            <?php } ?>

                            <?php if (isset($country_flag) && !empty($country_flag['url'])) { ?>
                                <span class="flag"><img src="<?php echo esc_url($country_flag['url']); ?>" alt="<?php echo esc_attr(get_the_title()); ?>"></span>
                            <?php } ?>
                        </div>

                        <div>
                            <h3><?php the_title(); ?></h3>
                            <?php
                            $terms = get_the_terms($query->ID, 'event_categories');
                            if ($terms && !is_wp_error($terms)) {
                                ?>
                                <ul>
                                    <?php foreach ($terms as $term) { ?>
                                        <li><?php echo esc_attr($term->name); ?></li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        </div>
                        <?php if (isset($disabled) && $disabled == 'disabled') { ?>
                            <div class="unavailable">
                                <span class="notice"><?php esc_html_e('Unavailable', 'triathlon'); ?></span>
                            </div>
                        <?php } ?>
                    </a>
                    <a href="<?php echo esc_url($book_now_url); ?>" class="btn-theme red btn-book-now">
                        <span class="txt"><?php echo esc_attr($book_now_text); ?></span>
                        <span class="round">
                            <i class="icon-arrow-right-latest-races"></i>
                        </span>
                    </a>
                </div>
                <div id="collapseOne-<?php echo esc_attr($random_id); ?>" class="panel-collapse collapse <?php echo esc_attr($tg_active); ?>" role="tabpanel" aria-labelledby="headingOne-<?php echo esc_attr($random_id); ?>">
                    <div class="panel-body">
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <table class="table theme-table">
                                <thead>
                                    <tr>
                                        <th><strong><?php esc_html_e('Contact Information', 'triathlon'); ?></strong></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($event_location)) { ?>
                                        <tr>
                                            <td><strong><?php esc_html_e('Event Location: ', 'triathlon'); ?></strong><?php echo esc_attr($event_location); ?></td>

                                        </tr>
                                    <?php } ?>
                                    <?php if (isset($start_date) && !empty($start_date)) { ?>
                                        <tr>
                                            <td><strong><?php esc_html_e('Start date: ', 'triathlon'); ?></strong><?php echo date(get_option('date_format'), strtotime($start_date)); ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if (isset($end_date) && !empty($end_date)) { ?>
                                        <tr>
                                            <td><strong><?php esc_html_e('End date: ', 'triathlon'); ?></strong><?php echo date(get_option('date_format'), strtotime($end_date)); ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if (isset($booking_last_day) && !empty($booking_last_day)) { ?>
                                        <tr>
                                            <td><strong><?php esc_html_e('Last day to book by:', 'triathlon'); ?></strong><?php echo date(get_option('date_format'), strtotime($booking_last_day)); ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if (isset($contact_email) && !empty($contact_email)) { ?>
                                        <tr>
                                            <td><strong><?php esc_html_e('Email: ', 'triathlon'); ?></strong> <?php echo esc_attr($contact_email); ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if (isset($contact_phone) && !empty($contact_phone)) { ?>
                                        <tr>
                                            <td><strong><?php esc_html_e('Phone: ', 'triathlon'); ?></strong> <?php echo esc_attr($contact_phone); ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-8 col-sm-12 col-xs-12">
                            <table class="table theme-table">
                                <thead>
                                    <tr>
                                        <th><strong><?php esc_html_e('Event Information', 'triathlon'); ?></strong></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (isset($entry_fee) && !empty($entry_fee)) { ?>
                                        <tr>
                                            <td><strong><?php esc_html_e('Entry Fee: ', 'triathlon'); ?></strong><?php echo esc_attr($entry_fee); ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if (isset($prize) && !empty($prize)) { ?>
                                        <tr>
                                            <td><strong><?php esc_html_e('Money Prize: ', 'triathlon'); ?></strong><?php echo esc_attr($prize); ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if (isset($atts['show_description']) && $atts['show_description'] == 'show') { ?>
                                        <tr>
                                            <td><strong><?php esc_html_e('Description: ', 'triathlon'); ?></strong><?php the_content(); ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php
                                    if (isset($extras) && !empty($extras)) {
                                        foreach ($extras as $key => $value) {
                                            ?>
                                            <tr>
                                                <td><strong><?php echo esc_attr($value['tab_title'] . ': '); ?></strong><?php echo esc_attr($value['tab_content']); ?></td>
                                            </tr>
                                        <?php
                                        }
                                    }
                                    ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        endwhile;
        wp_reset_postdata();
        ?>
    </div>
    <?php
    if (isset($atts['show_pagination']) && $atts['show_pagination'] == 'yes') :
        triathlon_prepare_pagination($count_post, $show_posts);
    endif;
    ?>
</div>