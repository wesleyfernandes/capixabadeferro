<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
$gl_catgories['']	= esc_html__('Select Category','triathlon');
$taxonomy = 'gallery_categories';
$terms = get_terms($taxonomy); 
if ( $terms && !is_wp_error( $terms ) ) {
 foreach ( $terms as $term ) { 
 	$gl_catgories[$term->slug]	=  $term->name;;
 }
}

$options = array(
	'heading' => array(
		'type'  => 'text',
		'value' => '',
		'label' => esc_html__('Gallery Title', 'triathlon'),
		'desc'  => esc_html__('', 'triathlon'),
	),
	'select_categories' => array(
		'type'  => 'select',
		'value' => 'yes',
		'label' => esc_html__('Select Category', 'triathlon'),
		'desc'  => esc_html__('', 'triathlon'),
		'choices' => $gl_catgories,
		'no-validate' => false,
	),
	'show_posts' => array(
		'type'  => 'slider',
		'value' => 9,
		'properties' => array(

			'min' => 0,
			'max' => 100,
			'sep' => 1,

		),
		'label' => esc_html__('Show No of Posts', 'triathlon'),
	),
	'show_pagination' => array(
		'type'  => 'select',
		'value' => 'yes',
		'label' => esc_html__('Show Pagination', 'triathlon'),
		'desc'  => esc_html__('', 'triathlon'),
		'choices' => array(
			'yes' => esc_html__('Yes', 'triathlon'),
			'no' => esc_html__('No', 'triathlon'),
		),
		'no-validate' => false,
	),
	'show_filterable' => array(
		'type'  => 'switch',
		'value' => 'show',
		'label' => esc_html__('Filterable', 'triathlon'),
		'desc'  => esc_html__('It will work in the case of all categories', 'triathlon'),
		'left-choice' => array(
			'value' => 'show',
			'label' => esc_html__('Show Filterable', 'triathlon'),
		),
		'right-choice' => array(
			'value' => 'hide',
			'label' => esc_html__('Hide Filterable', 'triathlon'),
		),
	),
);