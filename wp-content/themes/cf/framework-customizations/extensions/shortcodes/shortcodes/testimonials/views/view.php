<?php
if (!defined('FW')) {
    die('Forbidden');
}
/**
 * @var $atts
 */
?>
<?php
/**
 * Get All Shortcode Attributes
 */
$uniq_flag = fw_unique_increment();
?>
<div class="testimonials">
	<?php if(isset($atts['heading']) && !empty($atts['heading'])){?>
	<div class="head-section">
		<div class="border-title">
			<h3><?php echo esc_attr( $atts['heading'] ); ?></h3>
		</div>
	</div>
	<?php }?>
	<div class="testimonial-slider testimonial-<?php echo esc_attr($uniq_flag); ?>">
		<?php
		/**
		 * Loop Through The Testimonial Items
		 */
		if (isset($atts['testimonials_add'])) :
			foreach ($atts['testimonials_add'] as $testimonials) :
				$title = (isset($testimonials['testimonial_title']) && !empty($testimonials['testimonial_title'])) ? $testimonials['testimonial_title'] : '';
				$author = (isset($testimonials['testimonial_author']) && !empty($testimonials['testimonial_author'])) ? $testimonials['testimonial_author'] : '';
				$content = (isset($testimonials['testimonial_content']) && !empty($testimonials['testimonial_content'])) ? '<p>'.$testimonials['testimonial_content'].'</p>' : '';
				$icon = (isset($testimonials['testimonial_icon']) && !empty($testimonials['testimonial_icon'])) ? $testimonials['testimonial_icon'] : '';
				?>
				<div class="item">
					<i class="<?php echo esc_attr($icon); ?>"></i>
					<span class="title"><?php echo esc_attr($title); ?></span>
					<strong class="client-name"><?php echo esc_attr($author); ?></strong>
					<div class="description">
						<?php echo force_balance_tags( $content ); ?>
					</div>
				</div>
	
			<?php
			endforeach;
		endif;
		?>
	</div>
	<script>
		jQuery(document).ready(function (e) {
			jQuery(".testimonial-<?php echo esc_js($uniq_flag); ?>").owlCarousel({
				autoPlay: 10000,
				navigation: true,
				pagination: false,
				paginationSpeed: 1000,
				singleItem: true,
				stopOnHover:true
			});

		});
	</script>
</div>