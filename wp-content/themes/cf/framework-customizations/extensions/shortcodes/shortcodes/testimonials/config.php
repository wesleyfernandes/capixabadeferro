<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg['page_builder'] = array(
	'title'       => esc_html__( 'Testimonials', 'triathlon' ),
	'description' => esc_html__( 'Add some Testimonials', 'triathlon' ),
	'tab'         => esc_html__( 'Triathlon', 'triathlon' ),
);