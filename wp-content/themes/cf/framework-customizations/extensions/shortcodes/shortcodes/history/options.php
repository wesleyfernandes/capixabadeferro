<?php

if (!defined('FW')) {
    die('Forbidden');
}

$options = array(
    'heading' => array(
        'type' => 'text',
        'label' => esc_html__('Heading', 'triathlon'),
    ),
    'sub_heading' => array(
        'type' => 'text',
        'label' => esc_html__('Sub Heading', 'triathlon'),
    ),
    'description' => array(
        'type' => 'wp-editor',
        'value' => '',
        'attr' => array(),
        'label' => esc_html__('Description', 'triathlon'),
        'desc' => esc_html__('Add Detail Description For Your History', 'triathlon'),
        'tinymce' => true,
        'media_buttons' => true,
        'teeny' => true,
        'wpautop' => false,
        'editor_css' => '',
        'reinit' => true,
        'size' => 'small',
        'editor_type' => 'tinymce',
        'editor_height' => 250
    ),
    'gallery' => array(
        'type' => 'multi-upload',
        'attr' => array(),
        'label' => esc_html__('Gallery', 'triathlon'),
        'desc' => esc_html__('Add Gallery Images For Your History', 'triathlon'),
        'images_only' => true,
    ),
);
