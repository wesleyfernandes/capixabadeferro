<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'textblock_title'  => array(
        'type'  => 'text',
        'label' => esc_html__('Title', 'triathlon'),
    ),
	'textblock_sub_title'  => array(
        'type'  => 'text',
        'label' => esc_html__('Sub Title', 'triathlon'),
    ),
    'textblock_description'  => array(
        'type'  => 'wp-editor',
        'value' => 'default value',
        'attr'  => array(),
        'label' => esc_html__('Label', 'triathlon'),
        'desc'  => esc_html__('Description', 'triathlon'),
        'help'  => esc_html__('Help tip', 'triathlon'),
        'tinymce' => true,
        'media_buttons' => true,
        'teeny' => true,
        'wpautop' => false,
        'editor_css' => '',
        'reinit' => true,
        'size' => 'small', // small | large
        'editor_type' => 'tinymce',
        'editor_height' => 400
    ),
   'textblock_button_text'  => array(
        'type'  => 'text',
		'value'  => 'Book Now',
        'label' => esc_html__('Buuton Title', 'triathlon'),
		'desc'  => esc_html__('Leave it empty to hide button', 'triathlon'),
    ),
	'textblock_button_link'  => array(
        'type'  => 'text',
		'value'  => '#',
        'label' => esc_html__('Button Link', 'triathlon'),
    ),

	
);