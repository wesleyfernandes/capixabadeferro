<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
$options = array(
    'section_title' => array(
        'type'  => 'text',
        'label' => esc_html__('Section Title', 'triathlon'),
        'desc'  => esc_html__( '', 'triathlon' ),
    ),
	'services_title' => array(
        'type'  => 'text',
        'label' => esc_html__('Service Title', 'triathlon'),
        'desc'  => esc_html__( 'Enter Your Service Title.','triathlon' ),
    ),
	'services_sub_title' => array(
        'type'  => 'text',
        'label' => esc_html__('Service Sub Title', 'triathlon'),
        'desc'  => esc_html__( 'Enter Your Service Sub Title.','triathlon' ),
    ),
    'services_description' => array(
        'type'  => 'textarea',
        'label' => esc_html__('Service Descriotion', 'triathlon'),
        'desc'  => esc_html__('Enter Your Service Desription Here.', 'triathlon'),
    ),
    'services_icon' => array(
        'type' => 'icon',
		'value' => 'fa-smile-o',
		'attr' => array(),
		'label' => esc_html__('Icon', 'triathlon'),
		'desc' => esc_html__('Choose Icon', 'triathlon'),
    ),
	'services_button_text' => array(
        'type'  => 'text',
		'value'  => 'Learn More',
        'label' => esc_html__('Button Title', 'triathlon'),
        'desc'  => esc_html__( 'Leave empty to hide button', 'triathlon' ),
    ),
	'services_button_link' => array(
        'type'  => 'text',
		'value'  => '#',
        'label' => esc_html__('Button Link', 'triathlon'),
        'desc'  => esc_html__( '','triathlon' ),
    ),
);