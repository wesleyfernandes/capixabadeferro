<?php if (!defined('FW')) die( 'Forbidden' );
/**
 * @var $atts
 */
?>

<div class="three-columns">
	<?php if(isset($atts['section_title']) && $atts['section_title'] != '') { ?>
	<div class="border-title">
		<strong><?php echo esc_attr($atts['section_title']); ?></strong>
	</div>
	<?php } ?>
	<?php if(isset($atts['services_icon']) && $atts['services_icon'] != '') { ?>
		<i class="icon <?php echo esc_attr($atts['services_icon']);?>"></i>
	<?php } ?>
	<?php if(isset($atts['services_title']) && $atts['services_title'] != '') { ?>
		<h3><?php echo esc_attr($atts['services_title']);?></h3>
	<?php } ?>
	<?php if(isset($atts['services_sub_title']) && $atts['services_sub_title'] != '') { ?>
		<span class="athlete-naem"><?php echo esc_attr($atts['services_sub_title']);?></span>
	<?php } ?>
	<?php if(isset($atts['services_description']) && $atts['services_description'] != '') { ?>
	<div class="description">
		<p><?php echo esc_attr($atts['services_description']);?></p>
	</div>
	<?php } ?>
	<?php if(isset($atts['services_button_text']) && $atts['services_button_text'] != '') { ?>
	<a href="<?php echo esc_attr($atts['services_button_link']); ?>" class="btn-theme black learn-more">
		<span class="txt"><?php echo esc_attr($atts['services_button_text']); ?></span>
		<span class="round">
			<i class="icon-arrow-right-latest-races"></i>
		</span>
	</a>
	<?php } ?>
</div>