<?php
if (!defined('FW'))
    die('Forbidden');
/**
 * @var $atts
 */
?>
<div class="blog-post row">
    <?php
    global $paged;
    if (empty($paged))
        $paged = 1;

    $blog_view = $atts['blog_view'];
    // Count Total Pssts

    if (isset($blog_view) && $blog_view == 6) {
        $width = '430';
        $height = '200';
    } else if (isset($blog_view) && $blog_view == 4) {
        $width = '370';
        $height = '175';
    } else if (isset($blog_view) && $blog_view == 12) {
        $width = '1140';
        $height = '289';
    } else {
        $blog_view = 4;
        $width = '370';
        $height = '175';
    }

    $show_posts = $atts['show_posts'] ? $atts['show_posts'] : '-1';
    $args = array('posts_per_page' => "-1", 'post_type' => 'post', 'order' => 'DESC', 'orderby' => 'ID', 'post_status' => 'publish', 'ignore_sticky_posts' => 1);
    $query = new WP_Query($args);
    $count_post = $query->post_count;

    //Main Query	
    $args = array('posts_per_page' => $atts['show_posts'], 'post_type' => 'post', 'paged' => $paged, 'order' => 'DESC', 'orderby' => 'ID', 'post_status' => 'publish', 'ignore_sticky_posts' => 1);

    $query = new WP_Query($args);
    while ($query->have_posts()) : $query->the_post();

        $thumnail = triathlon_prepare_thumbnail($query->ID, $width, $height);

        if (isset($thumnail) && $thumnail) {
            $thumnail = $thumnail;
        } else {
            $thumnail = get_template_directory_uri() . '/img/no-image.png';
        }

        if (!function_exists('fw_get_db_post_option')) {
            return;
        } else {
            $blog_settings = fw_get_db_post_option($query->ID, 'blog_settings', true);
            $blog_post_video_custom = fw_get_db_post_option($query->ID, 'blog_video_link', true);
            $blog_standard_image = fw_get_db_post_option(get_the_ID(), 'blog_standard_image', true);
        }
        ?>
        <article class="post col-md-<?php echo intval($blog_view); ?> col-sm-6 col-xs-12">
            <?php if (isset($blog_settings) && $blog_settings == 'video' && isset($blog_post_video_custom)) { ?>
                <div class="block-list video-click">
                    <div class="post-img">
                        <img src="<?php echo esc_url($thumnail); ?>" alt="<?php echo esc_attr(get_the_title());?>">

                        <iframe src="<?php echo esc_url($blog_post_video_custom); ?>"></iframe>
                    </div>
                    <?php if (isset($atts['show_author']) && $atts['show_author'] == 'show') : ?>
                        <ul class="post-meta">
                            <li><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('nickname'); ?></a> <?php esc_html_e('Admin', 'triathlon'); ?></li>
                        </ul>
                    <?php endif; ?>
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

                    <?php if (isset($atts['show_description']) && $atts['show_description'] == 'show') { ?>
                        <div class="description"><p><?php triathlon_prepare_excerpt($atts['excerpt_length'], 'false', ''); ?></p></div>
                    <?php } ?>

                </div>
            <?php } else { ?>
                <div class="post-img">
                    <a href="<?php the_permalink(); ?>"><img src="<?php echo esc_url($thumnail); ?>" alt="<?php echo esc_attr(get_the_title());?>"></a>
                </div>
                <?php if (isset($atts['show_author']) && $atts['show_author'] == 'show') : ?>
                    <ul class="post-meta">
                        <li><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('nickname'); ?></a> <?php esc_html_e('Admin', 'triathlon'); ?></li>
                    </ul>
                <?php endif; ?>
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <?php if (isset($atts['show_description']) && $atts['show_description'] == 'show') { ?>
                    <div class="description"><p><?php triathlon_prepare_excerpt($atts['excerpt_length'], 'false', ''); ?></p></div>
                        <?php } ?>
                    <?php } ?>
        </article>
    <?php
    endwhile;
    wp_reset_postdata();
    ?>

    <?php if (isset($atts['show_pagination']) && $atts['show_pagination'] == 'yes') : ?>
        <?php triathlon_prepare_pagination($count_post, $atts['show_posts']); ?>
<?php endif; ?>
</div>