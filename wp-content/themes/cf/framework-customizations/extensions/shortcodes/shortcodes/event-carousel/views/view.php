<?php
if (!defined('FW'))
    die('Forbidden');
/**
 * @var $atts
 */
/**
 * Get Shortcodes Attributes
 */
$uni_flag = fw_unique_increment();
?>

<div class="latest-race haslayout">
    <div class="race-area row">
        <div class="col-sm-7">
            <?php if (isset($atts['latest_heading']) && !empty($atts['latest_heading'])) { ?>
                <div class="border-title">
                    <h3><?php echo esc_attr($atts['latest_heading']); ?></h3>
                </div>
            <?php } ?>
            <div class="race-slider races-<?php echo esc_attr($uni_flag); ?>">
                <?php
                $show_posts = $atts['total_up_races'] ? $atts['total_up_races'] : '-1';

                $meta_query[] = array(
                    'key' => 'start_date',
                    'value' => date("Y-m-d"),
                    'compare' => '>=',
                    'type' => 'NUMERIC,'
                );
                $args = array('posts_per_page' => $show_posts, 'post_type' => 'tg_events', 'order' => 'DESC', 'orderby' => 'ID', 'post_status' => 'publish', 'ignore_sticky_posts' => 1);

                if (is_array($meta_query) && count($meta_query) > 1) {
                    $args['meta_query'] = $meta_query;
                }
                $query = new WP_Query($args);
                while ($query->have_posts()) : $query->the_post(); 
                    global $post;
                    echo "<a href='".get_permalink($post->ID)."'>Link</a>" ;
                    $start_date = fw_get_db_post_option($post->ID, 'start_date', true);
                    ?>
                    <div class="item">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <strong class="date me"><?php echo date_i18n('d/m', strtotime($start_date)); ?></strong> <span class="venue"><?php the_title(); ?></span>
                            <div class="description">
                                <?php triathlon_prepare_excerpt(120, 'false', ''); ?>
                            </div>
                        </div>
                    </div>
                    <?php
                endwhile;
                ?>
            </div>
            <a <?php echo (isset($atts['latest_link']) && !empty($atts['latest_link'])) ? 'href="' . esc_url($atts['latest_link']) . '"' : 'href="#"'; ?> class="btn-theme black">
                <?php if (isset($atts['latest_text']) && !empty($atts['latest_text'])) { ?>
                    <span class="txt"><?php echo esc_attr($atts['latest_text']); ?></span>
                <?php } ?>
                <span class="round">
                    <i class="icon-arrow-right-latest-races"></i>
                </span>
            </a>     
        </div>
        <?php if (isset($atts['up_race_image']['url']) && is_array($atts['up_race_image'])) { ?>
            <div class="col-sm-5">
                <img src="<?php echo esc_url($atts['up_race_image']['url']); ?>" alt="<?php echo get_bloginfo('name'); ?>">
            </div>
        <?php } ?>

    </div>
</div>
<script>
    jQuery(document).ready(function () {
        jQuery(".races-<?php echo esc_js($uni_flag); ?>").owlCarousel({
            autoPlay: false,
            items: 2,
            navigation: true,
            itemsDesktop: [1199, 2],
            itemsDesktopSmall: [979, 2],
            pagination: false,
        });
    });

</script>