<?php
if (!defined('FW')) {
    die('Forbidden');
}
/**
 * @var $atts
 */
?>
<?php
/**
 * Get All Shortcodes Attributes
 */
$heading = (!empty($atts['heading'])) ? esc_attr($atts['heading']) : '';
$sub_heading = (!empty($atts['sub_heading'])) ? esc_attr($atts['sub_heading']) : '';
$team_persons = (is_array($atts['add_team_person'])) ? $atts['add_team_person'] : '';
?>
<div class="team">
<?php if (isset($heading) || isset($sub_heading)) { ?>
    <div class="head-section">
        <div class="border-title"><h2><?php echo esc_attr( $heading ); ?></h2></div>
        <span class="title"><?php echo  force_balance_tags( $sub_heading ); ?></span>
    </div>
<?php } ?>
<div class="row">
    <?php
    if (isset($team_persons)) {
        foreach ($team_persons as $team) {
            ?>
            <div class="col-sm-3 member">
                <div class="member-img">
                    <a <?php echo (isset($team['link']) && !empty($team['link'])) ? 'href="' . esc_url($team['link']) . '"' : 'href="#"'; ?>
                        <?php if(isset($team['link_target']) ) {  
                            echo 'target="' . $team['link_target'] . '"'; 
                        } ?>
                        >
                        <?php if (isset($team['person_image']['url']) && is_array($team['person_image'])) { ?>
                            <img src="<?php echo esc_url($team['person_image']['url']); ?>" alt="<?php echo get_bloginfo('name'); ?>">
                        <?php } ?>
                    </a>
                </div>
                <div class="member-foot">
                    <?php if(isset($team['person_name']) && !empty($team['person_name'])){ ?>
                    <span class="member-name"><?php echo esc_attr($team['person_name']); ?></span>
                    <?php } ?>
                    <a 
                        <?php echo (isset($team['link']) && !empty($team['link'])) ? 'href="' . esc_url($team['link']) . '"' : 'href="#"'; ?>
                         <?php if(isset($team['link_target']) ) {  
                            echo 'target="' . $team['link_target'] . '"'; 
                        } ?>
                        class="btn-theme btn-member red">
                         <?php if(isset($team['person_job']) && !empty($team['person_job'])){ ?>
                        <span class="txt"><?php echo esc_attr($team['person_job']); ?></span>
                         <?php } ?>
                        <span class="round">
                            <i class="icon-arrow-right-latest-races"></i>
                        </span>
                    </a>
                </div>
            </div>
            <?php
        } //endforeach
    }//endif
    ?>
</div>
</div>