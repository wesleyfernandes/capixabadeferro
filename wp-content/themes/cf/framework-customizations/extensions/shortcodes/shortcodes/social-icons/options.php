<?php

if (!defined('FW')) {
    die('Forbidden');
}
$options = array(
    'title' => array(
        'label' => esc_html__('Title', 'triathlon'),
        'desc' => esc_html__('Enter Your Title Here.', 'triathlon'),
        'type' => 'text'
    ),
    'description' => array(
        'type'  => 'wp-editor',
        'value' => '',
        'attr'  => array(),
        'label' => esc_html__('Description', 'triathlon'),
        'desc'  => esc_html__('', 'triathlon'),
        /**
         * Load TinyMCE, can be used to pass settings directly to TinyMCE using an array
         * bool|array
         */
        'tinymce' => true,
        /**
         * Whether to display media insert/upload buttons
         * boolean
         */
        'media_buttons' => false,
        /**
         * Whether to output the minimal editor configuration used in PressThis
         * boolean
         */
        'teeny' => true,
        /**
         * Whether to use wpautop for adding in paragraphs
         * boolean
         */
        'wpautop' => true,
        /**
         * Additional CSS styling applied for both visual and HTML editors buttons, needs to include <style> tags, can use "scoped"
         * string
         */
        'editor_css' => '',
        /**
         * If smething goes wrong try set to true
         * boolean
         */
        'reinit' => true,
        /**
         * Set the editor size: small - small box, large - full size
         * boolean
         */
        'size' => 'small', // small | large
        /**
         * Set editor type : 'tinymce' or 'html'
         */
        'editor_type' => 'tinymce',
        /**
         * Set the editor height, must be int
         */
        'editor_height' => 250
    ),
    'add_social_icons' => array(
        'type' => 'addable-popup',
        'label' => esc_html__('Add Social Links', 'triathlon'),
        'desc' => esc_html__('', 'triathlon'),
        'template' => '{{- social_title }}',
        'popup-title' => esc_html__('Add Social Links', 'triathlon'),
        'size' => 'small', // small, medium, large
        'limit' => 0, // limit the number of popup`s that can be added
        'add-button-text' => esc_html__('Add', 'triathlon'),
        'sortable' => true,
        'popup-options' => array(
            'social_title' => array(
                'label' => esc_html__('Social Title', 'triathlon'),
                'type' => 'text',
                'value' => '',
                'desc' => esc_html__('', 'triathlon'),
            ),
            'social_description' => array(
                'label' => esc_html__('Description', 'triathlon'),
                'type' => 'text',
                'value' => '',
                'desc' => esc_html__('', 'triathlon'),
            ),
            'social_icon' => array(
                'type'  => 'icon',
                'value' => 'fa-smile-o',
                'attr'  => array(),
                'label' => esc_html__('Icon', 'triathlon'),
                'desc'  => esc_html__('Choose your social icon.', 'triathlon'),
            ),
            'social_link' => array(
                'label' => esc_html__('Social Link', 'triathlon'),
                'type' => 'text',
                'value' => '',
                'desc' => esc_html__('', 'triathlon'),
            ),
            'social_link_target' => array(
                'type' => 'switch',
                'value' => 'hello',
                'attr' => array(),
                'label' => esc_html__('New Tab', 'triathlon'),
                'desc' => esc_html__('Open link in new tab or parent tab.', 'triathlon'),
                'left-choice' => array(
                    'value' => '_blank',
                    'label' => esc_html__('Blank', 'triathlon'),
                ),
                'right-choice' => array(
                    'value' => '_self',
                    'label' => esc_html__('Self', 'triathlon'),
                ),
            ),
        ),
    )
);
