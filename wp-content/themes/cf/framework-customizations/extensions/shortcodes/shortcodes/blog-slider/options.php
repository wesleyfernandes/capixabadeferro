<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'heading' => array(
		'type'  => 'text',
		'value' => '',
		'label' => esc_html__('Heading', 'triathlon'),
		'desc'  => esc_html__('', 'triathlon'),
	),
	'sub_heading' => array(
		'type'  => 'text',
		'value' => '',
		'label' => esc_html__('Sub Heading', 'triathlon'),
		'desc'  => esc_html__('', 'triathlon'),
	),
	'show_posts' => array(
		'type'  => 'slider',
		'value' => 9,
		'properties' => array(

			'min' => 0,
			'max' => 20,
			'sep' => 1,

		),
		'label' => esc_html__('Show No of Posts', 'triathlon'),
	),
);