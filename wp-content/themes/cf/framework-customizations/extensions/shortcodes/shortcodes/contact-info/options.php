<?php

if (!defined('FW')) {
    die('Forbidden');
}

$options = array(
    'heading' => array(
        'type' => 'text',
        'value' => 'default value',
        'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
        'label' => esc_html__('Heading', 'triathlon'),
        'desc' => esc_html__('Enter Your Contact Info Heading Here.', 'triathlon'),
    ),
    'description' => array(
        'type' => 'wp-editor',
        'value' => 'Your Contact Page Description',
        'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
        'label' => esc_html__('Enter Your Contact Page Detail', 'triathlon'),
        /**
         * Load TinyMCE, can be used to pass settings directly to TinyMCE using an array
         * bool|array
         */
        'tinymce' => true,
        /**
         * Whether to display media insert/upload buttons
         * boolean
         */
        'media_buttons' => true,
        /**
         * Whether to output the minimal editor configuration used in PressThis
         * boolean
         */
        'teeny' => true,
        /**
         * Whether to use wpautop for adding in paragraphs
         * boolean
         */
        'wpautop' => true,
        /**
         * Additional CSS styling applied for both visual and HTML editors buttons, needs to include <style> tags, can use "scoped"
         * string
         */
        'editor_css' => '',
        /**
         * If smething goes wrong try set to true
         * boolean
         */
        'reinit' => true,
        /**
         * Set the editor size: small - small box, large - full size
         * boolean
         */
        'size' => 'small', // small | large
        /**
         * Set editor type : 'tinymce' or 'html'
         */
        'editor_type' => 'tinymce',
        /**
         * Set the editor height, must be int
         */
        'editor_height' => 200
    ),
    'add_contacts' => array(
        'type' => 'addable-popup',
        'label' => esc_html__('Add Contacts', 'triathlon'),
        'desc' => esc_html__('Add Your Contact Detail', 'triathlon'),
        'template' => '{{- address }}',
        'popup-title' => esc_html__('Add Contacts', 'triathlon'),
        'size' => 'small', // small, medium, large
        'limit' => 0, // limit the number of popup`s that can be added
        'add-button-text' => esc_html__('Add', 'triathlon'),
        'sortable' => true,
        'popup-options' => array(
            'contact_icon' => array(
                'type' => 'icon',
                'value' => 'fa-smile-o',
                'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
                'label' => esc_html__('Label', 'triathlon'),
                'desc' => esc_html__('Choose Your Icon', 'triathlon'),
            ),
            'address' => array(
                'type' => 'wp-editor',
                'value' => 'default value',
                'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
                'label' => esc_html__('Contact Detail', 'triathlon'),
                /**
                 * Load TinyMCE, can be used to pass settings directly to TinyMCE using an array
                 * bool|array
                 */
                'tinymce' => true,
                /**
                 * Whether to display media insert/upload buttons
                 * boolean
                 */
                'media_buttons' => false,
                /**
                 * Whether to output the minimal editor configuration used in PressThis
                 * boolean
                 */
                'teeny' => true,
                /**
                 * Whether to use wpautop for adding in paragraphs
                 * boolean
                 */
                'wpautop' => true,
                /**
                 * If smething goes wrong try set to true
                 * boolean
                 */
                'reinit' => true,
                /**
                 * Set the editor size: small - small box, large - full size
                 * boolean
                 */
                'size' => 'small', // small | large
                /**
                 * Set editor type : 'tinymce' or 'html'
                 */
                'editor_type' => 'html',
                /**
                 * Set the editor height, must be int
                 */
                'editor_height' => 200
            ),
        ),
    ),
);
