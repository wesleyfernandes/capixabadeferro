<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */

if ( empty( $atts['image'] ) ) {
	return;
}

$width  = ( is_numeric( $atts['width'] ) && ( $atts['width'] > 0 ) ) ? $atts['width'] : '';
$height = ( is_numeric( $atts['height'] ) && ( $atts['height'] > 0 ) ) ? $atts['height'] : '';
if ( ! empty( $width ) && ! empty( $height ) ) {
	$image = fw_resize( $atts['image']['attachment_id'], $width, $height, true );
} else {
	$image = $atts['image']['url'];
}
?>

<?php if ( empty( $atts['link'] ) ) : ?>
	<img src="<?php echo esc_url( $image ) ?>" alt="" width="<?php echo intval( $width ); ?>" height="<?php echo intval( $height ); ?>"/>
<?php else : ?>
	<a href="<?php echo esc_url( $atts['link'] ); ?>" target="<?php echo esc_attr( $atts['target'] ) ?>">
		<img src="<?php echo esc_url( $image ) ?>" alt="" width="<?php echo intval( $width ) ?>" height="<?php echo intval( $height ); ?>"/>
	</a>
<?php endif ?>
