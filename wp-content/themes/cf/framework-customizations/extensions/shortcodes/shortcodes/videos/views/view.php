<?php
if (!defined('FW'))
    die('Forbidden');
/**
 * @var $atts
 */
$width = '370';
$height = '155';
?>
<div class="blog-post row">
    <?php
    global $paged;
    if (empty($paged))
        $paged = 1;

    // Count Total Pssts
    $show_posts = $atts['show_posts'] ? $atts['show_posts'] : '-1';
    $args = array('posts_per_page' => "-1", 'post_type' => 'videos', 'order' => 'DESC', 'orderby' => 'ID', 'post_status' => 'publish', 'ignore_sticky_posts' => 1);
    $query = new WP_Query($args);
    $count_post = $query->post_count;

    //Main Query	
    $args = array('posts_per_page' => $atts['show_posts'], 'post_type' => 'videos', 'paged' => $paged, 'order' => 'DESC', 'orderby' => 'ID', 'post_status' => 'publish', 'ignore_sticky_posts' => 1);

    $query = new WP_Query($args);
    while ($query->have_posts()) : $query->the_post();
        global $post;
        $thumnail = triathlon_prepare_thumbnail($post->ID, $width, $height);

        if (isset($thumnail) && $thumnail) {
            $thumnail = $thumnail;
        } else {
            $thumnail = get_template_directory_uri() . '/img/no-image.png';
        }

        if (!function_exists('fw_get_db_post_option')) {
            return;
        } else {
            $video_link = fw_get_db_post_option($post->ID, 'video_link', true);
        }
        
        ?>
        <article class="post col-sm-4">
            <?php
            $url = parse_url($video_link);
            if ($url['host'] == $_SERVER["SERVER_NAME"]) {
                echo '<div class="post-img">';
                echo do_shortcode('[video width="' . $width . '" height="' . $height . '" src="' . $video_link . '"][/video]');
                echo '</div>';
            } else {

                if ($url['host'] == 'vimeo.com' || $url['host'] == 'player.vimeo.com') {
                    echo '<div class="post-img">';
                    $content_exp = explode("/", $video_link);
                    $content_vimo = array_pop($content_exp);
                    echo '<iframe width="' . $width . '" height="' . $height . '" src="https://player.vimeo.com/video/' . $content_vimo . '" 
						></iframe>';
                    echo '</div>';
                } elseif ($url['host'] == 'soundcloud.com') {
                    $video = wp_oembed_get($video_link, array('height' => $height));
                    $search = array('webkitallowfullscreen', 'mozallowfullscreen', 'frameborder="0"');
                    echo '<div class="post-img">';
                    echo str_replace($search, '', $video);
                    echo '</div>';
                } else {
                    echo '<div class="post-img">';
                    $content = str_replace(array('watch?v=', 'http://www.dailymotion.com/'), array('embed/', '//www.dailymotion.com/embed/'), $video_link);
                    echo '<iframe width="' . $width . '" height="' . $height . '" src="' . $content . '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
                    echo '</div>';
                }
            }
            if (isset($atts['show_author']) && $atts['show_author'] == 'show') :
                ?>
                <ul class="post-meta">
                    <li><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('nickname'); ?></a><?php echo esc_html_e('Admin', 'triathlon'); ?></li>
                </ul>
            <?php endif; ?> 

            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <?php if (isset($atts['show_description']) && $atts['show_description'] == 'show') { ?>
                <div class="description"><p><?php triathlon_prepare_excerpt($atts['excerpt_length'], 'false', ''); ?></p></div>
                    <?php } ?>

        </article>
        <?php
    endwhile;
    wp_reset_postdata();
    ?>
</div>
<?php
if (isset($atts['show_pagination']) && $atts['show_pagination'] == 'yes') :
    triathlon_prepare_pagination($count_post, $show_posts);
endif;
?>