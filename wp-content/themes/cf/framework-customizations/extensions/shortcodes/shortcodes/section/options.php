<?php if (!defined('FW')) {
    die('Forbidden');
}

$options = array(
    'section_heading' => array(
        'label' => esc_html__('Section Heading', 'triathlon'),
        'desc'  => esc_html__('Enter Your Section Heading Here', 'triathlon'),
        'type'  => 'text',
    ),
	'custom_styling' => array(
        'label'        => esc_html__('Custom Styling', 'triathlon'),
		'desc'    => esc_html__('Show/hide custom margin and padding', 'triathlon'),
        'type'         => 'switch',
		'value' => 'hide',
		'left-choice' => array(
			'value' => 'show',
			'label' => esc_html__('Show', 'triathlon'),
		),
		'right-choice' => array(
			'value' => 'hide',
			'label' => esc_html__('Hide', 'triathlon'),
		),
    ),
    'margin_top' => array(
        'type'  => 'slider',
        'value' => 0,
        'properties' => array(
            'min' => -200,
            'max' => 200,
            'sep' => 1,

        ),
        'label' => esc_html__('Margin Top', 'triathlon'),
    ),
    'margin_bottom' => array(
        'type'  => 'slider',
        'value' => 0,
        'properties' => array(
            'min' => -200,
            'max' => 200,
            'sep' => 1,

        ),
        'label' => esc_html__('Margin Bottom', 'triathlon'),
    ),
    'padding_top' => array(
        'type'  => 'slider',
        'value' => 90,
        'properties' => array(
            'min' => -200,
            'max' => 200,
            'sep' => 1,

        ),
        'label' => esc_html__('Padding Top', 'triathlon'),
    ),
    'padding_bottom' => array(
        'type'  => 'slider',
        'value' => 90,
        'properties' => array(
            'min' => -200,
            'max' => 200,
            'sep' => 1,

        ),
        'label' => esc_html__('Padding Bottom', 'triathlon'),
    ),
    'is_fullwidth' => array(
        'label'        => esc_html__('Full Width', 'triathlon'),
        'type'         => 'switch',
    ),
    'background_color' => array(
        'label' => esc_html__('Background Color', 'triathlon'),
        'desc'  => esc_html__('Please select the background color', 'triathlon'),
        'type'  => 'color-picker',
    ),
    'background_image' => array(
        'label'   => esc_html__('Background Image', 'triathlon'),
        'desc'    => esc_html__('Please select the background image', 'triathlon'),
        'type'    => 'background-image',
        'choices' => array(//	in future may will set predefined images
        )
    ),
	'background_repeat' => array(
        'type'  => 'select',
		'value' => 'no-repeat',
		'attr'  => array(),
		'label' => esc_html__('Label', 'triathlon'),
		'desc'  => esc_html__('Repeat Background', 'triathlon'),
		'help'  => esc_html__('', 'triathlon'),
		'choices' => array(
			'no-repeat' => esc_html__('No Repeat', 'triathlon'),
			'repeat' => esc_html__('Repeat', 'triathlon'),
			'repeat-x' => esc_html__('Repeat X', 'triathlon'),
			'repeat-y' => esc_html__('Repeat Y', 'triathlon'),
		),
    ),
	'positioning_x' => array(
        'type'  => 'slider',
        'value' => 0,
        'properties' => array(
            'min' => -100,
            'max' => 100,
            'sep' => 1,

        ),
        'desc' => esc_html__('Background position Horizontally', 'triathlon'),
		 'label' => esc_html__('Position X, IN ( % )', 'triathlon'),
    ),
	'positioning_y' => array(
        'type'  => 'slider',
        'value' => 100,
        'properties' => array(
            'min' => -100,
            'max' => 100,
            'sep' => 1,

        ),
        'desc' => esc_html__('Background position Vertically', 'triathlon'),
		 'label' => esc_html__('Position Y, IN ( % )', 'triathlon'),
    ),
    'video' => array(
        'label' => esc_html__('Background Video', 'triathlon'),
        'desc'  => esc_html__('Insert Video URL to embed this video', 'triathlon'),
        'type'  => 'text',
    ),
	'custom_id' => array(
        'label' => esc_html__('Custom ID', 'triathlon'),
        'desc'  => esc_html__('Add Custom ID', 'triathlon'),
        'type'  => 'text',
    ),
	'custom_classes' => array(
        'label' => esc_html__('Custom Classes', 'triathlon'),
        'desc'  => esc_html__('Add Custom Classes', 'triathlon'),
        'type'  => 'text',
    )
);
