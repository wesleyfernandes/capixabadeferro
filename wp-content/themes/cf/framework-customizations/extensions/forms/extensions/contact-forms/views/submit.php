<?php
if (!defined('FW')) {
    die('Forbidden');
}
/**
 * @var int $form_id
 * @var string $submit_button_text
 */
?>


<button type="submit" class="btn-theme red btn-submit">
    <span class="txt"><?php echo esc_attr($submit_button_text) ?></span>
    <span class="round">
        <i class="icon-arrow-right-latest-races"></i>
    </span>
</button>