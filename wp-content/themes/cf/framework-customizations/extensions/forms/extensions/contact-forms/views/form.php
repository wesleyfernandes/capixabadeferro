<?php
if (!defined('FW')) {
    die('Forbidden');
}
/**
 * @var string $form_id
 * @var string $form_html
 */
?>
<div class="form-wrapper contact-form fullwdith po-contactus-form">
<?php echo force_balance_tags($form_html); ?>
</div>