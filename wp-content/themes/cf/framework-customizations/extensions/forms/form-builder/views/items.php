<?php if (!defined('FW')) die('Forbidden');
/**
 * @var string $items_html
 */
?>
<div class="wrap-forms">
	<?php echo force_balance_tags( $items_html ) ?>
</div>