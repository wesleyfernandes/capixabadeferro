<?php 
/**
 * The template for displaying Search Form
 */
?>

<form class="form-search" method="get" role="search" action="<?php echo esc_url(home_url()); ?>">
	<input type="text" class="form-control" placeholder="" value="" name="s" id="s">
	<button type="submit" class="fa fa-search"></button>
</form>