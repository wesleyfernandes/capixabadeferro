"use strict";
jQuery(document).ready(function ($) {
    /* -------------------------------------
     Pretty Photo Gallery			
     -------------------------------------- */
    $("a[data-rel]").each(function () {
        $(this).attr("rel", $(this).data("rel"));
    });
    $("a[data-rel^='prettyPhoto']").prettyPhoto({
        animation_speed: 'normal',
        theme: 'dark_square',
        slideshow: 3000,
        autoplay_slideshow: true,
    });
    /* -------------------------------------
     Back to Top					
     -------------------------------------- */
    $('.btn-backtotop').on('click', function () {
        $('html,body').animate({scrollTop: 0}, 'slow');
        return false;
    });

    /* -------------------------------------
     Product Slider 				
     -------------------------------------- */
    var sync1 = $("#sync1");
    var sync2 = $("#sync2");
    sync1.owlCarousel({
        singleItem: true,
        slideSpeed: 1000,
        navigation: false,
        pagination: false,
        afterAction: syncPosition,
        responsiveRefreshRate: 200,
    });
    sync2.owlCarousel({
        items: 4,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 2],
        itemsTablet: [768, 2],
        itemsTabletSmall: [640, 3],
        itemsMobile: [479, 2],
        pagination: false,
        responsiveRefreshRate: 100,
        afterInit: function (el) {
            el.find(".owl-item").eq(0).addClass("synced");
        }
    });
    function syncPosition(el) {
        var current = this.currentItem;
        $("#sync2")
                .find(".owl-item")
                .removeClass("synced")
                .eq(current)
                .addClass("synced")
        if ($("#sync2").data("owlCarousel") !== undefined) {
            center(current)
        }
    }
    $("#sync2").on("click", ".owl-item", function (e) {
        e.preventDefault();
        var number = $(this).data("owlItem");
        sync1.trigger("owl.goTo", number);
    });
    function center(number) {
        var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
        var num = number;
        var found = false;
        for (var i in sync2visible) {
            if (num === sync2visible[i]) {
                var found = true;
            }
        }
        if (found === false) {
            if (num > sync2visible[sync2visible.length - 1]) {
                sync2.trigger("owl.goTo", num - sync2visible.length + 2)
            } else {
                if (num - 1 === -1) {
                    num = 0;
                }
                sync2.trigger("owl.goTo", num);
            }
        } else if (num === sync2visible[sync2visible.length - 1]) {
            sync2.trigger("owl.goTo", sync2visible[1])
        } else if (num === sync2visible[0]) {
            sync2.trigger("owl.goTo", num - 1)
        }
    }
    
    /* -------------------------------------
     Custom Form				
     -------------------------------------- */
    $(function () {
        jcf.replaceAll();
    });


    /* -------------------------------------
     Shopping Cart Prodict Increase		
     -------------------------------------- */
    $('em.minus').on('click', function () {
        $('.quantity_variation').val(parseInt($('#quantity1').val()) + 1);
    });
    $('em.plus').on('click', function () {

        if ($('#quantity1').val() == '1') {
            $('.quantity_variation').val(1);
        } else {
            $('.quantity_variation').val(parseInt($('#quantity1').val()) - 1);
        }
    });

    /* -------------------------------------
     Add to cart Button			
     -------------------------------------- */

    jQuery(document).on('click', '.btn-addtocart', function () {
        var _this = this
        var quantity = jQuery(_this).parents('.content-box').find('.quantity_variation').val();
        var product_id = jQuery(_this).parents('.content-box').find('.quantity_variation').data('product_id');
        var fragments = '';
        var error = '';
        var message = '';
        var cart = '';
        jQuery(_this).next('.add-to-cart-message').html('').hide();
        jQuery.ajax({
            type: 'POST',
            url: scripts_vars.ajaxurl,
            dataType: "json",
            data: 'product_id=' + product_id + '&quantity=' + quantity + '&action=add_to_cart_variable_rc',
            success: function (response) {
                if (response.error == 'true' && response.product_url) {
                    //window.location = response.product_url;
                    jQuery(_this).next('.add-to-cart-message').html(response.message).show();
                    return;
                }

                fragments = response.fragments;
                if (fragments) {
                    jQuery.each(fragments, function (key, value) {
                        jQuery(key).replaceWith(value);
                    });
                }
                jQuery(_this).next('.add-to-cart-message').html(response.cart).show();
            }
        });
    });

    /* -------------------------------------
     Sorting Products			
     -------------------------------------- */

    jQuery('.woocommerce-ordering').on('click', '.orderby', function () {
        jQuery(this).parents('form').submit();
    });
	
	jQuery('#gallery-1').isotope({
	  itemSelector: '.gallery-item',
	  percentPosition: true,
	  masonry: {
		// use outer width of grid-sizer for columnWidth
		columnWidth: 50
	  }
	});

});