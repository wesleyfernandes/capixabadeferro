<?php
/**
 * Hook For Google Fonts
 */
 
/**
 * @Mailchimp List
 * @return 
 */
if (!function_exists('triathlon_mailchimp_list')) {
    function triathlon_mailchimp_list() {
		$mailchimp_list[]='';
		$mailchimp_list[0] = 'Select List';
		$api_key	= 'b1c640ffabcea48f48530987ffdae147-us11';
		if( isset( $api_key ) && !empty( $api_key )) {
			if (!function_exists('fw_get_db_settings_option')) {
				return;
			} else {
				//$mailchimp_option = fw_get_db_settings_option('mailchimp_key');
				$mailchimp_option = $api_key;
			}
			
			//$mailchimp_option = isset($mailchimp) ? $mailchimp : '';
			if($mailchimp_option <> ''){
				$mailchim_obj = new Triathlon_MailChimp();
				
				$lists = $mailchim_obj->triathlon_mailchimp_list($mailchimp_option);
				if(is_array($lists) && isset($lists['data'])){
					foreach($lists['data'] as $list){
						if(!empty($list['name'])) :
							$mailchimp_list[$list['id']] = $list['name'];
						endif;
					}
				}
			}
		}
		
		return $mailchimp_list;
	}
}


/**
 * @Section wraper before
 * @return 
 */
 
add_action('triathlon_prepare_section_wrapper_before','triathlon_prepare_section_wrapper_before');
function triathlon_prepare_section_wrapper_before(){
	echo '<div class="main-page-wrapper haslayout">';
}

/**
 * @Section wraper after
 * @return 
 */
 
add_action('triathlon_prepare_section_wrapper_after','triathlon_prepare_section_wrapper_after');
function triathlon_prepare_section_wrapper_after(){
		echo '</div>';
}

/**
 * Ajax Form PHP Mailer
 */
if (!function_exists('triathlon_mail_script')) {

    function triathlon_mail_script() {


        if (function_exists('fw_get_db_settings_option')) {

            $recipient = fw_get_db_settings_option('email');
            $subject_message = fw_get_db_settings_option('subject_message');
            $success_message = fw_get_db_settings_option('success_message');
            $failure_message = fw_get_db_settings_option('failure_message');
        }


        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Get the form fields and remove whitespace.
            $name = strip_tags(trim($_POST["name"]));
            $name = str_replace(array("\r", "\n"), array(" ", " "), $name);
            $phone = strip_tags(trim($_POST["phone"]));
            $email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
            $message = trim($_POST["message"]);

            // Check that data was sent to the mailer.
            if (empty($name) OR empty($phone) OR empty($message) OR ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
                // Set a 400 (bad request) response code and exit.
                http_response_code(400);
                esc_html_e('Oops! There was a problem with your submission. Please complete the form and try again.', 'triathlon');
                exit;
            }

            // Set the recipient email address.
            // FIXME: Update this to your desired email address.
            // Set the email subject.
            $subject = $subject_message . ' ' . $name;

            // Build the email content.
            $email_content = "Name: $name\n";
            $email_content .= "Email: $email\n\n";
            $email_content .= "Message:\n$message\n";

            // Build the email headers.
            $email_headers = "From: $name <$email>";

            // Send the email.
            if (mail($recipient, $subject, $email_content, $email_headers)) {
                // Set a 200 (okay) response code.
                http_response_code(200);
                echo esc_attr($success_message);
                die();
            } else {
                // Set a 500 (internal server error) response code.
                http_response_code(500);
                echo esc_attr($failure_message);
                die();
            }
        } else {
            // Not a POST request, set a 403 (forbidden) response code.
            http_response_code(403);
            echo esc_attr($failure_message);
            die();
        }
    }

    add_action('wp_ajax_triathlon_mail_script', 'triathlon_mail_script');
    add_action( 'wp_ajax_nopriv_triathlon_mail_script', 'triathlon_mail_script' );
}
 
 
/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function triathlon_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}
	
	$body_class	= '';
	if (function_exists('fw_get_db_post_option')) {
		$maintenance = fw_get_db_settings_option('maintenance');
                
	} else {
		$maintenance = '';
	}
	
	
	$body_class	 ='';
	$background_comingsoon	= '';
	
	$post_name  = triathlon_get_post_name();
	if (( isset($maintenance) && $maintenance == 'enable' && !is_user_logged_in() ) || $post_name === "coming-soon") {
		$classes[] = 'comingsoon-page';	
	} else{
		if( is_home() || is_front_page() ){
			$classes[] = 'home ';
		}
	}
	
	$body_class	= "sport-version";
	if(function_exists('fw_get_db_settings_option')) {
		$theme_type 		= fw_get_db_settings_option('theme_type');
		if( isset( $theme_type ) && !empty( $theme_type ) ){
			$classes[]	= $theme_type.'-version';
		}
	}	
	
	return $classes;
}
add_filter( 'body_class', 'triathlon_body_classes' );

/**
 * @Post Classes
 * @return 
 */
function dossier_post_classes( $classes, $class, $post_id ) {
	//Add Your custom classes
    return $classes;
}
add_filter( 'post_class', 'dossier_post_classes', 10, 3 );