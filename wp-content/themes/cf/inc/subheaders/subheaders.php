<?php

/**
 * @subheaders
 *
 */
/**
 * @get post thumbnail
 * @return thumbnail url
 */
if (!function_exists('triathlon_prepare_subheaders')) {

    function triathlon_prepare_subheaders($post_id = '') {
        global $post;

        if (class_exists('woocommerce')) {
            if (is_shop()) {
                $page_id = woocommerce_get_page_id('shop');
            } else {
                $page_id = get_the_ID();
            }
        } else {
            $page_id = get_the_ID();
        }

        if (function_exists('fw_get_db_settings_option')) {
            $enable_subheader = fw_get_db_post_option($page_id, 'enable_subheader', true);
            $sub_heading = fw_get_db_post_option($page_id, 'sub_heading', true);
            $sub_heading_bg = fw_get_db_post_option($page_id, 'sub_heading_bg', true);
            $subheader_bg_image = fw_get_db_post_option($page_id, 'subheader_bg_image', true);
            $sub_heading_text = fw_get_db_post_option($page_id, 'sub_heading_text', true);
        } else {
            $enable_subheader = 'enable';
            $sub_heading = '';
            $sub_heading_bg = '#fdb62d';
            $subheader_bg_image = '';
            $sub_heading_text = '#FFF';
        }

        if (!is_home() && !is_front_page()) {
            if (is_404() || is_archive() || is_search() || is_category() || is_tag()) {
                if (is_404()) {
                    $title = esc_html__('404', 'triathlon');
                } else if (is_archive() && !class_exists('woocommerce')) {
                    if (is_category() || is_tag()) {
                        $obj = get_queried_object();
                        $title = $obj->name;
                    } else {
                        $title = esc_html__('Archive', 'triathlon');
                    }
                } else if (is_search()) {
                    $title = esc_html__('Search', 'triathlon');
                } elseif (class_exists('woocommerce')) {
                    if (is_shop()) {
                        $page_id = woocommerce_get_page_id('shop');
                        $title = get_the_title($page_id);
                    } elseif (is_product_category()) {
                        $obj = get_queried_object();
                        $title = $obj->name;
                    } elseif (is_category() || is_tag()) {
                        $obj = get_queried_object();
                        $title = $obj->name;
                    } else {
                        $title = esc_html__('Archive', 'triathlon');
                    }
                } else {
                    $title = esc_html__('Archive', 'triathlon');
                }
                $subheader = '';
                $subheader .='<div class="page-heading haslayout">';
                $subheader .='<div class="container">';
                $subheader .='<div class="row">';
                $subheader .='<h1>' . $title . '</h1>';
                $subheader .='</div>';
                $subheader .='</div>';
                $subheader .='</div>';

                echo force_balance_tags($subheader);
            } else {
                if (isset($enable_subheader) && $enable_subheader === 'enable') {
                    $sub_heading_bg = 'style="background-color:' . $sub_heading_bg . '"';
                    if (isset($subheader_bg_image) && !empty($subheader_bg_image)) {
                        $sub_heading_bg = 'style="background-image:url(' . $subheader_bg_image['url'] . ');"';
                    }

                    $subheader = '';
                    $subheader .='<div class="page-heading haslayout" ' . $sub_heading_bg . '>';
                    $subheader .='<div class="container">';
                    $subheader .='<div class="row">';
                    $subheader .='<h1 style="color:' . $sub_heading_text . '">' . get_the_title($page_id) . '</h1>';
                    if (isset($sub_heading) && $sub_heading != '') {
                        $subheader .='<h5 style="color:' . $sub_heading_text . '">' . $sub_heading . '</h5>';
                    }
                    $subheader .='</div>';
                    $subheader .='</div>';
                    $subheader .='</div>';

                    echo force_balance_tags($subheader);
                }
            }
        }
    }
}