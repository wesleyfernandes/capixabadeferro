<?php
/**
 * @Set Post Views
 * @return {}
 */
if (!function_exists('triathlon_add_dynamic_styles')) {

    function triathlon_add_dynamic_styles() {
        wp_enqueue_style(
                'triathlon_color_style', get_template_directory_uri() . '/css/typo.css'
        );

        if (function_exists('fw_get_db_settings_option')) {
            $enable_typo = fw_get_db_settings_option('enable_typo');
            $color_base = fw_get_db_settings_option('color_settings');
            $background = fw_get_db_settings_option('background');
            $custom_css = fw_get_db_settings_option('custom_css');
            $body_font = fw_get_db_settings_option('body_font');
            $h1_font = fw_get_db_settings_option('h1_font');
            $h2_font = fw_get_db_settings_option('h2_font');
            $h3_font = fw_get_db_settings_option('h3_font');
            $h4_font = fw_get_db_settings_option('h4_font');
            $h5_font = fw_get_db_settings_option('h5_font');
            $h6_font = fw_get_db_settings_option('h6_font');
            $logo_margin_top = fw_get_db_settings_option('logo_margin_top');
            $logo_margin_bottom = fw_get_db_settings_option('logo_margin_bottom');

            if (isset($logo_margin_top) && !empty($logo_margin_top)) {
                $logo_margin_top = $logo_margin_top;
            } else {
                $logo_margin_top = '50';
            }
            if (isset($logo_margin_bottom) && !empty($logo_margin_bottom)) {
                $logo_margin_bottom = $logo_margin_bottom;
            } else {
                $logo_margin_bottom = '50';
            }
        } else {
            $logo_margin_top = '50';
            $logo_margin_bottom = '50';
        }

        ob_start();
        echo (isset($custom_css)) ? $custom_css : '';
        if (isset($enable_typo) && $enable_typo == 'on') {
            ?>
            body,p,ul,li {
            font-size:<?php echo (isset($body_font['size'])) ? $body_font['size'] : '100%'; ?>px;
            font-family:<?php echo (isset($body_font['family'])) ? $body_font['family'] : 'Lato'; ?>;
            font-style:<?php echo (isset($body_font['style'])) ? $body_font['style'] : ''; ?>;
            color:<?php echo (isset($body_font['color'])) ? $body_font['color'] : '#000'; ?>;
            }
            h1{
            font-size:<?php echo (isset($h1_font['size'])) ? $h1_font['size'] : ''; ?>px;
            font-family:<?php echo (isset($h1_font['family'])) ? $h1_font['family'] : ''; ?>;
            font-style:<?php echo (isset($h1_font['style'])) ? $h1_font['style'] : ''; ?>;
            color:<?php echo (isset($h1_font['color'])) ? $h1_font['color'] : ''; ?>;
            }
            h2{
            font-size:<?php echo (isset($h2_font['size'])) ? $h2_font['size'] : ''; ?>px;
            font-family:<?php echo (isset($h2_font['family'])) ? $h2_font['family'] : ''; ?>;
            font-style:<?php echo (isset($h2_font['style'])) ? $h2_font['style'] : ''; ?>;
            color:<?php echo (isset($h2_font['color'])) ? $h2_font['color'] : ''; ?>;
            }
            h3{font-size:<?php echo (isset($h3_font['size'])) ? $h3_font['size'] : ''; ?>px;
            font-family:<?php echo (isset($h3_font['family'])) ? $h3_font['family'] : ''; ?>;
            font-style:<?php echo (isset($h3_font['style'])) ? $h3_font['style'] : ''; ?>;
            color:<?php echo (isset($h3_font['color'])) ? $h3_font['color'] : ''; ?>;
            }
            h4{font-size:<?php echo (isset($h4_font['size'])) ? $h4_font['size'] : 'Lato'; ?>px;
            font-family:<?php echo (isset($h4_font['family'])) ? $h4_font['family'] : 'Lato'; ?>;
            font-style:<?php echo (isset($h4_font['style'])) ? $h4_font['style'] : 'Lato'; ?>;
            color:<?php echo (isset($h4_font['color'])) ? $h4_font['color'] : 'Lato'; ?>;
            }
            h5{font-size:<?php echo (isset($h5_font['size'])) ? $h5_font['size'] : 'Lato'; ?>px;
            font-family:<?php echo (isset($h5_font['family'])) ? $h5_font['family'] : 'Lato'; ?>;
            font-style:<?php echo (isset($h5_font['style'])) ? $h5_font['style'] : 'Lato'; ?>;
            color:<?php echo (isset($h5_font['color'])) ? $h5_font['color'] : 'Lato'; ?>;
            }
            h6{font-size:<?php echo (isset($h6_font['size'])) ? $h6_font['size'] : 'Lato'; ?>px;
            font-family:<?php echo (isset($h6_font['family'])) ? $h6_font['family'] : 'Lato'; ?>;
            font-style:<?php echo (isset($h6_font['style'])) ? $h6_font['style'] : 'Lato'; ?>;
            color:<?php echo (isset($h6_font['color'])) ? $h6_font['color'] : 'Lato'; ?>;
            }	
        <?php } ?>

        <?php
        if (isset($color_base['gadget']) && $color_base['gadget'] === 'custom') {
            if (!empty($color_base['custom']['primary_color'])) {
                $theme_color = $color_base['custom']['primary_color'];
				$theme_secondary_color = $color_base['custom']['secondary_color'];
                ?>
                .btn-theme.btn-date,
				.news-event-slider .item .foot a,
				#header,
				.testimonials .head-section .border-title:after,
				.products-btns a.btn-buy-now,
				.content form fieldset .cart-foot .cart-total .cart-btns button.btn-checkout,
				.content form fieldset .btn-placeorder,
				.social-area li .icon:hover,
				.team .member-foot .btn-member,
                .btn-theme.red,
                .border-title:after,
                #seconds,
                .woocommerce .widget_price_filter .ui-slider .ui-slider-range,
                .tag-sale-product, 
                .tag-new-product,
                .woocommerce #respond input#submit.alt, 
                .woocommerce a.button.alt, 
                .woocommerce button.button.alt, 
                .woocommerce input.button.alt,
                a.add_to_wishlist, 
                .woocommerce #respond input#submit, 
                .woocommerce a.button, 
                .woocommerce button.button, 
                .woocommerce input.button,
                .woocommerce .widget_price_filter .ui-slider .ui-slider-handle,
                .po-contactsocial .col-md-20 article figure,
                .competitor-section .border-title:after,
                #comment-form fieldset form .form-submit input[type="submit"], 
                input[type="submit"]
                {background:<?php echo esc_attr($theme_color); ?>;}

                /*Theme Text Color*/
                .three-columns .border-title strong,
				.three-columns .athlete-naem,
				.latest-race .race-area .date,
				.gallery-section h2,
				#nav .navbar-collapse ul li a,
				.table-responsive .table thead,
				.table-responsive .table tbody tr td:first-child,
				.modal-dialog .product a.btn-moredetail,
				#comment ul li .comment-meta .reply-date span .fa,
				#comment ul li .comment-meta .author-details strong,
				.content h2,
				#comment h3,
				#comment-form h3,
				.event .accordion-head > div h3,
				.my-account strong.title a,
				.widget ul li label:hover a,
				span.product-price,
				.product-size ul li.l a,
				.product-size ul li.xl a,
				.benefits ul li i.icon
				.size-guide a:focus,
				.testimonial-slider strong.client-name,
				.testimonials .head-section .border-title,
				.testimonials .head-section .border-title h3,
				.contact-info a:hover,
				.about-author .author-meta .author-details strong,
				.more-articles > h2,
				span.product-price,
				.content .login-form.creataccount-form h2,
				.size-guide a:hover,
               
                .three-columns .border-title strong,
				.three-columns .athlete-naem,
				.latest-race .race-area .date,
				.gallery-section h2,
				#nav .navbar-collapse ul li a,
				.table-responsive .table thead,
				.table-responsive .table tbody tr td:first-child,
				.modal-dialog .product a.btn-moredetail,
				#comment ul li .comment-meta .reply-date span .fa,
				#comment ul li .comment-meta .author-details strong,
				.content h2,
				#comment h3,
				#comment-form h3,
				.event .accordion-head > div h3,
				.my-account strong.title a,
				.widget ul li label:hover a,
				span.product-price,
				.product-size ul li.l a,
				.product-size ul li.xl a,
				.benefits ul li i.icon
				.size-guide a:focus,
				.testimonial-slider strong.client-name,
				.testimonials .head-section .border-title,
				.testimonials .head-section .border-title h3,
				.contact-info a:hover,
				.about-author .author-meta .author-details strong,
				.more-articles > h2,
				span.product-price,
				.content .login-form.creataccount-form h2,
				.size-guide a:hover,
                .dropdown-menu.cart-item strong.title,
                .dropdown-menu.cart-item .cart-list li .detail .quantity,
                .head-section .title,
                body.comingsoon-page .comming-soon h2,
                .comming-soon .tg-displaytablecell, 
                .comming-soon .tg-displaytablecell h2, 
                .comming-soon .tg-displaytablecell .tg-description p,
                .woocommerce div.product p.price ins, 
                .woocommerce div.product span.price ins,
                .woocommerce .quantity,
                .woocommerce-ordering ul li label.jcf-label-active,
                .float,
                .banner-content h2
                { color: <?php echo esc_attr($theme_color); ?>;}


                /*Theme Border Color*/
                
				.newsletter-form input[type="email"]:focus,
				.product-size ul li.l a,
				.product-size ul li.xl a,
				.testimonials .head-section .border-title,
                .border-title
                {border-color:<?php echo esc_attr($theme_color); ?>;}
                .benefits ul li i.icon{
					border:2px solid <?php echo esc_attr($theme_color); ?>;
					color:<?php echo esc_attr($theme_color); ?>;
				}
            
            	.tag-sale-product:after, 
            	.tag-new-product:after{border-left: 15px solid <?php echo esc_attr($theme_color); ?>;}
            	.tag-sale-product:before, .tag-new-product:before{border-top: 11px solid <?php echo esc_attr($theme_color); ?>;}
            	
            	
            	 /*****************************************************
            	 	Theme Secondry Color
            	 *
            	 *******************************************************/
            	 .competitor-section,
            	 .jcf-radio span,
            	 .jcf-checkbox span,
            	 .tag-sale-product,
            	 .page-heading
            	 {background:<?php echo esc_attr($theme_secondary_color); ?>;}
            	 
            	 /*Theme Text Color*/
            	 a:hover, 
            	 a:focus,
            	 .btn-delete-item,
            	 .btn-delete-item a,
            	 #nav .navbar-collapse ul li.current-menu-item a, 
            	 #nav .navbar-collapse ul li a:hover,
            	 .woocommerce div.product .woocommerce-tabs ul.tabs li.active a, 
            	 .woocommerce div.product .woocommerce-tabs ul.tabs li a:hover
            	 { color: <?php echo esc_attr($theme_secondary_color); ?>;}


                 /*Theme Border Color*/
                 .jcf-radio,
                 .jcf-checkbox,
                 .btn-delete-item,
                 .woocommerce div.product .woocommerce-tabs ul.tabs li.active a, 
            	 .woocommerce div.product .woocommerce-tabs ul.tabs li a:hover
                 {border-color:<?php echo esc_attr($theme_secondary_color); ?>;}
                 .tag-sale-product:after{ border-left-color:<?php echo esc_attr($theme_secondary_color); ?>;}

            <?php } ?>
        <?php
        }
		
        $custom_css = ob_get_clean();
        wp_add_inline_style('triathlon_color_style', $custom_css);
    }

    add_action('wp_enqueue_scripts', 'triathlon_add_dynamic_styles');
}