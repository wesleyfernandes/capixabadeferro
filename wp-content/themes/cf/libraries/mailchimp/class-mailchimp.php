<?php
if ( !class_exists('Triathlon_MailChimp') ) {
	class Triathlon_MailChimp {
		function __construct(){
			add_action('wp_ajax_nopriv_triathlon_mailchimp_form', array(&$this,'triathlon_mailchimp_form'));
			add_action('wp_ajax_triathlon_mailchimp_form', array(&$this,'triathlon_mailchimp_form'));
			add_action('wp_ajax_nopriv_subscribe_mailchimp', array(&$this,'triathlon_subscribe_mailchimp'));
			add_action('wp_ajax_subscribe_mailchimp', array(&$this,'triathlon_subscribe_mailchimp'));
		}
	
		public function triathlon_mailchimp_form(){
			$counter	= 0;
			$footer_text	= '';
			$mailchimp		= '';
			if(function_exists('fw_get_db_settings_option')) :
				$footer_text = fw_get_db_settings_option('mailchimp_title');
				$mailchimp 	 = fw_get_db_settings_option('mailchimp_list');
				
			endif;
			$counter++;
	
			?>
			<form action="#" class="newsletter-form" id="mailchimpform_<?php echo intval($counter); ?>">
			  <fieldset>
			  	
				<input type="email" id="email" class="form-control" placeholder="Email Address">
				<input id="list_id" type="hidden" name="list_id" value="<?php echo esc_attr( $mailchimp );?>" />
				<button class="btn-theme white btn-submit subscribe_me" type="submit"  data-counter="<?php echo intval($counter); ?>"> <span class="txt"><?php esc_html_e('Submit','triathlon');?></span> <span class="round"> <i class="icon-arrow-right-latest-races"></i> </span> </button>
				<div id="mailchimp_<?php echo intval($counter); ?>" class="tg-loader"></div>
				<div id="mailchim_message_<?php echo intval($counter); ?>" class="elm-display-none tg-message"><div class="mailchimp-message"></div></div> 
			  </fieldset>
			</form>
			<script>
				jQuery(document).ready(function(e) {
					jQuery(document).on('click','.subscribe_me',function(event){
							'use strict';
							event.preventDefault();
							$ = jQuery;
							var email	= jQuery(this).parents('form').find('#email').val();
							var list_id	= jQuery(this).parents('form').find('#list_id').val();
							var counter	= jQuery(this).data('counter');
						jQuery('#mailchimp_' + counter).html('<i class="fa fa-refresh fa-spin"></i>');
						jQuery.ajax({
								type: 'POST',
								url: '<?php echo admin_url('admin-ajax.php'); ?>',
								data:'email='+email+'&list_id='+list_id+'&action=subscribe_mailchimp',
								dataType:"json",
								success: function (response) {
									if( response.type == 'success' ){                                 
										jQuery('#mailchimpform_' + counter).get(0).reset();
										jQuery('#mailchim_message_' + counter).fadeIn(600);
										jQuery('#mailchim_message_' + counter+" .mailchimp-message").html('<span class="success">'+response.message+'</span>');
										jQuery('#mailchimp_' + counter).html('');
									} else{
                                                                           
										jQuery('#mailchim_message_' + counter).fadeIn(600);
										jQuery('#mailchim_message_' + counter+" .mailchimp-message").html('<span class="error">'+response.message+'</span>');
										jQuery('#mailchimp_' + counter).html('');
									}
								   
								}
							});
						});
				});
				
			</script>
		<?php        
		}
		
		/**
		 * @get Mail chimp list
		 *
		 */
		public function triathlon_mailchimp_list($apikey) {
			$MailChimp = new TRIATHLON_OATH_MailChimp($apikey);
			$mailchimp_list = $MailChimp->triathlon_call('lists/list');
			return $mailchimp_list;
		}
		
		/**
		 * @get Mail chimp list
		 *
		 */
		 public function triathlon_subscribe_mailchimp() {
			global $counter;
			$mailchimp_key = '';
			$json	= array();
			if(function_exists('fw_get_db_settings_option')) :
				$mailchimp_key = fw_get_db_settings_option('mailchimp_key');				
			endif;

			if (isset($_POST) and ! empty($_POST['list_id']) and $mailchimp_key != '') {
				if ($mailchimp_key <> '') {
					$MailChimp = new TRIATHLON_OATH_MailChimp($mailchimp_key);
				}
				$email 	 = $_POST['email'];
				$list_id = $_POST['list_id'];
				$result = $MailChimp->triathlon_call('lists/subscribe', array(
					'id' => $list_id,
					'email' => array('email' => $email),
					'merge_vars' => array(),
					'double_optin' => false,
					'update_existing' => false,
					'replace_interests' => false,
					'send_welcome' => true,
				));
				if ($result <> '') {
					if (isset($result['status']) and $result['status'] == 'error') {
						$json['type'] 	 = 'error';
						$json['message'] = $result['error'];
					} else {
						$json['type']    = 'success';
						$json['message'] = esc_html__('Subscribe Successfully','triathlon');
					}
				}
			} else {
				$json['type'] 	 = 'error';
				$json['message'] = esc_html__('Some error occur,please try again later.','triathlon');
			}
			echo json_encode($json);
			die();
		}
	}
	
	new Triathlon_MailChimp();
}