<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Triathlon
 */

get_header(); 
$tg_sidebar	 	 = 'full';
$section_width	 = 'col-sm-12 col-xs-12';
if (function_exists('fw_ext_sidebars_get_current_position')) {
	$current_position = fw_ext_sidebars_get_current_position();
	if( $current_position !== 'full' &&  ( $current_position == 'left' || $current_position == 'right' ) ) {
		$tg_sidebar	= $current_position;
		$section_width	= 'col-md-9 col-sm-8  col-xs-12';
	}
}

if( isset( $tg_sidebar ) && ( $tg_sidebar == 'full' ) ){
		while ( have_posts() ) : the_post();?>
			<div class="container">
				<div class="row">
					<?php 
						do_action('triathlon_prepare_section_wrapper_before');
							the_content();
							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						do_action('triathlon_prepare_section_wrapper_after');
					?>
				</div>
			</div>
		<?php 
		endwhile;
} else{ 
?> 
<div class="container">
	<div class="row">
		<div class="row">
			<?php do_action('triathlon_prepare_section_wrapper_before');?>
			<?php if( isset( $tg_sidebar ) && $tg_sidebar == 'left' ) {?>
				<div class="col-sm-4 col-md-3 col-xs-12 sidebar-section" id="sidebar">
					<?php echo fw_ext_sidebars_show('blue'); ?>
				</div>
			<?php }?>
			<div class="<?php echo esc_attr( $section_width );?>  page-section">
				<?php
					while ( have_posts() ) : the_post();
							the_content();
							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;;
					endwhile;
				?>
			</div>
			<?php if( isset( $tg_sidebar ) && $tg_sidebar == 'right' ) {?>
				<div class="col-sm-4 col-md-3 col-xs-12 sidebar-section" id="sidebar">
					<?php echo fw_ext_sidebars_show('blue'); ?>
				</div>
			<?php }?>
			<?php do_action('triathlon_prepare_section_wrapper_after');?>
		</div>
	</div>
</div>
<?php }?>
<?php get_footer(); ?>