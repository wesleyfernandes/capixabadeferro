<?php if (!defined('FW')) die('Forbidden');
/**
 * @var string $uri Demo directory url
 */

$manifest = array();
$manifest['title'] = esc_html__('Triathlon Gym', 'triathlon');
$manifest['screenshot'] = get_template_directory_uri(). '/demo-content/images/gym.jpg';
$manifest['preview_link'] = 'http://triathlon.themographics.com/gym/';