<?php if (!defined('FW')) die('Forbidden');
/**
 * @var string $uri Demo directory url
 */

$manifest = array();
$manifest['title'] = esc_html__('Triathlon Sports', 'triathlon');
$manifest['screenshot'] = get_template_directory_uri(). '/demo-content/images/sports.jpg';
$manifest['preview_link'] = 'http://triathlon.themographics.com/sports/';