<?php
/**
 * The template part for displaying results in search pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Triathlon
 */
 $section_width	 = 'col-xs-12';
?>
<div class="col-md-12  tg-search-for">
	<div class="border-left">
		<h3><?php printf( esc_attr( 'Search Results for: %s', 'triathlon' ), '<span>' . get_search_query() . '</span>' ); ?></h3>
	</div><!-- .page-header -->
</div>
<div class="<?php echo esc_attr( $section_width );?> page-section">
	<div class="blog-grid blog-detail">
		<?php 
			global $paged;
			$tg_get_excerpt	= get_option('rss_use_excerpt');
			get_option('posts_per_page');
			
			if ( have_posts() ) : 
				if (empty($paged)) {
					$paged = 1;
				}
				 
				if (!isset($_GET["s"])) {
						$_GET["s"] = '';
				}
				
				while ( have_posts() ) : the_post(); 
					$width = '1170';
					$height = '450';
					$title_limit = 1000;
					$thumbnail 	 = triathlon_prepare_thumbnail( $post->ID, $width, $height );
					
					$stickyClass	= '';
					if( is_sticky() && !is_singular() ) {
						$stickyClass	= 'sticky';
					}
			  ?>                         
			 
				<article id="post-<?php the_ID(); ?>" class="tg-landing-page">
					<?php  
						if (isset($thumbnail) && $thumbnail != '') {
							?>
							<div class="tg-thumbnail">
								<figure><a href="<?php esc_url(the_permalink()); ?>"><img src="<?php echo esc_url($thumbnail); ?>" alt="<?php echo esc_attr(get_the_title());?>"></a></figure>
							 </div> <?php
						}
					
					?>
					<h2><a href="<?php esc_url(the_permalink()); ?>"><?php the_title();?></a></h2>
					<div class="tg-archive-meta">
					<?php 
						if( is_sticky() && !is_singular() ) :
							echo '<span class="sticky-post-wrap"><span class="sticky-txt"><i class="fa fa-bolt"></i>' . esc_html__("Featured", 'triathlon') .'</span></span>';
						endif;
					?>
					<?php echo get_the_term_list($post->ID, 'category', '<span class="tg-cats"><i class="fa fa-folder-open"></i>', ', ', '</span>'); ?>
					<?php the_tags( '<span class="tg-tags"><i class="fa fa-tags"></i>', ', ', '</span>' ); ?>
					</div>
					<div class="post-description">
						<p><?php echo triathlon_prepare_excerpt(255, 'false', 'Read More'); ?></p> 
						<a href="<?php esc_url(the_permalink()); ?>" class="btn-theme black archive-readmore">
							<span class="txt"><?php esc_html_e('Read more','triathlon');?></span>
							<span class="round">
								<i class="icon-arrow-right-latest-races"></i>
							</span>
						</a>
					</div>
			   </article>
			<?php 
			endwhile; 
			wp_reset_postdata();
		else:
			 esc_html_e('Sorry, but nothing matched your search terms. Please try again with some different keywords.','triathlon');
		endif; 
		
		$qrystr = '';
		if ($wp_query->found_posts > get_option('posts_per_page')) {
		   if ( function_exists( 'triathlon_prepare_pagination' ) ) { 
				echo triathlon_prepare_pagination(wp_count_posts()->publish,get_option('posts_per_page'));
		 } 
		}
		?>
	</div>
</div>