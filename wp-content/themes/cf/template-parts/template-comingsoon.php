<?php
global $post;
if (function_exists('fw_get_db_settings_option')) :
    $logo = fw_get_db_settings_option('logo');
    $background = fw_get_db_settings_option('background');
    $comming_title = fw_get_db_settings_option('comming_title');
    $comming_description = fw_get_db_settings_option('comming_description');

    $maintenance = fw_get_db_settings_option('maintenance');
    $date = fw_get_db_settings_option('date');
    $new_date = $date;
    $timestamp = strtotime($new_date);
    $timestamp = date("Y-m-d", strtotime("-1 month", strtotime($new_date)));
    $formatted_date = date('Y, m, d ,H, i, s', strtotime($timestamp));

//fw_print($formatted_date);
endif;

$post_name	= triathlon_get_post_name();
if (( isset($maintenance) && $maintenance == 'on' && !is_user_logged_in() ) || $post_name == "coming-soon") {
    ?>

    <!--************************************
                            Wrapper Start
            *************************************-->
    <div id="wrapper" class="haslayout">
        <!--************************************
                        Main Start
        *************************************-->
        <main id="main" class="haslayout">
            <!--************************************
                            Comming-Soon Start
            *************************************-->
            <div class="comming-soon">
                <div class="tg-displaytable">
                    <div class="tg-displaytablecell">
                        <?php if (isset($comming_title) && !empty($comming_title)) { ?>
                            <h2><?php echo esc_attr($comming_title); ?></h2>
                        <?php } ?>
                        <?php if (isset($comming_description) && !empty($comming_description)) { ?>
                            <div class="tg-description">
                                <p><?php echo do_shortcode($comming_description); ?></p>
                            </div>
                        <?php } ?>
                        <?php if (isset($logo['url']) && !empty($logo['url'])) { ?>
                            <img src="<?php echo esc_url($logo['url']); ?>">
                        <?php } ?>
                        <div class="coming-soon-counter">
                            <div id="days" class="timer_box tg-border-topleft"></div>
                            <div id="hours" class="timer_box tg-border-topleft"></div>
                            <div id="minutes" class="timer_box tg-border-topleft"></div>
                            <div id="seconds" class="timer_box seconds tg-border-topleft"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--************************************
                            Comming-Soon End
            *************************************-->
        </main>
        <!--************************************
                        Main End
        *************************************-->
    </div>
    <!--************************************
                    Wrapper End
    *************************************-->

    <script>
        	/* -------------------------------------
			COMMING SOON PAGE
	------------------------------------- */
	(function($) {
		var launch = new Date(<?php echo esc_attr($formatted_date); ?>);
		var days = $('#days');
		var hours = $('#hours');
		var minutes = $('#minutes');
		var seconds = $('#seconds');
		setDate();
		function setDate(){
			var now = new Date();
			if( launch < now ){
				days.html('<h1>0</H1><p>Day</p>');
				hours.html('<h1>0</h1><p>Hour</p>');
				minutes.html('<h1>0</h1><p>Minute</p>');
				seconds.html('<h1>0</h1><p>Second</p>');
			}
			else{
				var s = -now.getTimezoneOffset()*60 + (launch.getTime() - now.getTime())/1000;
				var d = Math.floor(s/86400);
				days.html('<h1>'+d+'</h1><p>Day'+(d>1?'s':''),'</p>');
				s -= d*86400;
				var h = Math.floor(s/3600);
				hours.html('<h1>'+h+'</h1><p>Hour'+(h>1?'s':''),'</p>');
				s -= h*3600;
				var m = Math.floor(s/60);
				minutes.html('<h1>'+m+'</h1><p>Minute'+(m>1?'s':''),'</p>');
				s = Math.floor(s-m*60);
				seconds.html('<h1>'+s+'</h1><p>Second'+(s>1?'s':''),'</p>');
				setTimeout(setDate, 1000);
			}
		}
	})(jQuery);
    </script>
    <?php
    die();
}
?>
