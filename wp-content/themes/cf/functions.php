<?php
/**
 * @theme Functionality Files
 * @return 
 */
require_once ( get_template_directory() . '/inc/helpers/theme-setup.php'); //Theme setup
require_once ( get_template_directory() . '/inc/helpers/general-helpers.php'); //Theme functionalty
require_once ( get_template_directory() . '/inc/headers/headers.php');
require_once ( get_template_directory() . '/inc/footers/footers.php');
require_once ( get_template_directory() . '/inc/subheaders/subheaders.php');
require_once ( get_template_directory() . '/inc/template-tags.php');
require_once ( get_template_directory() . '/inc/extras.php');
require_once ( get_template_directory() . '/inc/customizer.php');
require_once ( get_template_directory() . '/inc/jetpack.php');
require_once ( get_template_directory() . '/inc/google-fonts/google_fonts.php'); // goole fonts
require_once ( get_template_directory() . '/libraries/mailchimp/class-mailchimp.php');
require_once ( get_template_directory() . '/libraries/mailchimp/class-mailchimp-oath.php');
require_once ( get_template_directory() . '/inc/hooks.php');
require_once ( get_template_directory() . '/inc/widgets/init.php'); //widgets
require_once ( get_template_directory() . '/plugins/install-plugin.php');
require_once ( get_template_directory() . '/inc/theme-styling/dynamic-styles.php');
require_once ( get_template_directory() . '/inc/woocommerce/class-woocommerce.php');