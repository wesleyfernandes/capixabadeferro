			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<select class="surstudio_plugin_translator_revolution_lite_select" name="{{ id }}" id="{{ id }}">
{{ collection_formatted }}
						</select>
					</td>
					<td>&nbsp;&nbsp;&nbsp;</td>
					<td>
						<input class="button submit-button reset-button" type="button" value="Select" onclick="SurStudioPluginTranslatorRevolutionLiteAdmin.selectResource(); return false;" />
					</td>
					<td>&nbsp;&nbsp;&nbsp;</td>
					<td>
						<input class="button submit-button reset-button" type="button" value="Reset" onclick="SurStudioPluginTranslatorRevolutionLiteAdmin.resetResource(); return false;" />
					</td>
					<td>&nbsp;&nbsp;&nbsp;</td>
					<td id="{{ id }}_remove" class="surstudio_plugin_translator_revolution_lite_no_display_important">
						<input class="button submit-button reset-button" type="button" value="Remove" onclick="SurStudioPluginTranslatorRevolutionLiteAdmin.removeResource('All the cached translations for the selected Resource will be removed. This operation cannot be undone. Do you want to continue?'); return false;" />
					</td>
					<td>&nbsp;&nbsp;&nbsp;</td>
					<td id="{{ id }}_loading"></td>
				</tr>
			</table>