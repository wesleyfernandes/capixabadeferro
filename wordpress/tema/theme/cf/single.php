<?php
/**
 * The template for displaying all single posts.
 *
 * @package Triathlon
 */
get_header();
?>
<?php
if (!function_exists('fw_get_db_post_option') && !function_exists('fw_get_db_settings_option')) {
    //return;
} else {
    if (function_exists('fw_get_db_settings_option')) {
        $blog_settings = fw_get_db_post_option(get_the_ID(), 'blog_settings', true);
        $blog_post_image = fw_get_db_post_option(get_the_ID(), 'blog_post_image', true);
        $blog_post_gallery = fw_get_db_post_option(get_the_ID(), 'blog_post_gallery', true);
        $blog_post_video_custom = fw_get_db_post_option(get_the_ID(), 'blog_video_link', true);
        $related_title = fw_get_db_post_option(get_the_ID(), 'related_posts_title', true);
        $enable_related = fw_get_db_post_option(get_the_ID(), 'enable_related_posts', true);
    } else {
        $blog_settings = '';
        $blog_post_image = '';
        $blog_post_gallery = '';
        $blog_post_video_custom = '';
    }
}
$thumnail = triathlon_prepare_thumbnail(get_the_ID(), 1140, 289);
?>
<div class="blog-post haslayout">
    <div class="container">
        <div class="row">
            <?php while (have_posts()) : the_post(); ?>
                <article class="post single">
                    <?php if (isset($blog_settings) && $blog_settings == 'image' && !empty($thumnail)) { ?>
                        <div class="post-img">
                            <a href="javascript:;"><img src="<?php echo esc_url($thumnail); ?>" alt="<?php echo get_bloginfo('name'); ?>"></a>
                            <ul class="post-meta">
                                <li><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo ucfirst(get_the_author_meta('nickname')); ?></a> <?php esc_html_e('Admin', 'triathlon'); ?></li>
                            </ul>
                        </div>
                        <?php
                    } elseif (isset($blog_settings) && $blog_settings === 'gallery' && !empty($blog_post_gallery) && $blog_post_gallery != '') {
                        $uniq_flag = fw_unique_increment();
                        ?>
                        <div class="blog-slider-<?php echo esc_attr($uniq_flag); ?> row">
                            <?php
                            foreach ($blog_post_gallery as $blog_gallery) {
                                $attachment_id = $blog_gallery['attachment_id'];
                                $image_data = wp_get_attachment_image_src($attachment_id, array(1140, 289));
                                if (isset($image_data) && !empty($image_data) && $image_data[0] != '') {
                                    ?>
                                    <div class="item">
                                        <img src="<?php echo esc_url($image_data[0]); ?>" alt="<?php echo get_bloginfo('name'); ?>">
                                    </div>
                                <?php
                                }
                            }
                            ?>
                        </div>
                        <script>
                            jQuery(document).ready(function () {
                                jQuery(".blog-slider-<?php echo esc_js($uniq_flag); ?>").owlCarousel({
                                    autoPlay: true,
                                    navigation: false,
                                    pagination: false,
                                    slideSpeed: 300,
                                    paginationSpeed: 400,
                                    singleItem: true,
                                });
                            });
                        </script>

                    <?php
                    } elseif (isset($blog_settings) && $blog_settings === 'video') {
                        $height = 450;
                        $width = 1140;

                        $url = parse_url($blog_post_video_custom);
                        if (isset($url['host']) &&  $url['host'] == $_SERVER["SERVER_NAME"]) {
                            echo '<div class="post-img">';
                            echo do_shortcode('[video width="' . $width . '" height="' . $height . '" src="' . $blog_post_video_custom . '"][/video]');
                            echo '</div>';
                        } else {

                            if (isset($url['host']) && $url['host'] == 'vimeo.com' || isset($url['host']) && $url['host'] == 'player.vimeo.com') {
                                echo '<div class="post-img">';
                                $content_exp = explode("/", $blog_post_video_custom);
                                $content_vimo = array_pop($content_exp);
                                echo '<iframe width="' . $width . '" height="' . $height . '" src="https://player.vimeo.com/video/' . $content_vimo . '" 
						></iframe>';
                                echo '</div>';
                            } elseif (isset($url['host']) &&  $url['host'] == 'soundcloud.com') {
                                $video = wp_oembed_get($blog_post_video_custom, array('height' => $height));
                                $search = array('webkitallowfullscreen', 'mozallowfullscreen', 'frameborder="0"');
                                echo '<div class="post-img">';
                                echo str_replace($search, '', $video);
                                echo '</div>';
                            } else {
                                echo '<div class="post-img">';
                                $content = str_replace(array('watch?v=', 'http://www.dailymotion.com/'), array('embed/', '//www.dailymotion.com/embed/'), $blog_post_video_custom);
                                echo '<iframe width="' . $width . '" height="' . $height . '" src="' . $content . '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
                                echo '</div>';
                            }
                        }
                        ?>
    <?php } else if (!empty($thumnail)) { ?>
                        <div class="post-img">
                            <a href="javascript:;"><img src="<?php echo esc_url($thumnail); ?>" alt="<?php echo get_bloginfo('name'); ?>"></a>
                            <ul class="post-meta">
                                <li><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo ucfirst(get_the_author_meta('nickname')); ?></a> <?php esc_html_e('Admin', 'triathlon'); ?></li>
                            </ul>
                        </div>
        <?php }
    ?>

                    <h2 class="single-post-title"><?php the_title(); ?></h2>
                    <span class="date"><?php echo date_i18n(get_option('date_format'), strtotime(get_the_date())); ?></span>
                    <div class="description">
                <?php the_content(); ?>
                    </div>
                </article>
<?php endwhile; ?>
            <div class="post-comments haslayout">
                <div class="about-author">
                    <h4><?php esc_html_e('About The Author', 'triathlon'); ?></h4>
                    <div class="author-meta">
                        <div class="author-img">
                            <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>">
<?php echo get_avatar(get_the_author_meta('ID'), 53); ?>
                            </a>
                        </div>
                        <div class="author-details">
                            <strong class="author-name"><?php echo get_the_author_meta('nickname'); ?></strong>
                            <span class="author-post"><?php echo esc_attr(get_the_author_meta('designation')); ?></span>
                        </div>
                    </div>
<?php if (get_the_author_meta('description')) { ?>
                        <div class="author-description">
                            <p><?php echo esc_attr(get_the_author_meta('description')); ?> </p>
                        </div>
                <?php } ?>
                </div>
                <?php
                if (comments_open() || get_comments_number()) :
                    comments_template();
                endif;
                ?>
            </div>
                <?php if (isset($enable_related) && $enable_related == 'show') { ?>
                <!-- More Articles Start -->
                <div class="more-articles">
                    <?php if (isset($related_title) && !empty($related_title)) { ?>
                        <h2 class="single-post-title"><?php echo isset($related_title) && !empty($related_title) && $related_title != '1' ? $related_title : esc_html__('Related Posts', 'triathlon'); ?></h2>
                        <?php } ?>
                    <div class="posts row">
                        <?php
                        /**
                         * Custom Query
                         */
                        $args = array('post_type' => 'post', 'post_status' => 'publish', 'posts_per_page' => 3, 'ignore_sticky_posts' => true, 'order' => 'ASC');
                        $query = new WP_Query($args);
                        // The Loop
                        if ($query->have_posts()) :
                            while ($query->have_posts()) : $query->the_post();
                                $thumnail = triathlon_prepare_thumbnail($query->ID, 370, 175);

                                if (isset($thumnail) && !empty($thumnail)) {
                                    $thumnail = $thumnail;
                                } else {
                                    $thumnail = get_template_directory_uri() . '/img/no-image.png';
                                }
                                ?>
                                <article class="post col-sm-4">
                                    <div class="post-img"><a href="<?php the_permalink(); ?>"><img src="<?php echo esc_url($thumnail); ?>" alt="<?php echo get_bloginfo('name'); ?>"></a></div>
                                    <ul class="post-meta">
                                        <li><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo ucfirst(get_the_author_meta('nickname')); ?></a> <?php esc_html_e('Admin', 'triathlon'); ?></li>
                                    </ul>
                                    <h2  class="single-post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                    <div class="description">
                                		<?php triathlon_prepare_excerpt(150, false); ?>
                                    </div>
                                </article>
                                <?php
                            endwhile;
                        endif;
                        // Reset Post Data
                        wp_reset_postdata();
                        ?>
                    </div>
                </div>
                <!-- More Articles End -->
<?php } ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>