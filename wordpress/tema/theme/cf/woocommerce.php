<?php
/**
 * @Woocommerce Listing
 * return {}
 */

get_header();
global $post;
 wp_reset_query();
 

?>
<div class="container">
  <div class="row">
	<?php
	if(is_shop()){
		get_template_part( 'woocommerce/woocommerce-listing', 'page' );
	} else if(is_single()){
		get_template_part( 'woocommerce/single-product', 'page' );
	} else if(is_product_category() or is_product_tag()){
		get_template_part( 'woocommerce/archive-product', 'page' );
	} else{?>
		<div class="two-columns">
			<?php if( function_exists('woocommerce_content') ) { woocommerce_content(); } ?>
		</div>
	<?php 
	}?>
  </div>
</div>
<?php get_footer();?>