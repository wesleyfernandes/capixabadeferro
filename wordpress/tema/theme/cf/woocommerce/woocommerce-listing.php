<?php
/**
 * @Woocommerce Product Listing
 * @return {}
 */

if(function_exists('fw_get_db_settings_option')){
	$enable_sidebar = fw_get_db_settings_option('enable_sidebar');
	$sidebar_position = fw_get_db_settings_option('sidebar_position');
	$is_sidebar		= isset( $enable_sidebar ) && $enable_sidebar == 'on' ? 'on' : 'off';
	$content_div	= isset( $enable_sidebar ) && $enable_sidebar == 'on' ? 'col-sm-9 col-xs-12' : 'col-sm-12 col-xs-12';
} else{
	$content_div	= 'col-sm-12 col-xs-12';
	$is_sidebar		= 'off';
	$sidebar_position		= 'left';
}

if( isset( $sidebar_position ) && $sidebar_position == 'right' ){
	$content_div		= $content_div.' pull-left';
	$sidebar_position	= ' pull-right';
} else{
	$content_div		= $content_div.' pull-right';
	$sidebar_position	= ' pull-left';
}

?>

<div id="two-columns">
<fieldset>
	<div class="row">
		<div class="products <?php echo esc_attr( $content_div );?>">
		<?php 
			if(function_exists('fw_get_db_settings_option')){
				$enable_banner = fw_get_db_settings_option('enable_banner');
				$product_logo = fw_get_db_settings_option('product_logo');
				$product_title = fw_get_db_settings_option('product_title');
				$product_button_link = fw_get_db_settings_option('product_button_link');
				$product_button_text = fw_get_db_settings_option('product_button_text');

			if( isset( $enable_banner ) && $enable_banner === 'on' && !empty( $product_logo['url'] ) ) {?>
				<div class="product-banner">
					<img src="<?php echo esc_url( $product_logo['url'] );?>" alt="<?php esc_html_e('Banner','triathlon');?>">
					<div class="banner-content">
						<h2><?php echo esc_attr( $product_title );?></h2>
						<a href="<?php echo esc_url( $product_button_link );?>" class="btn-theme btn-shop black"> <span class="txt"><?php echo esc_attr( $product_button_text );?></span> <span class="round"> <i class="icon-arrow-right-latest-races"></i> </span> </a> 
					</div>
			   </div>
			<?php } }?>
			<div class="shop-head">
				<div class="shop-head-filter">
					<?php woocommerce_catalog_ordering(); ?>
				</div>
				<div class="product-perpage">
                    <div class="col-sm-6 tg_woo_total_results">
                         <?php woocommerce_get_template( 'loop/result-count.php' ); ?>
                    </div>
                    <div class="col-sm-6 tg_woo_items">
                        <label><?php esc_html_e('Items Per Page:', 'triathlon'); ?></label>
    					<?php triathlon_woocommerce::triathlon_woocommerce_catalog_page_ordering();?>
                    </div>	
				</div>
			</div>
			<div class="products-list haslayout">
				<?php 
					if ( have_posts() ) :
						  while ( have_posts() ) : the_post(); 
							get_template_part( 'woocommerce/content', 'product' ); 
						  endwhile; 
					endif;
				?>
			</div>
			<!-- Pagination Start -->
			<?php woocommerce_get_template( 'loop/pagination.php' );;?>
		</div>
		<?php if( isset( $is_sidebar ) && $is_sidebar ==  'on' ) {?>
		<aside id="sidebar" class="col-sm-3 col-xs-12 <?php echo esc_attr ( $sidebar_position );?> woocommerce">
			<?php 
				if( !is_product() )  {
					if(is_woocommerce()){
						if (!function_exists('dynamic_sidebar') || !dynamic_sidebar("shop_sidebar")) : endif;
					}
				}?>
		</aside>
		<?php }?>
	</div>
</fieldset>
</div>
