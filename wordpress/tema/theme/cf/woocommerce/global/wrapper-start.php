<?php
/**
 * Content wrappers
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$template = get_option( 'template' );
switch( $template ) {
	case 'triathlon' :
		echo '<div class="container"><div class="row">';
		break;
	default :
		echo '<div id="container"><div id="content" role="main">';
		break;
}
