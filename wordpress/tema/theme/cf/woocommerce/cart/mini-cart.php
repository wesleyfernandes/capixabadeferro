<?php
/**
 * Mini-cart
 *
 * Contains the markup for the mini-cart, used by the cart widget
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php do_action( 'woocommerce_before_mini_cart' ); ?>

<ul class="cart-list cart_list product_list_widget <?php echo triathlon_esc_specialchars( $args['list_class'] ); ?>">

	<?php if ( ! WC()->cart->is_empty() ) : ?>

		<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {

					$product_name  = apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key );
					$thumbnail     = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
					$product_price = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
					?>
					<li class="<?php echo esc_attr( apply_filters( 'woocommerce_mini_cart_item_class', 'mini_cart_item', $cart_item, $cart_item_key ) ); ?>">
						
						<div class="product-img">
						<?php if ( ! $_product->is_visible() ) : ?>
							<?php 	echo str_replace( array( 'http:', 'https:' ), '', $thumbnail ). '&nbsp;'; ?>
						<?php else : ?>
							<a href="<?php echo esc_url( $_product->get_permalink( $cart_item ) ); ?>">
								<?php echo str_replace( array( 'http:', 'https:' ), '', $thumbnail ). '&nbsp;'; ?>
							</a>
						<?php endif; ?>
						</div>
						<div class="detail">
							<span class="product-title"><?php echo esc_attr( $product_name );?></span>
							<div class="quantity">
							<?php echo WC()->cart->get_item_data( $cart_item ); ?>
							<?php echo apply_filters( 'woocommerce_widget_cart_item_quantity', '' . sprintf( '<span class="qty">%s</span> <span class="times">&times;</span> %s', $cart_item['quantity'], $product_price ) . '', $cart_item, $cart_item_key ); ?>
							</div>
						</div>
						
						<i class="btn-delete-item">
						<?php
						echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
							'<a href="%s" class="remove" title="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
							esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
							esc_html__( 'Remove this item', 'triathlon' ),
							esc_attr( $product_id ),
							esc_attr( $_product->get_sku() )
						), $cart_item_key );
						?>
						</i>
					</li>
					<?php
				}
			}
		?>

	<?php else : ?>

		<li class="empty"><?php esc_html_e( 'No products in the cart.', 'triathlon' ); ?></li>

	<?php endif; ?>

</ul><!-- end product list -->

<?php if ( ! WC()->cart->is_empty() ) : ?>

	<?php /*?><p class="total"><strong><?php esc_html_e( 'Subtotal', 'triathlon' ); ?>:</strong> <?php echo WC()->cart->get_cart_subtotal(); ?></p><?php */?>

	<?php do_action( 'woocommerce_widget_shopping_cart_before_buttons' ); ?>
	
	<div class="btn-area">
		<a href="<?php echo WC()->cart->get_cart_url(); ?>" class="btn-theme">
			<span class="txt"><?php esc_html_e( 'View All', 'triathlon' ); ?></span>
			<span class="round">
				<i class="icon-arrow-right-latest-races"></i>
			</span>
		</a>
		<a href="<?php echo WC()->cart->get_checkout_url(); ?>" class="btn-theme">
			<span class="txt"><?php esc_html_e( 'Checkout', 'triathlon' ); ?></span>
			<span class="round">
				<i class="icon-arrow-right-latest-races"></i>
			</span>
		</a>
	</div>

<?php endif; ?>

<?php do_action( 'woocommerce_after_mini_cart' ); ?>
