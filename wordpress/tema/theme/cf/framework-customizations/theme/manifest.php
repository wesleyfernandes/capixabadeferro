<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$manifest = array();

$manifest['id'] = 'triathlon';

$manifest['supported_extensions'] = array(
	'page-builder' 	=> array(),
	'backups' 		=> array(),
	'translation' 	=> array(),
	'sidebars' 		=> array(),
	//'styling' => array(),
	//'breadcrumbs' => array(),
	//'events' => array(),
	//'feedback' => array(),
	//'learning' => array(),
	//'megamenu' => array(),
	//'portfolio' => array(),
	//'sidebars' => array(),
);
