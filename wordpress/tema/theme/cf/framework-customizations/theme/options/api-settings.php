<?php

if (!defined('FW')) {
    die('Forbidden');
}

$options = array(
    'api_settings' => array(
        'type' => 'tab',
        'title' => esc_html__('Api Settings', 'triathlon'),
        'options' => array(
			'google' => array(
				'title' => esc_html__('Google', 'triathlon'),
				'type' => 'tab',
				'options' => array(
					'google_key' => array(
						'type' => 'text',
						'value' => '',
						'label' => esc_html__('Google Map Key', 'triathlon'),
						'desc' => wp_kses( __( 'Enter google map key here. It will be used for google maps. Get and Api key From <a href="https://developers.google.com/maps/documentation/javascript/get-api-key" target="_blank"> Get API KEY </a>', 'triathlon' ),array(
																		'a' => array(
																			'href' => array(),
																			'title' => array()
																		),
																		'br' => array(),
																		'em' => array(),
																		'strong' => array(),
																	)),
					),
				)
			),
            'mailchimp' => array(
                'title' => esc_html__('Mail Chimp', 'triathlon'),
                'type' => 'tab',
                'options' => array(
                    'mailchimp_key' => array(
                        'type' => 'text',
                        'value' => '',
                        'label' => esc_html__('MailChimp Key', 'triathlon'),
                        'desc' => esc_html__('Enter your MailChimp Key Here. Default: b1c640ffabcea48f48530987ffdae147-us11', 'triathlon'),
                    ),
                    'mailchimp_list' => array(
                        'type' => 'select',
                        'label' => esc_html__('MailChimp List', 'triathlon'),
                        'choices' => triathlon_mailchimp_list(),
                    /**
                     * Allow save not existing choices
                     * Useful when you use the select to populate it dynamically from js
                     */
                    //'no-validate' => false,
                    )
                )
            ),
        )
    )
);


