<?php

if (!defined('FW')) {
    die('Forbidden');
}

$options = array(
    'event_settings_advanced' => array(
        'title' => 'Event Settings',
        'type' => 'box',
        'options' => array(
            'sub_headers' => array(
                'title' => esc_html__('Sub-Header Settings', 'triathlon'),
                'type' => 'tab',
                'options' => array(
                    'enable_subheader' => array(
                        'type' => 'switch',
                        'value' => 'enable',
                        'label' => esc_html__('Subheader', 'triathlon'),
                        'desc' => esc_html__('Enable or Disable Subheader', 'triathlon'),
                        'left-choice' => array(
                            'value' => 'enable',
                            'label' => esc_html__('Enable', 'triathlon'),
                        ),
                        'right-choice' => array(
                            'value' => 'disable',
                            'label' => esc_html__('Disable', 'triathlon'),
                        ),
                    ),
                    'sub_heading' => array(
                        'type' => 'textarea',
                        'value' => '',
                        'label' => esc_html__('Sub Heading', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                    ),
                    'sub_heading_bg' => array(
                        'type' => 'color-picker',
                        'value' => '#FDB62D',
                        'label' => esc_html__('Sub Heading bg color', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                    ),
                    'sub_heading_text' => array(
                        'type' => 'color-picker',
                        'value' => '#FFF',
                        'label' => esc_html__('Sub Heading Text color', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                    ),
                )
            ),
            'event_settings' => array(
                'title' => esc_html__('Event Settings', 'triathlon'),
                'type' => 'tab',
                'options' => array(
                    'event_settings' => array(
                        'type' => 'box',
                        'options' => array(
                            'contact-box' => array(
                                'title' => esc_html__('Contact Information', 'triathlon'),
                                'type' => 'tab',
                                'options' => array(
                                    'contact_email' => array(
                                        'type' => 'text',
                                        'value' => '',
                                        'attr' => array(),
                                        'label' => esc_html__('Contact Email', 'triathlon'),
                                        'desc' => esc_html__('Contact person email ID', 'triathlon'),
                                        'help' => esc_html__('', 'triathlon'),
                                    ),
                                    'contact_phone' => array(
                                        'type' => 'text',
                                        'value' => '',
                                        'attr' => array(),
                                        'label' => esc_html__('Contact Phone', 'triathlon'),
                                        'desc' => esc_html__('Contact person Phone', 'triathlon'),
                                        'help' => esc_html__('', 'triathlon'),
                                    ),
                                )
                            ),
                            'event-box' => array(
                                'title' => esc_html__('Event Infromation', 'triathlon'),
                                'type' => 'tab',
                                'options' => array(
                                    'start_date' => array(
                                        'type' => 'datetime-picker',
                                        'value' => '',
                                        'attr' => array(),
                                        'label' => esc_html__('Start Date', 'triathlon'),
                                        'desc' => esc_html__('', 'triathlon'),
                                        'help' => esc_html__('', 'triathlon'),
                                        'datetime-picker' => array(
                                            'format' => 'Y-m-d H:i', // Format datetime.
                                            'maxDate' => false, // By default there is not maximum date , set a date in the datetime format.
                                            'minDate' => false, // By default minimum date will be current day, set a date in the datetime format.
                                            'timepicker' => true, // Show timepicker.
                                            'datepicker' => true, // Show datepicker.
                                            'defaultTime' => '12:00' // If the input value is empty, timepicker will set time use defaultTime.
                                        ),
                                    ),
                                    'end_date' => array(
                                        'type' => 'datetime-picker',
                                        'value' => '',
                                        'attr' => array(),
                                        'label' => esc_html__('End Date', 'triathlon'),
                                        'desc' => esc_html__('', 'triathlon'),
                                        'help' => esc_html__('', 'triathlon'),
                                        'datetime-picker' => array(
                                            'format' => 'Y-m-d H:i', // Format datetime.
                                            'maxDate' => false, // By default there is not maximum date , set a date in the datetime format.
                                            'minDate' => false, // By default minimum date will be current day, set a date in the datetime format.
                                            'timepicker' => true, // Show timepicker.
                                            'datepicker' => true, // Show datepicker.
                                            'defaultTime' => '12:00' // If the input value is empty, timepicker will set time use defaultTime.
                                        ),
                                    ),
                                    'booking_last_day' => array(
                                        'type' => 'datetime-picker',
                                        'value' => '',
                                        'attr' => array(),
                                        'label' => esc_html__('Last day to book by', 'triathlon'),
                                        'desc' => esc_html__('', 'triathlon'),
                                        'help' => esc_html__('', 'triathlon'),
                                        'datetime-picker' => array(
                                            'format' => 'Y-m-d H:i', // Format datetime.
                                            'maxDate' => false, // By default there is not maximum date , set a date in the datetime format.
                                            'minDate' => false, // By default minimum date will be current day, set a date in the datetime format.
                                            'timepicker' => true, // Show timepicker.
                                            'datepicker' => true, // Show datepicker.
                                            'defaultTime' => '12:00' // If the input value is empty, timepicker will set time use defaultTime.
                                        ),
                                    ),
                                    'event_location' => array(
                                        'type' => 'text',
                                        'value' => '',
                                        'attr' => array(),
                                        'label' => esc_html__('Event Location', 'triathlon'),
                                        'desc' => esc_html__('Please add location for event.', 'triathlon'),
                                        'help' => esc_html__('', 'triathlon'),
                                    ),
                                    'entry_fee' => array(
                                        'type' => 'text',
                                        'value' => '',
                                        'attr' => array(),
                                        'label' => esc_html__('Entry Fee', 'triathlon'),
                                        'desc' => esc_html__('', 'triathlon'),
                                        'help' => esc_html__('', 'triathlon'),
                                    ),
                                    'prize' => array(
                                        'type' => 'text',
                                        'value' => '',
                                        'attr' => array(),
                                        'label' => esc_html__('Money Prize', 'triathlon'),
                                        'desc' => esc_html__('', 'triathlon'),
                                        'help' => esc_html__('', 'triathlon'),
                                    ),
                                    'book_now_text' => array(
                                        'type' => 'text',
                                        'value' => '',
                                        'attr' => array(),
                                        'label' => esc_html__('Add Book now button text', 'triathlon'),
                                        'desc' => esc_html__('', 'triathlon'),
                                        'help' => esc_html__('', 'triathlon'),
                                    ),
                                    'book_now_url' => array(
                                        'type' => 'text',
                                        'value' => '',
                                        'attr' => array(),
                                        'label' => esc_html__('Add Book now button url', 'triathlon'),
                                        'desc' => esc_html__('', 'triathlon'),
                                        'help' => esc_html__('', 'triathlon'),
                                    ),
                                    'country_flag' => array(
                                        'type' => 'upload',
                                        'value' => '',
                                        'attr' => array(),
                                        'label' => esc_html__('Add country flag image', 'triathlon'),
                                        'desc' => esc_html__('', 'triathlon'),
                                        'help' => esc_html__('', 'triathlon'),
                                    ),
                                    'extras' => array(
                                        'type' => 'addable-popup',
                                        'label' => esc_html__('Extras', 'triathlon'),
                                        'popup-title' => esc_html__('Add/Edit Extras', 'triathlon'),
                                        'desc' => esc_html__('', 'triathlon'),
                                        'template' => '{{=tab_title}}',
                                        'popup-options' => array(
                                            'tab_title' => array(
                                                'type' => 'text',
                                                'label' => esc_html__('Title', 'triathlon')
                                            ),
                                            'tab_content' => array(
                                                'type' => 'textarea',
                                                'label' => esc_html__('Content', 'triathlon')
                                            )
                                        ),
                                    ),
                                /* 'event_map' => array(
                                  'type'  => 'map',
                                  'value' => array(
                                  'coordinates' => array(
                                  'lat'   => -34,
                                  'lng'   => 150,
                                  )
                                  ),
                                  'attr'  => array(),
                                  'label' => esc_html__('Event Map', 'triathlon'),
                                  'desc'  => esc_html__('', 'triathlon'),
                                  'help'  => esc_html__('', 'triathlon'),
                                  ) */
                                )
                            ),
                        ),
                    )
                )
            ),
        )
    ),
);
