<?php

if (!defined('FW')) {
    die('Forbidden');
}

$options = array(
    'settings' => array(
        'type' => 'box',
        'options' => array(
            'sub_headers' => array(
                'title' => esc_html__('Sub-Header Settings', 'triathlon'),
                'type' => 'tab',
                'options' => array(
                    'enable_subheader' => array(
                        'type' => 'switch',
                        'value' => 'enable',
                        'label' => esc_html__('Subheader', 'triathlon'),
                        'desc' => esc_html__('Enable or Disable Subheader', 'triathlon'),
                        'left-choice' => array(
                            'value' => 'enable',
                            'label' => esc_html__('Enable', 'triathlon'),
                        ),
                        'right-choice' => array(
                            'value' => 'disable',
                            'label' => esc_html__('Disable', 'triathlon'),
                        ),
                    ),
                    'sub_heading' => array(
                        'type' => 'textarea',
                        'value' => '',
                        'label' => esc_html__('Sub Heading', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                    ),
                    'sub_heading_bg' => array(
                        'type' => 'color-picker',
                        'value' => '#FDB62D',
                        'label' => esc_html__('Sub Heading bg color', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                    ),
                    'subheader_bg_image' => array(
                        'type' => 'upload',
                        'label' => esc_html__('Upload background image', 'triathlon'),
                        'desc' => esc_html__('It will override background color', 'triathlon'),
                        'images_only' => true,
                    ),
                    'sub_heading_text' => array(
                        'type' => 'color-picker',
                        'value' => '#FFF',
                        'label' => esc_html__('Sub Heading Text color', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                    ),
                )
            ),
            'page_settings' => array(
                'title' => esc_html__('Page Settings', 'triathlon'),
                'type' => 'tab',
                'options' => array(
                    'page_settings_type' => array(
                        'type' => 'radio',
                        'value' => 'image',
                        'label' => esc_html__('Banner Type', 'triathlon'),
                        'desc' => esc_html__('Choose settings for your page. Simple Image or Slider', 'triathlon'),
                        'choices' => array(
                            'image' => esc_html__('1 - Image', 'triathlon'),
                            'slider' => esc_html__('2 - Slider', 'triathlon'),
                        ),
                        // Display choices inline instead of list
                        'inline' => true,
                    ),
                    'page_inner_settings' => array(
                        'title' => esc_html__('', 'triathlon'),
                        'type' => 'box',
                        'options' => array(
                            'image-box' => array(
                                'title' => esc_html__('Image', 'triathlon'),
                                'type' => 'tab',
                                'options' => array(
                                    'page_image' => array(
                                        'type' => 'html',
                                        'html' => 'Upload Image',
                                        'label' => esc_html__('Upload Image', 'triathlon'),
                                        'desc' => esc_html__('Upload your featured image as banner', 'triathlon'),
                                    )
                                )
                            ),
                            'slider-box' => array(
                                'title' => esc_html__('Slider', 'triathlon'),
                                'type' => 'tab',
                                'options' => array(
                                    'slides' => array(
                                        'type' => 'addable-popup',
                                        'label' => esc_html__('Add Slider', 'triathlon'),
                                        'template' => '{{- slider_title }}',
                                        'popup-title' => esc_html__('Add Slide', 'triathlon'),
                                        'size' => 'small', // small, medium, large
                                        'limit' => 0, // limit the number of popup`s that can be added
                                        'popup-options' => array(
                                            'slider_title' => array(
                                                'label' => esc_html__('Title', 'triathlon'),
                                                'type' => 'text',
                                                'desc' => esc_html__('Enter Slider title.', 'triathlon'),
                                            ),
                                            'slider_date' => array(
                                                'label' => esc_html__('Date', 'triathlon'),
                                                'type' => 'text',
                                                'desc' => esc_html__('Enter Slider date.eg : July 29, 2015', 'triathlon'),
                                            ),
                                            'slider_image' => array(
                                                'type' => 'upload',
                                                'label' => esc_html__('Upload Image', 'triathlon'),
                                                'desc' => esc_html__('Upload your slider image.', 'triathlon'),
                                                'images_only' => true,
                                            ),
                                            'slider_button_tite' => array(
                                                'label' => esc_html__('Button Title', 'triathlon'),
                                                'value' => 'Book Now',
                                                'type' => 'text',
                                                'desc' => esc_html__('Enter Button Title.', 'triathlon'),
                                            ),
                                            'slider_button_link' => array(
                                                'label' => esc_html__('link', 'triathlon'),
                                                'type' => 'text',
                                                'value' => '#',
                                                'desc' => esc_html__('Enter link.', 'triathlon'),
                                            ),
                                            'slider_description' => array(
                                                'label' => esc_html__('Description', 'triathlon'),
                                                'type' => 'textarea',
                                                'desc' => esc_html__('Enter Description.', 'triathlon'),
                                            ),
                                        ),
                                    ),
                                    'slider-box-1' => array(
                                        'title' => esc_html__('Slider Settings', 'triathlon'),
                                        'type' => 'box',
                                        'options' => array(
                                            'enable_slider_nav' => array(
                                                'type' => 'switch',
                                                'value' => 'enable',
                                                'label' => esc_html__('Navigation', 'triathlon'),
                                                'desc' => esc_html__('Enable or Disable Slider Naviagtion', 'triathlon'),
                                                'left-choice' => array(
                                                    'value' => 'enable',
                                                    'label' => esc_html__('Enable', 'triathlon'),
                                                ),
                                                'right-choice' => array(
                                                    'value' => 'disable',
                                                    'label' => esc_html__('Disable', 'triathlon'),
                                                ),
                                            ),
                                            'autoplay' => array(
                                                'type' => 'switch',
                                                'value' => 'enable',
                                                'label' => esc_html__('Autoplay', 'triathlon'),
                                                'desc' => esc_html__('Enable or Disable Slider Autoplay', 'triathlon'),
                                                'left-choice' => array(
                                                    'value' => 'enable',
                                                    'label' => esc_html__('Enable', 'triathlon'),
                                                ),
                                                'right-choice' => array(
                                                    'value' => 'disable',
                                                    'label' => esc_html__('Disable', 'triathlon'),
                                                ),
                                            ),
                                            'slider_speed' => array(
                                                'type' => 'text',
                                                'value' => 300,
                                                'label' => esc_html__('Slider Speed', 'triathlon'),
                                                'desc' => esc_html__('For Example 300 equals to 0.3 seconds.', 'triathlon'),
                                            )
                                        )
                                    ),
                                )
                            ),
                        ),
                    )
                )
            ),
        )
    )
);

