<?php

if (!defined('FW')) {
    die('Forbidden');
}

$options = array(
    'video_settings_advanced' => array(
        'title' => 'Video Settings',
        'type' => 'box',
        'options' => array(
            'sub_headers' => array(
                'title' => esc_html__('Sub-Header Settings', 'triathlon'),
                'type' => 'tab',
                'options' => array(
                    'enable_subheader' => array(
                        'type' => 'switch',
                        'value' => 'enable',
                        'label' => esc_html__('Subheader', 'triathlon'),
                        'desc' => esc_html__('Enable or Disable Subheader', 'triathlon'),
                        'left-choice' => array(
                            'value' => 'enable',
                            'label' => esc_html__('Enable', 'triathlon'),
                        ),
                        'right-choice' => array(
                            'value' => 'disable',
                            'label' => esc_html__('Disable', 'triathlon'),
                        ),
                    ),
                    'sub_heading' => array(
                        'type' => 'textarea',
                        'value' => '',
                        'label' => esc_html__('Sub Heading', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                    ),
                    'sub_heading_bg' => array(
                        'type' => 'color-picker',
                        'value' => '#FDB62D',
                        'label' => esc_html__('Sub Heading bg color', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                    ),
                    'sub_heading_text' => array(
                        'type' => 'color-picker',
                        'value' => '#FFF',
                        'label' => esc_html__('Sub Heading Text color', 'triathlon'),
                        'desc' => esc_html__('', 'triathlon'),
                    ),
                )
            ),
            'video-box' => array(
                'title' => esc_html__('Video', 'triathlon'),
                'type' => 'tab',
                'options' => array(
                    'video_link' => array(
                        'type' => 'text',
                        'label' => esc_html__('Add Video link', 'triathlon'),
                        'desc' => esc_html__('You can add video link from Youtube,Vimeo,Dailymotion,and from your own server link', 'triathlon'),
                    ),
                )
            ),
        )
    ),
);
