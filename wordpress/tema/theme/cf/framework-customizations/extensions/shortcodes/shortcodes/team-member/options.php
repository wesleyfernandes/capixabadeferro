<?php

if (!defined('FW')) {
    die('Forbidden');
}

$options = array(
    'heading' => array(
        'label' => esc_html__('Title', 'triathlon'),
        'desc' => esc_html__('Enter Your Main Title Here.', 'triathlon'),
        'type' => 'text',
        'value' => ''
    ),
    'sub_heading' => array(
        'label' => esc_html__('Sub Title', 'triathlon'),
        'desc' => esc_html__('Enter Your Sub Title Here', 'triathlon'),
        'type' => 'text',
        'value' => ''
    ),
    'add_team_person' => array(
        'type' => 'addable-popup',
        'label' => esc_html__('Add Team Member', 'triathlon'),
        'desc' => esc_html__('', 'triathlon'),
        'template' => '{{- person_name }}',
        'popup-title' => esc_html__('Add Team Member', 'triathlon'),
        'size' => 'small', // small, medium, large
        'limit' => 0, // limit the number of popup`s that can be added
        'add-button-text' => esc_html__('Add', 'triathlon'),
        'sortable' => true,
        'popup-options' => array(
            'person_image' => array(
                'label' => esc_html__('Team Member Image', 'triathlon'),
                'desc' => esc_html__('Either upload a new, or choose an existing image from your media library', 'triathlon'),
                'type' => 'upload'
            ),
            'person_name' => array(
                'label' => esc_html__('Team Member Name', 'triathlon'),
                'desc' => esc_html__('Name of the person', 'triathlon'),
                'type' => 'text',
                'value' => ''
            ),
            'person_job' => array(
                'label' => esc_html__('Team Member Job Title', 'triathlon'),
                'desc' => esc_html__('Designation of the person.', 'triathlon'),
                'type' => 'text',
                'value' => ''
            ),
            'link' => array(
                'label' => esc_html__('Team Member Link', 'triathlon'),
                'desc' => esc_html__('Enter Your Link Here.', 'triathlon'),
                'type' => 'text',
                'value' => ''
            ),
            'link_target' => array(
                'type' => 'switch',
                'value' => '_blank',
                'attr' => array(),
                'label' => esc_html__('Link Target', 'triathlon'),
                'desc' => esc_html__('', 'triathlon'),
                'left-choice' => array(
                    'value' => '_blank',
                    'label' => esc_html__('Blank', 'triathlon'),
                ),
                'right-choice' => array(
                    'value' => '_self',
                    'label' => esc_html__('Self', 'triathlon'),
                ),
            ),
        ),
    ),
);
