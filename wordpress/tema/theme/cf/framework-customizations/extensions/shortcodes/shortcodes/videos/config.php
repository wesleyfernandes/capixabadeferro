<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg = array(
	'page_builder' => array(
		'title' => esc_html__('Promotional Videos', 'triathlon'),
		'description' => esc_html__('Display simple Videos.', 'triathlon'),
		'tab' => esc_html__('Triathlon', 'triathlon'),
		'popup_size' => 'small' // can be large, medium or small
	)
);