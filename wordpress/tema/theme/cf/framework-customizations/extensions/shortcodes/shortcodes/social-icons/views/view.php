<?php
if (!defined('FW'))
    die('Forbidden');
/**
 * @var $atts
 * Get Shortcodes Attributes
 */

$title = (isset($atts['title']) && !empty($atts['title'])) ? $atts['title'] : '';
$description = (isset($atts['description']) && !empty($atts['description'])) ? $atts['description'] : '';
?>

<div class="col-sm-10 col-sm-offset-1">
    <div class="fullwidth contact-textbox text-center">
        <header class="po-headingborder">
            <h2><?php echo esc_attr($title); ?></h2>
        </header>
        <?php echo  force_balance_tags( $description ); ?>
    </div>
</div>
<div class="fullwidth po-contactsocial">
    <?php if(isset($atts['add_social_icons'])){
            foreach($atts['add_social_icons'] as $social_icons){
        ?>
    <div class="col-md-20">
        <article>
            <figure>
                <i class="<?php echo (isset($social_icons['social_icon']) && !empty($social_icons['social_icon'])) ? esc_attr($social_icons['social_icon']) : '' ; ?>"></i>
            </figure>
            <h3>
                <a target="<?php echo (isset($social_icons['social_link_target']) && !empty($social_icons['social_link_target'])) ? esc_attr($social_icons['social_link_target']) : '' ; ?>" href="<?php echo (isset($social_icons['social_link']) && !empty($social_icons['social_link'])) ? esc_attr($social_icons['social_link']) : '' ; ?>">
                <?php echo (isset($social_icons['social_title']) && !empty($social_icons['social_title'])) ? esc_attr($social_icons['social_title']) : '' ; ?>
                </a>
            </h3>
            <p>
                <a target="<?php echo (isset($social_icons['social_link_target']) && !empty($social_icons['social_link_target'])) ? esc_attr($social_icons['social_link_target']) : '' ; ?>" href="<?php echo (isset($social_icons['social_link']) && !empty($social_icons['social_link'])) ? esc_attr($social_icons['social_link']) : '' ; ?>">
                    <?php echo (isset($social_icons['social_description']) && !empty($social_icons['social_description'])) ? esc_attr($social_icons['social_description']) : '' ; ?>
                </a>
            </p>
        </article>
    </div>
    <?php } //endforeach
        } //endif
    ?>
</div>