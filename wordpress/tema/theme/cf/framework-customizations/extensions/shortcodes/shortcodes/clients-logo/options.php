<?php

if (!defined('FW')) {
    die('Forbidden');
}

$options = array(
    'heading' => array(
        'label' => esc_html__('Heading', 'triathlon'),
        'desc' => esc_html__('Enter Your Section Heading Here.', 'triathlon'),
        'type' => 'text'
    ),
    'slides' => array(
        'type' => 'addable-popup',
        'label' => esc_html__('Logo Image', 'triathlon'),
        'template' => '{{- client_title }}',
        'popup-title' => null,
        'size' => 'small', // small, medium, large
        'limit' => 0, // limit the number of popup`s that can be added
        'popup-options' => array(
            'client_title' => array(
                'label' => esc_html__('Client Name', 'triathlon'),
                'type' => 'text',
                'desc' => esc_html__('Enter Client Name.', 'triathlon'),
            ),
            'client_url' => array(
                'label' => esc_html__('URL', 'triathlon'),
                'value' => '#',
                'type' => 'text',
                'desc' => esc_html__('Enter client website url.', 'triathlon'),
            ),
            'client_image' => array(
                'type' => 'upload',
                'label' => esc_html__('Upload Image', 'triathlon'),
                'desc' => esc_html__('Upload your image.', 'triathlon'),
                'images_only' => true,
            ),
            /**
             * New Window choice
             */
            'client_logo_target' => array(
                'type' => 'switch',
                'value' => '_self',
                'label' => esc_html__('Open in New Window', 'triathlon'),
                'desc' => esc_html__('The links will be opened into new tab or window when your visitors clicked on the link.', 'triathlon'),
                'left-choice' => array(
                    'value' => '_blank',
                    'label' => esc_html__('Enable', 'triathlon'),
                ),
                'right-choice' => array(
                    'value' => '_self',
                    'label' => esc_html__('Disable', 'triathlon'),
                ),
            ),
        ),
    ),
);
