<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
$options = array(
	'heading' => array(
		'type'  => 'text',
		'value' => '',
		'label' => esc_html__('Body Mass Index Title', 'triathlon'),
		'desc'  => esc_html__('', 'triathlon'),
	),
	'bg_color' => array(
		'type'  => 'rgba-color-picker',
		'value' => 'rgba(0,0,0,0.5)',
		'attr'  => array(),
		'label' => esc_html__('Background color', 'triathlon'),
		'desc'  => esc_html__('', 'triathlon'),
		'help'  => esc_html__('', 'triathlon'),
	),
);