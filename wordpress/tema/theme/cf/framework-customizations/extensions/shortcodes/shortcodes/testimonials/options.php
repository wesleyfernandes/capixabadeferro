<?php

if (!defined('FW')) {
    die('Forbidden');
}

$options = array(
    'heading' => array(
        'label' => esc_html__('Heading', 'triathlon'),
        'desc' => esc_html__('Enter the Heading Here.', 'triathlon'),
        'type' => 'text'
    ),
    'testimonials_add' => array(
        'type' => 'addable-popup',
        'label' => esc_html__('Testimonials', 'triathlon'),
        'desc' => esc_html__('Add Your Testimonial Here.', 'triathlon'),
        'template' => '{{- testimonial_title }}',
        'popup-title' => esc_html__('Add Testimonial', 'triathlon'),
        'size' => 'small', // small, medium, large
        'limit' => 0, // limit the number of popup`s that can be added
        'add-button-text' => esc_html__('Add', 'triathlon'),
        'sortable' => true,
        'popup-options' => array(
            'testimonial_title' => array(
                'label' => esc_html__('Title', 'triathlon'),
                'desc' => esc_html__('Enter the Testimonial Title', 'triathlon'),
                'type' => 'text'
            ),
            'testimonial_author' => array(
                'label' => esc_html__('Author', 'triathlon'),
                'desc' => esc_html__('Enter the Testimonial Author Name', 'triathlon'),
                'type' => 'text'
            ),
            'testimonial_content' => array(
                'label' => esc_html__('Testimonial', 'triathlon'),
                'desc' => esc_html__('Enter the testimonial Content Here', 'triathlon'),
                'type' => 'textarea',
                'teeny' => false
            ),
            'testimonial_icon' => array(
                'type' => 'icon',
                'value' => 'fa-smile-o',
                'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
                'label' => esc_html__('Icon', 'triathlon'),
                'desc' => esc_html__('Choose Icon For Your Testimonial', 'triathlon'),
            ),
        ),
    ),
    'arrow_links' => array(
        'type' => 'switch',
        'value' => 'on',
        'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
        'label' => esc_html__('Arrows', 'triathlon'),
        'desc' => esc_html__('Enable or Disable Arrows ', 'triathlon'),
        'left-choice' => array(
            'value' => 'off',
            'label' => esc_html__('OFF', 'triathlon'),
        ),
        'right-choice' => array(
            'value' => 'on',
            'label' => esc_html__('ON', 'triathlon'),
        ),
    )
);
