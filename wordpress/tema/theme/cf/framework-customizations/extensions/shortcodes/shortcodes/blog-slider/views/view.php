<?php if (!defined('FW')) die( 'Forbidden' );
/**
 * @var $atts
 */
$width  = '320';
$height = '320';

$rand_id	= fw_unique_increment();
?>
<div class="container">
<div class="row">
	<div class="news-event-section">
		<?php if( isset( $atts['heading'] ) && !empty( $atts['heading'] ) ) {?>
			<h2><?php echo esc_attr( $atts['heading'] );?></h2>
		<?php }?>
		
		<?php if( isset( $atts['sub_heading'] ) && !empty( $atts['sub_heading'] ) ) {?>
			<div class="description">
			  <p><?php echo esc_attr( $atts['sub_heading'] );?></p>
			</div>
		<?php }?>
		
		<div class="news-event-slider events-slider-<?php echo esc_attr( $rand_id );?>">
		
		<?php
			global $paged;
			if (empty($paged)) $paged = 1;
			
			// Count Total Pssts
			$show_posts    = $atts['show_posts'] ? $atts['show_posts'] : '-1';        
			$args = array('posts_per_page' => "-1", 'post_type' => 'post', 'order' => 'DESC', 'orderby' => 'ID', 'post_status' => 'publish', 'ignore_sticky_posts' => 1);
			$query 		= new WP_Query( $args );
			$count_post = $query->post_count;        
			
			//Main Query	
			$args 		= array('posts_per_page' => "$show_posts", 'post_type' => 'post', 'paged' => $paged, 'order' => 'DESC', 'orderby' => 'ID', 'post_status' => 'publish', 'ignore_sticky_posts' => 1);
				
			$query 		= new WP_Query($args);
			while($query->have_posts()) : $query->the_post();
			
			$thumnail	= triathlon_prepare_thumbnail($query->ID ,$width,$height);
			
			if( isset( $thumnail ) && $thumnail ){
				$thumnail	= $thumnail;
			} else{
				$thumnail	= get_template_directory_uri(). '/img/no-image.png';
			}
			
			if (!function_exists('fw_get_db_post_option')) {
				return;
			} else {
				$blog_standard_image 		= fw_get_db_post_option(get_the_ID(), 'blog_standard_image', true);
			}
			?>
			
			
				
				  <div class="item"><a href="<?php the_permalink(); ?>"><img src="<?php echo esc_url($thumnail);?>" alt="<?php echo esc_attr(get_the_title());?>"></a>
					<div class="foot"> 
                    	<a class="foot" href="<?php the_permalink(); ?>"> 
                            <i class="icon-arrow-right-latest-races"></i> <span class="title"><?php the_title();?></span> 
                            <span class="date"> <em class="fa fa-calendar-o"></em> 
                            <em><?php echo date_i18n(get_option('date_format'), strtotime(get_the_date())); ?></em> 
                            </span>
                        </a> 
                        </div>
				  </div>
				<?php endwhile; wp_reset_postdata(); ?>
				
			 </div>
	</div>
	 <script>
			jQuery(document).ready(function(e) {
				jQuery(".events-slider-<?php echo esc_js( $rand_id );?>").owlCarousel({
					autoPlay: false,
					items : 3,
					navigation : false,
					itemsDesktop : [1199,3],
					itemsDesktopSmall : [979,2],
					pagination : true,
				});
			});
		</script>
</div>
</div>