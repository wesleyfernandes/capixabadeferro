<?php if (!defined('FW')) die( 'Forbidden' );
/**
 * @var $atts
 */
?>
<div class="fw-heading fw-heading-<?php echo sanitize_html_class( $atts['heading'] ); ?> <?php echo !empty($atts['centered']) ? 'fw-heading-center' : ''; ?>">
	<?php 
		if( isset( $atts['heading'] ) && !empty( $atts['heading'] ) ) {
			$heading = "<{$atts['heading']} class=fw-special-title>{$atts['title']}</{$atts['heading']}>"; 
		} else{
			$heading = "<h1 class='fw-special-title'>".$atts['title'].'</h1>'; 
			echo( $heading );
		}
		
	?>

</div>