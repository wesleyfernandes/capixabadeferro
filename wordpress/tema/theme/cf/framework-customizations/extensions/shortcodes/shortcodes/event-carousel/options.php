<?php

if (!defined('FW')) {
    die('Forbidden');
}

$options = array(
    'upcoming_settings' => array(
        'type' => 'tab',
        'title' => esc_html__('Upcoming Events Settings', 'triathlon'),
        'options' => array(
            'latest_heading' => array(
                'type' => 'text',
                'value' => 'Latest Races',
                'label' => esc_html__('Heading', 'triathlon'),
                'desc' => esc_html__('', 'triathlon'),
            ),
            'total_up_races' => array(
                'type' => 'slider',
                'value' => 10,
                'properties' => array(
                    'min' => 0,
                    'max' => 20,
                    'sep' => 1,
                ),
                'label' => esc_html__('Show No of Races', 'triathlon'),
            ),
            'show_description' => array(
                'type' => 'switch',
                'value' => 'hide',
                'label' => esc_html__('Description', 'triathlon'),
                'desc' => esc_html__('', 'triathlon'),
                'left-choice' => array(
                    'value' => 'hide',
                    'label' => esc_html__('Hide', 'triathlon'),
                ),
                'right-choice' => array(
                    'value' => 'show',
                    'label' => esc_html__('Show', 'triathlon'),
                ),
            ),
            'up_race_image' => array(
                'type'  => 'upload',
                'attr'  => array(),
                'label' => esc_html__('Image', 'triathlon'),
                'desc'  => esc_html__('Upload Image for your up coming races.', 'triathlon'),
                'images_only' => true,
            ),
            'latest_text' => array(
                'type' => 'text',
                'value' => '',
                'label' => esc_html__('Button Text', 'triathlon'),
                'desc' => esc_html__('', 'triathlon'),
            ),
            'latest_link' => array(
                'type' => 'text',
                'value' => '',
                'label' => esc_html__('Button Link', 'triathlon'),
                'desc' => esc_html__('', 'triathlon'),
            ),
        )
    ),
);
