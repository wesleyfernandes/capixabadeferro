<?php
if (!defined('FW'))
    die('Forbidden');
/**
 * @var $atts
 */
?>
<?php 
/**
 * Get Contact Info Shortcode Variables
 */
    
$heading = (isset($atts['heading']) && !empty($atts['heading'])) ? $atts['heading'] : '<h2>GET IN TOUCH WITH US</h2>';
$description = (isset($atts['description']) && !empty($atts['description'])) ? $atts['description'] : '';
?>

        <div class="col-sm-10 col-sm-offset-1">
            <div class="fullwidth contact-textbox text-center">
                <header class="po-headingborder">
                    <h2><?php echo esc_attr($heading); ?></h2>
                </header>
                <?php echo force_balance_tags( $description ); ?>
            </div>
        </div>
        <!-- Contact info -->
        <div class="po-contactinfo-sec fullwidth">
            <?php
                if($atts['add_contacts']) {
                    foreach($atts['add_contacts'] as $contact){
            ?>
            <div class="col-sm-4 col-sx-12 text-center">
                <article>
                    <figure class="po-color">
                        <i class="<?php echo (isset($contact['contact_icon']) && !empty($contact['contact_icon'])) ? esc_attr($contact['contact_icon']) : ''; ?> float"></i>
                    </figure>
                    <?php echo (isset($contact['address']) && !empty($contact['address'])) ? $contact['address'] : ''; ?>
                </article>
            </div>
            <?php }// endForeach
            }//endif 
            ?>
        </div>
        <!-- Contact info End -->
       
