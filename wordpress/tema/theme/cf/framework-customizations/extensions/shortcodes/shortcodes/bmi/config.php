<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg = array(
	'page_builder' => array(
		'title' => esc_html__('BMI', 'triathlon'),
		'description' => esc_html__('Calculate Body Mass Index.', 'triathlon'),
		'tab' => esc_html__('Triathlon', 'triathlon'),
		'popup_size' => 'small'
	)
);