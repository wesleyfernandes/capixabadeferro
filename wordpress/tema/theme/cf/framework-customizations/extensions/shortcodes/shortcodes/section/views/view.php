<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
/**
 * @var $atts
 */

$bg_color = '';
if ( ! empty( $atts['background_color'] ) ) {
	$bg_color = 'background-color:' . $atts['background_color'] . ';';
}

$bg_image = '';
if ( ! empty( $atts['background_image'] ) && ! empty( $atts['background_image']['data']['icon'] ) ) {
	$bg_image = 'background-image:url(' . $atts['background_image']['data']['icon'] . ');';
}

$margin_top 	= '';
$margin_bottom  = '';
$padding_top 	= '';
$padding_bottom = '';
$bg_repeat 		= '';
$bg_position_x 		= '';
$bg_position_y 		= '';
$custom_id 		= '';
$custom_classes = '';

if( isset( $atts['custom_styling'] ) && $atts['custom_styling'] == 'show' ) {
	if(isset($atts['margin_top'])) :  $margin_top = 'margin-top:'.$atts['margin_top'].'px;';  endif;
	if(isset($atts['margin_bottom'])) :  $margin_bottom = 'margin-bottom:'.$atts['margin_bottom'].'px;';  endif;
	if(isset($atts['padding_top'])) :  $padding_top = 'padding-top:'.$atts['padding_top'].'px;';  endif;
	if(isset($atts['padding_bottom'])) :  $padding_bottom = 'padding-bottom:'.$atts['padding_bottom'].'px;';  endif;
	
}
if(isset($atts['background_repeat'])) :  $bg_repeat = 'background-repeat:'.$atts['background_repeat'].';';  endif;
if(isset($atts['custom_id'])) :  $custom_id = $atts['custom_id'];  endif;
if(isset($atts['custom_classes'])) :  $custom_classes = $atts['custom_classes'];  endif;
$custom_id	= isset(  $atts['custom_id'] ) &&  !empty( $atts['custom_id'] ) ? 'id='.$atts['custom_id'].'' : '';
$bg_position_x	= isset(  $atts['positioning_x'] ) &&  !empty( $atts['positioning_x'] ) ? $atts['positioning_x'].'%' : '0%';
$bg_position_y	= isset(  $atts['positioning_y'] ) &&  !empty( $atts['positioning_y'] ) ? $atts['positioning_y'].'%' : '100%';
$bg_positioning = 'background-position:'.$bg_position_x.' '.$bg_position_y;

$bg_video_data_attr    = '';
$section_extra_classes = '';
if ( ! empty( $atts['video'] ) ) {
	$filetype           = wp_check_filetype( $atts['video'] );
	$filetypes          = array( 'mp4' => 'mp4', 'ogv' => 'ogg', 'webm' => 'webm', 'jpg' => 'poster' );
	$filetype           = array_key_exists( (string) $filetype['ext'], $filetypes ) ? $filetypes[ $filetype['ext'] ] : 'video';
	$bg_video_data_attr = 'data-wallpaper-options="' . fw_htmlspecialchars( json_encode( array( 'source' => array( $filetype => $atts['video'] ) ) ) ) . '"';
	$section_extra_classes .= ' background-video';
}

$section_extra_classes .= ' '.$custom_classes;
$section_style   = ( $bg_color || $bg_image || $padding_top || $padding_bottom || $margin_bottom || $margin_top ) ? 'style="' . $bg_color . $bg_image . $margin_top . $margin_bottom . $padding_top . $padding_bottom . $bg_repeat . $bg_positioning .'"' : '';
$container_class = ( isset( $atts['is_fullwidth'] ) && $atts['is_fullwidth'] ) ? 'container-fluid' : 'container';

$tg_sidebar	 	 = 'full';
if (function_exists('fw_ext_sidebars_get_current_position')) {
	$current_position = fw_ext_sidebars_get_current_position();
	if( $current_position !== 'full' &&  ( $current_position == 'left' || $current_position == 'right' ) ) {
		$container_class	= 'wrapper-container';
	}
}

?>
<section class="tg-main-section haslayout <?php echo triathlon_esc_specialchars( $section_extra_classes ) ?>" <?php echo esc_attr( $custom_id );?> <?php echo triathlon_esc_specialchars( $section_style ); ?> <?php echo triathlon_esc_specialchars( $bg_video_data_attr ); ?>>
	<div class="<?php echo triathlon_esc_specialchars( $container_class ); ?>">
		<?php if( isset( $atts['section_heading'] ) && !empty( $atts['section_heading'] ) ){?>
       	 	<h2 class="tg-section-title"><?php echo esc_attr( $atts['section_heading'] ); ?></h2>
		<?php }?>
		<?php echo do_shortcode( $content ); ?>
	</div>
</section>
