<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => esc_html__('Section Heading', 'triathlon'),
	'description'   => esc_html__('Add a Section Heading', 'triathlon'),
	'tab'           => esc_html__('Triathlon', 'triathlon'),
);