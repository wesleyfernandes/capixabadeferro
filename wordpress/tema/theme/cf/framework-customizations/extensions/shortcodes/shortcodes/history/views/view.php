<?php
if (!defined('FW'))
    die('Forbidden');
/**
 * @var $atts
 */

?>

<div class="trithlon-history haslayout">
    <?php if (isset($atts['heading']) || isset($atts['sub_heading'])) { ?>
        <div class="head-section">
            <?php if (!empty($atts['heading'])) { ?>
                <div class="border-title">
                    <h2><?php echo esc_attr($atts['heading']); ?></h2>
                </div>
            <?php } ?>
            <?php if (!empty( $atts['sub_heading'] )) { ?>
                <span class="title"><?php echo esc_attr($atts['sub_heading']); ?></span>
            <?php } ?>
        </div>
    <?php } //endif ?>
    <?php if (isset($atts['description']) && !empty($atts['description'])) { ?>
        <div class="description">
            <?php echo balanceTags($atts['description']); ?>
        </div>
    <?php }//endif ?>
    <?php if (isset($atts['gallery']) && !empty($atts['gallery']) && is_array($atts['gallery'])) { ?>
    <div class="three-columns row">
            <?php foreach($atts['gallery'] as $gallery) { ?>
            <div class="col-sm-4">
                <img src="<?php echo esc_url($gallery['url']); ?>" alt="<?php echo get_bloginfo('name'); ?>">
            </div>
            <?php }//endforeach ?>
    </div>
     <?php }//endif ?>
</div>
