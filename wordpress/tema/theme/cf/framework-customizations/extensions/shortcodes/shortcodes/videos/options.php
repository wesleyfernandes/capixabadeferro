<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'show_posts' => array(
		'type'  => 'slider',
		'value' => 9,
		'properties' => array(

			'min' => 0,
			'max' => 20,
			'sep' => 1,

		),
		'label' => esc_html__('Show No of Posts', 'triathlon'),
	),
	'show_pagination' => array(
		'type'  => 'select',
		'value' => 'yes',
		'label' => esc_html__('Show Pagination', 'triathlon'),
		'desc'  => esc_html__('', 'triathlon'),
		'choices' => array(
			'yes' => esc_html__('Yes', 'triathlon'),
			'no' => esc_html__('No', 'triathlon'),
		),
		'no-validate' => false,
	),
	
	'show_author' => array(
		'type'  => 'switch',
		'value' => 'show',
		'label' => esc_html__('Author', 'triathlon'),
		'desc'  => esc_html__('', 'triathlon'),
		'left-choice' => array(
			'value' => 'show',
			'label' => esc_html__('Show author', 'triathlon'),
		),
		'right-choice' => array(
			'value' => 'hide',
			'label' => esc_html__('Hide author', 'triathlon'),
		),
	),
	'show_description' => array(
		'type'  => 'switch',
		'value' => 'show',
		'label' => esc_html__('Description', 'triathlon'),
		'desc'  => esc_html__('', 'triathlon'),
		'left-choice' => array(
			'value' => 'show',
			'label' => esc_html__('Show Description', 'triathlon'),
		),
		'right-choice' => array(
			'value' => 'hide',
			'label' => esc_html__('Hide Description', 'triathlon'),
		),
	),
	'excerpt_length' => array(
		'type'  => 'slider',
		'value' => 130,
		'properties' => array(

			'min' => 0,
			'max' => 300,
			'sep' => 1,

		),
		'label' => esc_html__('Excerpt length', 'triathlon'),
	),
);