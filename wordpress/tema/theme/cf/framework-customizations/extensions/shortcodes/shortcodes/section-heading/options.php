<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'title'    => array(
		'type'  => 'text',
		'label' => esc_html__( 'Heading Title', 'triathlon' ),
		'desc'  => esc_html__( 'Write the heading title content', 'triathlon' ),
	),
	'heading' => array(
		'type'    => 'select',
		'label'   => esc_html__('Heading Size', 'triathlon'),
		'choices' => array(
			'h1' => 'H1',
			'h2' => 'H2',
			'h3' => 'H3',
			'h4' => 'H4',
			'h5' => 'H5',
			'h6' => 'H6',
		)
	),
	'centered' => array(
		'type'    => 'switch',
		'label'   => esc_html__('Centered', 'triathlon'),
//		'left-choice' => array(
//			'value' => 'no',
//			'label' => esc_html__('No', 'triathlon'),
//		),
//		'right-choice' => array(
//			'value' => 'yes',
//			'label' => esc_html__('Yes', 'triathlon'),
//		),
	)
);
