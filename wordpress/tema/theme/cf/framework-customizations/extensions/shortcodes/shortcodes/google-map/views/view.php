<?php
if (!defined('FW'))
    die('Forbidden');
/**
 * @var $atts
 * Get the Map shortcode attributes
 */
$title = (isset($atts['title']) && !empty($atts['title'])) ? $atts['title'] : 'LOCATE US ON MAP';
$map = (isset($atts['map']) && is_array($atts['map'])) ? $atts['map'] : '';
//fw_print($map);
$uni_flag = fw_unique_increment();
triathlon_enque_map_library();	
?>
<section class="po-mapsec fullwidth">
	<strong id="btn-map-<?php echo esc_attr( $uni_flag );?>" class="btn-theme black btn-map-<?php echo esc_attr( $uni_flag ); ?>">
    <span class="txt"><?php echo esc_attr($title); ?></span>
    <span class="round">
        <i class="icon-arrow-right-latest-races"></i>
    </span>
</strong>
	<div class="map-canvas fullwidth2 map_canvas_<?php echo esc_attr( $uni_flag ); ?>" id="map_canvas"></div>
	<script>
    jQuery(document).ready(function ($) {
      jQuery("#btn-map-<?php echo esc_js( $uni_flag ); ?>").click(function(){
			jQuery('#map_canvas').stop(true, true).slideToggle(400);
			initialize();
		});
		
		  function initialize() {
			  var mapOptions = {
				  center: new google.maps.LatLng(<?php echo esc_js( $atts['map']['coordinates']['lat']);?>, <?php  echo esc_js( $atts['map']['coordinates']['lng']);?>),
				  zoom: 11,
				  mapTypeId: google.maps.MapTypeId.ROADMAP
			  };
			  var mapClass = jQuery('.map_canvas_<?php echo esc_js( $uni_flag ); ?>');
			  var map = new google.maps.Map(mapClass[0],mapOptions);
		  }
		
		  initialize();
    });
</script>
</section>