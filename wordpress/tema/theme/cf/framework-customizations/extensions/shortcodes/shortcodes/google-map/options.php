<?php

if (!defined('FW')) {
    die('Forbidden');
}
$options = array(
    'quote' => array(
        'label' => esc_html__('Title', 'triathlon'),
        'desc' => esc_html__('Enter Your Map Title Here.', 'triathlon'),
        'type' => 'text'
    ),
    'map' => array(
        'type' => 'map',
        'value' => array(
            'coordinates' => array(
                'lat' => -34,
                'lng' => 150,
            )
        ),
        'attr' => array(),
        'label' => esc_html__('Map', 'triathlon'),
        'desc' => esc_html__('Choose Your Map Location', 'triathlon'),
    )
);
