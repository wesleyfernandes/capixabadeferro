<?php
if (!defined('FW'))
    die('Forbidden');
/**
 * @var $atts
 */
$uniq_flag = fw_unique_increment();
?>
<div class="our-sponsors">
    <?php if (!empty($atts['heading'])) : ?>
        <div class="border-title"><h2><?php echo esc_attr($atts['heading']); ?></h2></div>
<?php endif; ?>

    <div class="sponsors-slider tg-logo-slider-<?php echo esc_attr($uniq_flag); ?>">
        <?php
        foreach ($atts['slides'] as $logo) {
            if (isset($logo['client_image']['url']) && !empty($logo['client_image']['url'])) {
                if (isset($logo['client_url']) && !empty($logo['client_url'])) {
                    $url = $logo['client_url'];
                }
                
                $target = $logo['client_logo_target'];
                ?>
                <div class="item">
                    <div class="box">
                        <a target="<?php echo esc_attr($target); ?>" href="<?php echo esc_url($url); ?>"><img src="<?php echo esc_url($logo['client_image']['url']); ?>" alt="<?php echo esc_attr($logo['client_title']); ?>"></a>
                    </div>
                </div>
            <?php }
        } ?>
    </div>
    <script>
        jQuery(document).ready(function (e) {
            jQuery(".tg-logo-slider-<?php echo esc_js($uniq_flag); ?>").owlCarousel({
                autoPlay: true,
                items: 5,
                navigation: true,
                itemsDesktop: [1199, 5],
                itemsDesktopSmall: [979, 3],
                itemsTablet: [768, 4],
                pagination: false,
            });
        });
    </script>
</div>