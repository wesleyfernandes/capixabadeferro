<?php if (!defined('FW')) die( 'Forbidden' );
/**
 * @var $atts
 */
$bg_setting	= '';
$rand_id	=  fw_unique_increment();
if( isset( $atts['bg_color'] ) && !empty( $atts['bg_color'] ) ){
	$bg_setting	= 'style="background:'.$atts['bg_color'].'"';
}

?>


<div class="calculator haslayout" <?php echo triathlon_esc_specialchars( $bg_setting );?>>
	<div class="container">
		<div class="row">
			<?php
				if ( isset($atts['heading']) &&  !empty( $atts['heading'] )) {
					echo force_balance_tags( '<h2>'.$atts['heading'].'</h2>' );
				}
			?>
			
			
			<form id="contactForm" action="" class="calculator-form bmi-form-<?php echo esc_attr( $rand_id );?>" name="bmiForm">
				<fieldset class="row">
					<div class="col-md-8">
						<div class="col-sm-4">
							<input type="text" class="form-control bmi-weight" placeholder="<?php esc_attr_e('Weight in Pound','triathlon');?>" id="bmi-weight" name="weight">
						</div>
						<div class="col-sm-4">
							<input type="text" class="form-control bmi-height" placeholder="<?php esc_attr_e('Height in Inch','triathlon');?>" id="bmi-height" name="height">
						</div>
						<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="<?php esc_attr_e('BMI','triathlon');?>" name="bmi">
						</div>
					</div>
					<div class="col-md-4">
						<div class="col-xs-6">
							<div class="btn-theme red calculate-bmi"><span class="txt"><?php esc_html_e('Calculate','triathlon');?></span> <span class="round"> <i class="icon-arrow-right-latest-races"></i> </span> </div>
						</div>
						<div class="col-xs-6">
							<div class="btn-theme red bmi-reset"> <span class="txt"><?php esc_html_e('Reset','triathlon');?></span> <span class="round"> <i class="icon-arrow-right-latest-races"></i> </span> </div>
						</div>
					</div>
				</fieldset>
			</form>
			<ul>
				<li id="suggest-<?php echo esc_attr( $rand_id );?>"></li>
			</ul>
			<script>
				jQuery(document).ready(function($) {
					jQuery( document ).on('click','.calculate-bmi',function(){
						var weight = jQuery(this).parents('#contactForm').find('.bmi-weight').val();
						var height = jQuery(this).parents('#contactForm').find('.bmi-height').val();
						console.log(weight+'--'+height);
						var suggest = jQuery('#suggest-<?php echo esc_js( $rand_id );?>');
						if (weight > 0 && height > 0) {
							var finalBmi = (weight / (height * height)) * 703;
							document.bmiForm.bmi.value = "Your BMI is " + finalBmi;
							if (finalBmi < 18.5) {
								suggest.text('Meaning: That you are too thin.');
							}
							if (finalBmi > 18.5 && finalBmi < 25) {
								suggest.text("Meaning: That you are healthy.");
							}
							if (finalBmi > 25) {
								suggest.text("Meaning: That you have overweight.");
							}
						}
						else {
							alert("Please Fill in everything correctly");
						}
					
						return false;
					});
					
					jQuery( document ).on('click','.bmi-reset',function(){
						var suggest = jQuery('#suggest-<?php echo esc_js( $rand_id );?>');
						//jQuery( '.bmi-form-<?php echo esc_js( $rand_id );?>' ).reset();
						suggest.empty();
					});
				});
			</script>
		</div>
	</div>
</div>
