<?php
if (!defined('FW'))
    die('Forbidden');
/**
 * @var $atts
 */
global $post;
$width = '370';
$height = '175';
$uniq_flag	= fw_unique_increment();
?>
<div class="gallery-section">
    <div class="row"> 
        <?php
        if (isset($atts['heading']) && $atts['heading'] != '') {
            echo '<h2>' . esc_attr($atts['heading']) . '</h2>';
        }
        ?>

        <div class="gallery">
            <?php
            global $paged;
            if (empty($paged))
                $paged = 1;

            // Count Total Posts
            $show_posts = $atts['show_posts'] ? $atts['show_posts'] : '-1';
            $args = array('posts_per_page' => "-1", 'post_type' => 'tg_gallery', 'order' => 'DESC', 'orderby' => 'ID', 'post_status' => 'publish', 'ignore_sticky_posts' => 1);
            $query = new WP_Query($args);
            $count_post = $query->post_count;

            //Main Query	
            $args = array('posts_per_page' => "$show_posts", 'post_type' => 'tg_gallery', 'paged' => $paged, 'order' => 'DESC', 'orderby' => 'ID', 'post_status' => 'publish', 'ignore_sticky_posts' => 1);

            $query = new WP_Query($args);

            if (!function_exists('fw_get_db_post_option')) {
                return;
            } else {
                //do something
                $atts['show_filterable'];
            }
            ?>

            <!-- begin filter -->
            <?php
            if (isset($atts['show_filterable']) && $atts['show_filterable'] == 'show') {

                if (isset($cs_blog_cat) && $cs_blog_cat != '' && $cs_blog_cat != '0') {
                    $row_cat = $wpdb->get_row($wpdb->prepare("SELECT * from $wpdb->terms WHERE slug = %s", $cs_blog_cat));
                    //Do nothing
                } else {
                    $taxonomy = 'gallery_categories';
                    $terms = get_terms($taxonomy);
                    if ($terms && !is_wp_error($terms)) {
						?>
                            <ul id="gallery-cats" class="option-set option-set<?php echo esc_attr($uniq_flag); ?>">
                                <li class="select">
                                    <a href="javascript:;" class="btn-theme black" data-filter="*">
                                        <span class="txt"><?php esc_html_e('All','triathlon');?></span>
                                        <span class="round">
                                            <i class="icon-arrow-right-latest-races"></i>
                                        </span>
                                    </a>
                                </li>
        
                                <?php foreach ($terms as $term) { ?>
                                    <li>
                                        <a href="javascript:;" class="btn-theme black" data-filter=".<?php echo esc_attr($term->slug); ?>">
                                            <span class="txt"><?php echo esc_attr($term->slug); ?></span>
                                            <span class="round">
                                                <i class="icon-arrow-right-latest-races"></i>
                                            </span>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
						<?php
                    }
                }
            }
            ?>
            <!-- end filter -->
            <div class="portfolio-content portfolio-content<?php echo esc_attr($uniq_flag); ?> isotope">
                <div class="row">
                    <?php
                    while ($query->have_posts()) : $query->the_post();
                        global $post;
						$thumnail = triathlon_prepare_thumbnail($post->ID, $width, $height);
                        if (isset($thumnail) && !empty($thumnail)) {
                            $post_categories = wp_get_post_categories($post->ID);
                            $categories_array = array();
                            $categories = get_the_terms($post->ID, 'gallery_categories');
                            
                           
                            if (is_array($categories)) {
                                foreach ($categories as $category) {
                                    $categories_array[$category->slug] = $category->slug;
                                }
                            }
                           
                            ?>	
                            <article class="gallery-item gallery-item<?php echo esc_attr($uniq_flag); ?> col-sm-4 <?php echo implode(' ', $categories_array); ?>">
                                <a href="<?php echo triathlon_prepare_thumbnail($post->ID, 0, 0); ?>" data-rel="prettyPhoto[gallery1]">
                                  <img alt="<?php the_title(); ?>" src="<?php echo triathlon_prepare_thumbnail($post->ID, 370, 175); ?>">
                                </a>
                                <?php /*?><a href="javascript:;" class="btn-theme btn-date red">
                                    <span class="txt"><?php echo date_i18n(get_option('date_format'), strtotime(get_the_date())); ?></span>
                                    <span class="round">
                                        <i class="icon-arrow-right-latest-races"></i>
                                    </span>
                                </a><?php */?>
                            </article>
                        <?php } ?>

                    <?php endwhile;
                    wp_reset_postdata();
                    ?>
                </div>
            </div>
        </div>
        <?php if (isset($atts['show_pagination']) && $atts['show_pagination'] == 'show') : ?>
            <?php triathlon_prepare_pagination($count_post, $show_posts); ?>
<?php endif; ?>
    </div>
</div>

<script>
    jQuery(document).ready(function (){
        /* -------------------------------------
     Portfolio Filter			
     -------------------------------------- */
    var $container = jQuery('.portfolio-content<?php echo esc_js($uniq_flag); ?>');
    // set selected menu items
    var $optionSets = jQuery('.option-set<?php echo esc_js($uniq_flag); ?>');
    var $optionLinks = $optionSets.find('a');
    function doIsotopeFilter() {
        if (jQuery().isotope) {
            var isotopeFilter = '';
            $optionLinks.each(function () {
                var selector = jQuery(this).attr('data-filter');
                var link = window.location.href;
                var firstIndex = link.indexOf('filter=');
                if (firstIndex > 0) {
                    var id = link.substring(firstIndex + 7, link.length);
                    if ('.' + id == selector) {
                        isotopeFilter = '.' + id;
                    }
                }
            });
            if (isotopeFilter.length > 0) {
                // initialize Isotope
                $container.isotope({
                    itemSelector: '.gallery-item<?php echo esc_js($uniq_flag); ?>',
                    filter: isotopeFilter
                });
                $optionLinks.each(function () {
                    var $this = jQuery(this);
                    var selector = $this.attr('data-filter');
                    if (selector == isotopeFilter) {
                        if (!$this.hasClass('selected')) {
                            var $optionSet = $this.parents('.option-set<?php echo esc_js($uniq_flag); ?>');
                            $optionSet.find('.selected').removeClass('selected');
                            $this.addClass('selected');
                        }
                    }
                });
            }
            // filter items when filter link is clicked
            $optionLinks.on('click', function () {
                var $this = jQuery(this);
                var selector = $this.attr('data-filter');
                $container.isotope({itemSelector: '.gallery-item<?php echo esc_js($uniq_flag); ?>', filter: selector});
                if (!$this.hasClass('selected')) {
                    var $optionSet = $this.parents('.option-set<?php echo esc_js($uniq_flag); ?>');
                    $optionSet.find('.selected').removeClass('selected');
                    $this.addClass('selected');
                }
                return false;
            });
        }
    }
    var isotopeTimer = window.setTimeout(function () {
        window.clearTimeout(isotopeTimer);
        doIsotopeFilter();
    }, 1000);
    var selected = jQuery('#gallery-cats > li > a');
    var $this = jQuery(this);
    selected.on('click', function () {
        if (selected.hasClass('selected')) {
            jQuery(this).parent().addClass('select').siblings().removeClass('select');
        }
    });
    });

</script>