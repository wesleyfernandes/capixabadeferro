<?php if (!defined('FW')) die( 'Forbidden' );
/**
 * @var $atts
 */
?>

<div class="text-box">
	<?php if(isset($atts['textblock_title']) && $atts['textblock_title'] != '') { ?>
		<h2><?php echo esc_attr($atts['textblock_title']); ?></h2>
	<?php } ?>
	<?php if(isset($atts['textblock_sub_title']) && $atts['textblock_sub_title'] != '') { ?>
		<span class="subtitle"><?php echo esc_attr($atts['textblock_sub_title']); ?></span>
	<?php } ?>
	<?php if(isset($atts['textblock_description']) && $atts['textblock_description'] != '') { ?>
	<div class="description">
		<?php echo do_shortcode($atts['textblock_description']); ?>
	</div>
	<?php } ?>
	<?php if(isset($atts['textblock_button_text']) && $atts['textblock_button_text'] != '') { ?>
	<a href="<?php echo esc_attr($atts['textblock_button_link']); ?>" class="btn-theme black">
		<span class="txt"><?php echo esc_attr($atts['textblock_button_text']); ?></span>
		<span class="round">
			<i class="icon-arrow-right-latest-races"></i>
		</span>
	</a>
	<?php } ?>
</div>

