<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package Triathlon
 */

get_header(); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-text">
                <div class="page-content">
                    <h1><?php esc_html_e('404','triathlon');?></h1>
					<?php
						if ( ! function_exists('fw_get_db_settings_option') ) {
							$message = esc_html__('<strong>Sorry,</strong> but nothing matched your search terms. Please try again with some different keywords.','triathlon');
						} else {
							$message = fw_get_db_settings_option('404_message');
						}
                    ?>
                    <?php if(isset($message) && $message != '') : ?><p><?php echo force_balance_tags( $message ); ?></p><?php endif; ?>
                    <?php get_search_form(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
