<?php
/**
 * The template for displaying comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package Triathlon
 */
/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if (post_password_required()) {
    return;
}

/* ----------------------------------------------------------------------- */
/* 	Display Comments
  /*----------------------------------------------------------------------- */
?>
<?php
if (!function_exists('triathlon_comments')) {

    function triathlon_comments($comment, $args, $depth) {
        $path = get_template_directory_uri();
        $GLOBALS['comment'] = $comment;
        ?>
        <li <?php comment_class('comment-entry clearfix'); ?> id="comment-<?php comment_ID(); ?>">
            <div class="author-img">
                <a href="javascript:;">
                    <?php echo get_avatar($comment, 77); ?>
                </a>
            </div>
            <div class="comment-box">
                <div class="comment-meta">
                    <div class="author-details">
                        <strong class="author-name"><?php echo ucfirst(get_comment_author()); ?></strong>
                        <span class="author-post"><?php echo esc_attr(get_the_author_meta('designation')); ?></span>
                    </div>
                    <div class="reply-date">
                        <span class="date">
                            <i class="fa fa-calendar-o"></i>
                            <em><?php printf( _x( '%s ago', '%s = human-readable time difference', 'triathlon' ), human_time_diff( get_comment_time( 'U' ), current_time( 'timestamp' ) ) ); ?></em>
                        </span>
                        <span class="btn-reply">
                            <a class="fa fa-mail-reply" href="#"></a>
                            <em><?php edit_comment_link(esc_html__('(Edit)', 'triathlon'), ' '); ?>  /  <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))); ?></em>
                        </span>
                    </div>
                </div>
                <div class="author-description">
                    <?php if ($comment->comment_approved == '0') : ?>
                        <p class="comment-moderation"><?php esc_html_e('Your comment is awaiting moderation.', 'triathlon'); ?></p>
                    <?php endif; ?>
                    <?php comment_text(); ?>
                    
                </div>
            </div>
            
            <?php
        }

    }

    //comments reply and edit

    function format_comment($comment, $args, $depth) {
        $GLOBALS['comment'] = $comment;
        ?>
    <li>
        <div class="comm-block">
            <div class="com-img">
                <?php echo get_avatar($comment, 77); ?>
            </div>
            <div class="com-txt">
                <span><?php esc_html_e('Sammy Portman&ensp;.&ensp;2 days ago', 'triathlon'); ?></span>
                <?php comment_text(); ?>
                <?php edit_comment_link(esc_html__('(Edit)', 'triathlon'), ' '); ?>  /  <?php comment_reply_link(array_merge($args, array('reply_text' => 'Reply', 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
            </div>
        </div>



        <?php if ($comment->comment_approved == '0') : ?>
            <em><?php esc_html_e('Your comment is awaiting moderation.', 'triathlon') ?></em><br />
        <?php endif; ?>



    <?php }
    ?>
    <!-- BEGIN #respond-wrapper -->
    <div id="comment-form">
        <fieldset>
            <?php
            if (comments_open()) :

                global $aria_req;

                comment_form(array(
                    'fields' => apply_filters(
                            'comment_form_default_fields', array(
                        'author' => '
                        <div class="form-group">' . '<input class="form-control" id="author" placeholder="Your Name" name="author" type="text" value="' .

                        esc_attr($commenter['comment_author']) . '" size="30"' . $aria_req . ' />' .
                        '</div>'
                        ,
                        'email' => '<div class="form-group">' . '<input class="form-control" id="email" placeholder="Your Email Address (Will Not Be Published)" name="email" type="text" value="' . esc_attr($commenter['comment_author_email']) .
                        '" size="30"' . $aria_req . ' />' .
                        '</div>',
                        'website' => '<div class="form-group">' . '<input class="form-control" id="text" placeholder="Your Website URL" name="website" type="text" value="" size="30"' . $aria_req . ' />' .
                        '</div>'
                            )
                    ),
                    'comment_field' => '<div class="form-group">' .
                    '<textarea class="form-control" name="comment" placeholder="Your Message..."  aria-required="true"></textarea>' .
                    '</div>',
                    'comment_notes_after' => '',
                    'notes' => '',
                    'comment_notes_before' => '',
                    'comment_notes_after' => '',
                    'class_form' => '',
                    'id_form' => '',
                    'class_submit' => '',
                    'id_submit' => '',
                    'title_reply' => esc_html__('Post Comment', 'triathlon'),
                    'title_reply_to' => '<h3>' . esc_html__('Leave us a comment', 'triathlon') . '</h3>',
                    'cancel_reply_link' => esc_html__('Cancel reply', 'triathlon'),
                    'label_submit' => esc_html__('Submit Now', 'triathlon'),
                ));
                ?>
            <?php endif; ?>
        </fieldset>
    </div>

    <div id="comment">
        <?php if (have_comments()) : ?>

            <h3 id="comment-number" class="block-title"><?php comments_number(esc_html__('No discusion on this article', 'triathlon'), esc_html__('1 discusion on this article', 'triathlon'), esc_html__('% discusions on this article', 'triathlon')); ?></h3>
            <ul>
                <?php wp_list_comments(array('callback' => 'triathlon_comments')); ?>
            </ul>

            <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>
                <?php previous_comments_link(esc_html__('&larr; Older Comments', 'triathlon')); ?>
                <?php next_comments_link(esc_html__('Newer Comments &rarr;', 'triathlon')); ?>
            <?php endif; ?>
            <?php
        endif;

        /* ----------------------------------------------------------------------- */
        /* 	Comment Form
          /*----------------------------------------------------------------------- */
        ?>
    </div> <!--//Comment End-->