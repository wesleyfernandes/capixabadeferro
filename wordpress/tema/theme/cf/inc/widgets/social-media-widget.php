<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

if ( ! class_exists( 'Triathlon_SocialMedia' ) ) { 
	class Triathlon_SocialMedia extends WP_Widget {	
	
	/**
	 * @init Social Media
	 *
	 *
	 */
	 public function __construct() {
		$widget_ops = array('classname' => 'social_media', 'description' => 'To Displays Social Media Links.' );
        $control_ops = array( 'width' => 300, 'height' => 250, 'id_base' => 'social_media' );
        parent::__construct( 'social_media', esc_html__('TG Social Media Widget', 'triathlon'), $widget_ops, $control_ops );
	 }
	 
	 /**
	 * @Social Media form
	 *
	 *
	 */
	 public function form($instance) {
		if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = esc_html__('FOLLOW US', 'triathlon');
        } ?>

		<p>
			<label for="title">
				<?php esc_html_e('<strong>Title</strong>', 'triathlon'); ?>
			</label>
			<input id="title" class="widefat" name="<?php echo esc_attr( $this->get_field_name('title') ); ?>"
						   type="text" value="<?php echo esc_attr($title); ?>"/>
		</p>
		<?php
 	}
		
		/**
		 * @Update Social Media
		 *
		 *
		 */
		public function update($new_instance, $old_instance) {
			$instance = $old_instance;
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
			return $instance;
		}
		
		/**
		 * @Display Social Media
		 *
		 *
		 */
		public function widget($args, $instance) {
			extract($args);
			$title = $instance['title'];
			if(function_exists('fw_get_db_settings_option')) :
				$social_icons = fw_get_db_settings_option('social_icons');
				$social_icons_target = fw_get_db_settings_option('social_icon_target');
				echo triathlon_esc_specialchars( $args['before_widget'] );
				if ( ! empty( $title ) ) {
					echo triathlon_esc_specialchars( $args['before_title'] . apply_filters( 'widget_title', esc_attr($title) ). $args['after_title'] );
				}
				echo '<ul>';
				foreach($social_icons as $social_links) : ?>
					<li><a <?php echo (isset($social_icons_target) && $social_icons_target == true) ? "target='_blank'" : "target='_self'"; ?> href="<?php echo esc_url($social_links['social_url']); ?>"><?php echo esc_attr($social_links['social_name']); ?></a></li>
	<?php
				endforeach;
				echo '</ul>';
				echo triathlon_esc_specialchars( $args['after_widget'] );
			endif;
		}
	}
}
add_action('widgets_init', create_function('', 'return register_widget("Triathlon_SocialMedia");'));
?>