<?php
/**
 * @Headers
 *
 */

if (!function_exists('triathlong_do_init_headers')) {
	add_action('triathlong_do_init_headers','triathlong_do_init_headers');
	function triathlong_do_init_headers() {
        global $post, $woocommerce;
		$post_name  = triathlon_get_post_name();
		?>
		<?php get_template_part('template-parts/template','comingsoon'); ?>
		<div id="wrapper">
		<!-- Header Start -->
		  <header id="header" class="haslayout">
			<?php triathlon_prepare_topstrip();?>
			<?php triathlon_prepare_header();?>
		  </header>
		  <?php triathlon_prepare_banner();?>
		  <!-- Header End -->  
		  <?php triathlon_prepare_subheaders(); ?>
		<?php
	}
}

/**
 * @Prepare Top Strip
 * @return {}
 */
if (!function_exists('triathlon_prepare_comingsoon')) {

    function triathlon_prepare_comingsoon() {
        global $post, $woocommerce;
		$post_name  = triathlon_get_post_name();
		if (function_exists('fw_get_db_post_option')) {
			$maintenance = fw_get_db_settings_option('maintenance');
					
		} else {
			$maintenance = '';
		}
		
		$background_comingsoon	= '';
		if (( isset($maintenance) && $maintenance == 'on' && !is_user_logged_in() ) || $post_name === "coming-soon") {
			if (function_exists('fw_get_db_post_option')) {
				$background = fw_get_db_settings_option('background');
						   
			} else {
				$background = '';
			}
	
			if( isset( $background['url'] ) && !empty( $background['url'] ) ) {
				$background_comingsoon	= 'style="background:url('.$background['url'].') no-repeat; background-size:cover; height:100%;"';
			}
		}
		
		echo ( $background_comingsoon );
	}
}
/**
 * @Prepare Top Strip
 * @return {}
 */
if (!function_exists('triathlon_prepare_topstrip')) {

    function triathlon_prepare_topstrip() {
        global $post, $woocommerce;


        if (function_exists('fw_get_db_settings_option')) {
            $enable_top_strip = fw_get_db_settings_option('enable_top_strip');
            $enable_top_menu = fw_get_db_settings_option('enable_top_menu');
            $enable_language = fw_get_db_settings_option('enable_language');
            $enable_social_icons = fw_get_db_settings_option('enable_social_icons');
            $social_icon = fw_get_db_settings_option('social_icons');

            if (isset($enable_top_strip) && $enable_top_strip === 'on') {
                ?>
                <div class="topbar haslayout">
                    <div class="container">
                        <div class="row">
                            <div class="left-bar pull-left">
                                <?php if (class_exists('woocommerce')) { ?>
                                    <div class="dropdown">
                                        <button class="btn-dropdown btn-cart" type="button" id="cart" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <i class="fa fa-shopping-cart"></i>
                                            <span class="cart-contents">Cart: <?php echo intval($woocommerce->cart->cart_contents_count); ?> item(s)</span>
                                            <em class="icon-arrow-down"></em>
                                        </button>
                                        <div class="dropdown-menu cart-item" aria-labelledby="cart">
                                            <strong class="title"><?php esc_html_e('Recently added item(s)', 'triathlon'); ?></strong>
                                            <div class="widget_shopping_cart_content"></div>
                                        </div>
                                    </div>
                                <?php } ?>

                                <?php
                                $show_translation = 'true';
                                if (class_exists('woocommerce') && is_shop()) {
                                    $show_translation = 'false';
                                } else {
                                    $show_translation = 'true';
                                }
                                ?>

                                <?php if (function_exists('fw_ext_translation_get_frontend_active_language') && $show_translation == 'true' && $enable_language == 'on') { ?>
                                    <div class="dropdown">
                                        <button class="btn-dropdown btn-languages" type="button" id="languages" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <span><?php esc_html_e('EN', 'triathlon'); ?></span>
                                            <em class="icon-arrow-down"></em>
                                        </button>
                                        <div class="dropdown-menu languages" aria-labelledby="languages">
                                            <?php echo fw_ext('translation')->frontend_language_switcher(); ?>
                                        </div>
                                    </div>
                                <?php } ?>

                            </div>
                            <?php if (isset($enable_top_menu) && $enable_top_menu === 'on') { ?>
                                <div class="right-bar pull-right">
                                    <nav class="add-nav">
                                        <?php echo triathlon_prepare_navigation('top-nav', '', '', '0'); ?>
                                    </nav>
                                </div>
                            <?php } ?>
                            <?php if (isset($enable_social_icons) && isset($social_icon) && is_array($social_icon) && $enable_social_icons == 'on') { ?>
                                <div class="right-bar pull-right">
                                    <ul class="social-icon">
                                        <?php
                                        foreach ($social_icon as $key => $value) {
                                            if ($value ['social_icons_list'] != '') {
                                                ?>
                                                <li><a title="<?php echo esc_attr($value ['social_name']); ?>" href="<?php echo esc_url($value ['social_url']); ?>"><i class="fa <?php echo esc_attr($value ['social_icons_list']); ?>"></i></a></li>
                                            <?php } ?>
                                        <?php } ?>
                                    </ul>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
    }

}


/**
 * @Prepare Top Strip
 * @return {}
 */
if (!function_exists('triathlon_prepare_header')) {

    function triathlon_prepare_header() {
        global $post;
        $primary_logo = '';
        $search_form = '';

        if (function_exists('fw_get_db_settings_option')) {
            $primary_logo = fw_get_db_settings_option('main_logo');
            $enable_search = fw_get_db_settings_option('enable_search');
        }

        if (isset($primary_logo['url']) && !empty($primary_logo['url'])) {
            $logo = $primary_logo['url'];
        } else {
            $logo = get_template_directory_uri() . '/img/logo.png';
        }

        $tg_output = '';
        $tg_output .= '<div class="navigation-area haslayout">';
        $tg_output .= '<div class="container">';
        $tg_output .= '<div class="row">';
        $tg_output .= '<strong class="logo"><a href="' . esc_url(home_url('/')) . '"><img src="' . $logo . '"></a></strong>';
        $tg_output .= '<nav id="nav" class="haslayout">';

        if (isset($enable_search) && $enable_search == 'on') {
            $tg_output .= get_search_form(false);
        }

        $tg_output .= '<div class="navbar-header">';
        $tg_output .= '<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">';
        $tg_output .= '<span class="sr-only">Toggle navigation</span>';
        $tg_output .= '<span class="icon-bar"></span>';
        $tg_output .= '<span class="icon-bar"></span>';
        $tg_output .= '<span class="icon-bar"></span>';
        $tg_output .= '</button>';
        $tg_output .= '</div>';
        $tg_output .= '<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">';
        $tg_output .= triathlon_prepare_navigation('main-menu', '', '', '0');
        $tg_output .= '</div>';
        $tg_output .= '</nav>';
        $tg_output .= '</div>';
        $tg_output .= '</div>';
        $tg_output .= '</div>';

        echo force_balance_tags($tg_output);
    }

}

/**
 * @Main Navigation
 * @return {}
 */
if (!function_exists('triathlon_prepare_navigation')) {

    function triathlon_prepare_navigation($location = '', $id = 'menus', $class = '', $depth = '0') {
        if (has_nav_menu($location)) {
            $defaults = array(
                'theme_location' => "$location",
                'menu' => '',
                'container' => '',
                'container_class' => '',
                'container_id' => '',
                'menu_class' => "$class",
                'menu_id' => "$id",
                'echo' => false,
                'fallback_cb' => 'wp_page_menu',
                'before' => '',
                'after' => '',
                'link_before' => '',
                'link_after' => '',
                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                'depth' => "$depth",
                'walker' => '',);
            return do_shortcode(wp_nav_menu($defaults));
        } else {
            $defaults = array(
                'theme_location' => "",
                'menu' => '',
                'container' => '',
                'container_class' => '',
                'container_id' => '',
                'menu_class' => "$class",
                'menu_id' => "$id",
                'echo' => false,
                'fallback_cb' => 'wp_page_menu',
                'before' => '',
                'after' => '',
                'link_before' => '',
                'link_after' => '',
                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                'depth' => "$depth",
                'walker' => '',);
            return do_shortcode(wp_nav_menu($defaults));
        }
    }

}

/**
 * @Prepare banner
 * @return {}
 */
if (!function_exists('triathlon_prepare_banner')) {

    function triathlon_prepare_banner() {
        global $post, $woocommerce;

        if (!is_page()) {
            return;
        }

        if (!function_exists('fw_get_db_post_option')) {
            $page_setting_type = '';
            $slides = '';
            $enable_slider_nav = '';
            $data_autoplay = '';
            $slider_speed = '';
            $padding_top = '';
            $padding_bottom = '';
            $slider_bg_color = '';
            $slider_bg_image = '';
        } else {
            $page_setting_type = fw_get_db_post_option($post->ID, 'page_settings_type', true);
            $slides = fw_get_db_post_option($post->ID, 'slides', true);
            
            
            $custom_slider_shortcode = fw_get_db_post_option(get_the_ID(), 'custom_slider_shortcode', true);
            $enable_slider_nav = fw_get_db_post_option($post->ID, 'enable_slider_nav', true);
            $data_autoplay = fw_get_db_post_option($post->ID, 'data_autoplay', true);
            $slider_speed = fw_get_db_post_option($post->ID, 'slider_speed', true);
            $padding_top = fw_get_db_post_option($post->ID, 'padding_top', true);
            $padding_bottom = fw_get_db_post_option($post->ID, 'padding_bottom', true);
            $slider_bg_color = fw_get_db_post_option($post->ID, 'slider_bg_color', true);
            $slider_bg_image = fw_get_db_post_option($post->ID, 'slider_bg_image', true);
        }
        
		$uniq_flag = rand(1,9999);
        $banner = triathlon_prepare_thumbnail($post->ID, 1140, 289);
        
        
        if ($enable_slider_nav == 'enable') {
            $enable_slider_nav = 'true';
        } else {
            $enable_slider_nav = 'false';
        }

        if ($data_autoplay == 'enable') {
            $data_autoplay = 'true';
        } else {
            $data_autoplay = 'false';
        }

        if (!empty($slider_speed) && intval($slider_speed)) {
            $slider_speed = $slider_speed;
        } else {
            $slider_speed = '300';
        }
        $custom_styling = '';

        if (isset($padding_top) && !empty($padding_top)) {
            $custom_styling .= 'padding-top:' . $padding_top . 'px;';
        }

        if (isset($padding_bottom) && !empty($padding_bottom)) {
            $custom_styling .= 'padding-bottom:' . $padding_bottom . 'px;';
        }



        if (isset($slider_bg_color) && !empty($slider_bg_color)) {
            $custom_styling .= 'background:' . $slider_bg_color . ';';
        }
        $banner_type_class = '';

        if (isset($slider_bg_image) && !empty($slider_bg_image['url'])) {
            $custom_styling .= 'background-image:url(' . esc_url($slider_bg_image['url']) . ');';
            $banner_type_class = 'tg-gym';
        }

        if (isset($custom_styling) && !empty($custom_styling)) {
            $styling_settings = 'style="' . $custom_styling . '"';
        }

            if (isset($page_setting_type) && $page_setting_type === 'image' && isset($banner) && !is_single()) {
            ?>
                <div><img style="width:100%;" src="<?php echo esc_url($banner); ?>" alt="<?php echo get_option('blogname'); ?>"> </div>	
                <?php
            } else if ($page_setting_type === 'slider' && is_array($slides) && !is_single()) {
                if (isset($slides) && !empty($slides)) {
                    ?>
                    <!-- Slider Start -->
                    <section class="banner-slider haslayout <?php echo triathlon_esc_specialchars($banner_type_class); ?>" <?php echo triathlon_esc_specialchars($styling_settings); ?>>
                        <div class="container">
                            <div class="row">
                                <div class="main-slider-<?php echo esc_attr($uniq_flag); ?> row">
                                    <?php foreach ($slides as $slider_data) : ?>
                                        <div class="item">
                                            <div class="col-sm-6">
                                                <div class="heading-area">
                                                    <?php if (isset($slider_data['slider_title']) && !empty($slider_data['slider_title'])) { ?>
                                                        <h1><?php echo esc_attr($slider_data['slider_title']); ?></h1>
                                                    <?php } ?>
                                                    <?php if (isset($slider_data['slider_date']) && !empty($slider_data['slider_date'])) { ?>
                                                        <span class="date"><?php echo esc_attr($slider_data['slider_date']); ?></span>
                                                    <?php } ?>
                                                </div>
                                                <?php if (isset($slider_data['slider_description']) && !empty($slider_data['slider_description'])) { ?>
                                                    <p><?php echo esc_attr($slider_data['slider_description']); ?></p>
                                                <?php } ?>

                                                <?php if (isset($slider_data['slider_button_tite']) && !empty($slider_data['slider_button_tite'])) { ?>
                                                    <a href="<?php echo esc_url($slider_data['slider_button_link']); ?>" class="btn-theme red"> <span class="txt"><?php echo esc_attr($slider_data['slider_button_tite']); ?></span> <span class="round"> <i class="icon-arrow-right-latest-races"></i> </span> </a>
                                                <?php } ?>
                                            </div>
                                            <?php if (isset($slider_data['slider_image']['url']) && !empty($slider_data['slider_image']['url'])) { ?>
                                                <div class="col-sm-6"> <img src="<?php echo esc_url($slider_data['slider_image']['url']); ?>" alt="<?php echo esc_attr($slider_data['slider_title']);?>"> </div>
                                                <?php } ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <script>
                            jQuery(document).ready(function (e) {
                                jQuery(".main-slider-<?php echo esc_attr($uniq_flag); ?>").owlCarousel({
                                    autoPlay: <?php echo esc_attr($data_autoplay); ?>,
                                    navigation: false,
                                    pagination: <?php echo esc_attr($enable_slider_nav); ?>,
                                    slideSpeed: <?php echo esc_attr($slider_speed); ?>,
                                    paginationSpeed: 400,
                                    singleItem: true,
                                });
                            });

                        </script>
                    </section>
                    <!-- Slider End -->
                    <?php
                }
            } else if ( $page_setting_type === 'custom_slider' && !empty( $custom_slider_shortcode )) {
                if (isset($custom_slider_shortcode) && $custom_slider_shortcode != '') {
                    echo '<section class="haslayout">';
						echo do_shortcode($custom_slider_shortcode);
					echo '</div>';
                }
            }
        }

    }