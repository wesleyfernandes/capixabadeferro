<?php
/**
 * Jetpack Compatibility File
 * See: https://jetpack.me/
 *
 * @package Triathlon
 */

/**
 * Add theme support for Infinite Scroll.
 * See: https://jetpack.me/support/infinite-scroll/
 */
function triathlon_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'render'    => 'triathlon_infinite_scroll_render',
		'footer'    => 'page',
	) );
} // end function triathlon_jetpack_setup
add_action( 'after_setup_theme', 'triathlon_jetpack_setup' );

function triathlon_infinite_scroll_render() {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'template-parts/content', get_post_format() );
	}
} // end function triathlon_infinite_scroll_render