<?php
/**
 * @Woocommerce Customization
 * return {}
 */
if (!class_exists('triathlon_woocommerce')) {

    class triathlon_woocommerce {

        function __construct() {

            add_filter('woocommerce_enqueue_styles', '__return_false');
            remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
            add_filter('woocommerce_show_page_title', array(&$this, 'triathlon_prepare_shop_title'));
            remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
            add_action('woocommerce_after_shop_loop_item', array(&$this, 'triathlon_woocommerce_add_to_cart'), 10);
            add_filter('add_to_cart_fragments', array(&$this, 'triathlon_woocommerce_header_add_to_cart')); // Ajax Add To cart
            add_action('wp_ajax_add_to_cart_variable_rc', array(&$this, 'triathlon_add_to_cart_variable_rc'));


            //Change sorting names
            add_filter('woocommerce_catalog_orderby', array(&$this, 'triathlon_woocommerce_change_sorting_names'));
            add_filter('woocommerce_default_catalog_orderby_options', array(&$this, 'triathlon_woocommerce_change_sorting_names'));

            //per page
            remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering');
            add_filter('loop_shop_per_page', array(&$this, 'triathlon_woocommerce_sort_by_page'));
            add_action('woocommerce_before_shop_loop', array(&$this, 'triathlon_woocommerce_catalog_page_ordering', 20));
            add_action('triathlon_render_quick_view', array(&$this, 'triathlon_render_quick_view'));

            // Remove Rating From Listing
            remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);

            add_filter('loop_shop_per_page', array(&$this, 'triathlon_shop_per_page'), 20);
            $this->triathlon_get_quick_view();

            //add_action("after_switch_theme", array(&$this,"triathlon_woocommerce_image_settings",10)); //Thumbnails
        }

        /**
         * Woocommerce Quick View
         *
         */
        public function triathlon_shop_per_page($cols) {
            // $cols contains the current number of products per page based on the value stored on Options -> Reading
            // Return the number of products you wanna show per page.
            $per_page = '9';
            if (function_exists('fw_get_db_settings_option')) {
                $show_shop_posts = fw_get_db_settings_option('show_shop_posts');
                if (isset($show_shop_posts) && !empty($show_shop_posts)) {
                    $per_page = intval($show_shop_posts);
                } else {
                    $per_page = '9';
                }
            }
            return $per_page;
        }

        /**
         * Woocommerce Quick View
         *
         */
        public function triathlon_loop_after_title() {
            //add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
        }

        /**
         * Woocommerce Quick View
         *
         */
        public function triathlon_get_quick_view() {

            // Image
            add_action('triathlon_quick_product_image', 'woocommerce_show_product_sale_flash', 10);
            add_action('triathlon_quick_product_image', 'woocommerce_show_product_images', 20);

            // Summary
            add_action('triathlon_quick_product_summary', 'woocommerce_template_single_title', 5);
            add_action('triathlon_quick_product_summary', 'woocommerce_template_single_rating', 10);
            add_action('triathlon_quick_product_summary', 'woocommerce_template_single_price', 15);
            add_action('triathlon_quick_product_summary', 'woocommerce_template_single_excerpt', 20);
            add_action('triathlon_quick_product_summary', 'woocommerce_template_single_add_to_cart', 25);
            add_action('triathlon_quick_product_summary', 'woocommerce_template_single_meta', 30);
        }

        /** 	
         * @Shop Title
         * @return {}
         */
        public function triathlon_prepare_shop_title() {
            $shop_title = '';
            return $shop_title;
        }

        /**
         * @Before Title
         * @return {}
         */
        function triathlon_woocommerce_add_to_cart() {
            global $product;
            echo apply_filters('woocommerce_loop_add_to_cart_link', sprintf('<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" data-quantity="%s" class="btn-theme btn-addtocart %s product_type_%s ajax_add_to_cart add_to_cart_button"><i class="fa fa-refresh fa-spin"></i> %s </a>', esc_url($product->add_to_cart_url()), esc_attr($product->id), esc_attr($product->get_sku()), esc_attr(isset($quantity) ? $quantity : 1 ), $product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '', esc_attr($product->product_type), '<span class="txt">' . esc_html($product->add_to_cart_text()) . '</span>
						<span class="round">
							<i class="icon-arrow-right-latest-races"></i>
						</span>
					</a> '
                    ), $product);
        }

        /**
         * @Add to cart via ajax
         * @return {}
         */
        public function triathlon_woocommerce_header_add_to_cart($fragments) {
            global $woocommerce;

            ob_start();
            ?>
            <span class="cart-contents"><?php esc_html_e('Cart: ', 'triathlon'); ?><?php echo intval($woocommerce->cart->cart_contents_count); ?> item(s)</span>
            <?php
            $fragments['span.cart-contents'] = ob_get_clean();

            return $fragments;
        }

        /**
         * @add to cart
         * @return {}
         */
        public function triathlon_add_to_cart_variable_rc() {
            global $woocommerce;
            $product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
            $quantity = empty($_POST['quantity']) ? 1 : apply_filters('woocommerce_stock_amount', $_POST['quantity']);
            $variation_id = 0;
            $variation = array();
            $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);

            if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id, $variation)) {
                do_action('woocommerce_ajax_added_to_cart', $product_id);
                if (get_option('woocommerce_cart_redirect_after_add') == 'yes') {
                    wc_add_to_cart_message($product_id);
                }
                // Return fragments
                // WC_AJAX::get_refreshed_fragments();
                $this->triathlon_get_fragments();
            } else {
                //$this->json_headers();
                // If there was an error adding to the cart, redirect to the product page to show any errors
                $data = array(
                    'error' => 'true',
                    'message' => esc_html__('Some error occur,please try again later.', 'triathlon'),
                    'product_url' => apply_filters('woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id)
                );
                echo json_encode($data);
            }
            die();
        }

        /**
         * @refresh fregments for multiple products rendering
         * @return {}
         */
        public function triathlon_get_fragments() {
            global $woocommerce;
            ob_start();
            woocommerce_mini_cart();
            $mini_cart = ob_get_clean();

            // Fragments and mini cart are returned
            $data = array(
                'fragments' => apply_filters('woocommerce_add_to_cart_fragments', array(
                    'div.widget_shopping_cart_content' => '<div class="widget_shopping_cart_content">' . $mini_cart . '</div>'
                        )
                ),
                'cart_hash' => apply_filters('woocommerce_add_to_cart_hash', WC()->cart->get_cart_for_session() ? md5(json_encode(WC()->cart->get_cart_for_session())) : '', WC()->cart->get_cart_for_session())
            );

            $data['message'] .= esc_html__('Cart updated', 'triathlon');
            if (sizeof($woocommerce->cart->cart_contents) > 0) :
                $data['cart'] .= '<a class="added_to_cart wc-forward" href="' . $woocommerce->cart->get_checkout_url() . '" title="' . esc_html__('Checkout', 'triathlon') . '">' . esc_html__('Checkout', 'triathlon') . '</a>';
            endif;

            wp_send_json($data);
        }

        /**
         * @Rename Sortable 
         * @return {}
         */
        public function triathlon_woocommerce_change_sorting_names($catalog_orderby) {
            $catalog_orderby = str_replace("Default sorting", "Default", $catalog_orderby);
            $catalog_orderby = str_replace("Sort by popularity", "Popular", $catalog_orderby);
            $catalog_orderby = str_replace("Sort by average rating", "Average Rating", $catalog_orderby);
            $catalog_orderby = str_replace("Sort by newness", "Newness", $catalog_orderby);
            $catalog_orderby = str_replace("Sort by price: low to high", "Lowest Price", $catalog_orderby);
            $catalog_orderby = str_replace("Sort by price: high to low", "Highest Price", $catalog_orderby);
            return $catalog_orderby;
        }

        /**
         * @Per page Ordering 
         * @return {}
         */
        public static function triathlon_woocommerce_catalog_page_ordering() {
            ?>
            <form action="" method="POST" name="results" class="woocommerce-ordering">
                <select name="woocommerce-sort-by-columns" id="woocommerce-sort-by-columns" class="sortby" onchange="this.form.submit()">
                    <?php
                    //Get products on page reload
                    if(isset($_COOKIE['shop_pageResults'])){
                        $shop_pageresult = $_COOKIE['shop_pageResults'];
                    }else{
                        $shop_pageresult = 10;
                    }
                    if (isset($_POST['woocommerce-sort-by-columns']) && ($shop_pageresult <> $_POST['woocommerce-sort-by-columns'])) {
                        $numberOfProductsPerPage = $_POST['woocommerce-sort-by-columns'];
                    } else {
                        $numberOfProductsPerPage = $shop_pageresult;
                    }
                    $shopCatalog_orderby = apply_filters('woocommerce_sortby_page', array(
                        '-1' => esc_html__('All', 'triathlon'),
                        '10' => esc_html__('10', 'triathlon'),
                        '20' => esc_html__('20', 'triathlon'),
                        '50' => esc_html__('50', 'triathlon'),
                        '100' => esc_html__('100', 'triathlon'),
                    ));

                    foreach ($shopCatalog_orderby as $sort_id => $sort_name) {
                        echo '<option value="' . $sort_id . '" ' . selected($numberOfProductsPerPage, $sort_id, true) . ' >' . $sort_name . '</option>';
                    }
                    ?>
                </select>
            </form>
            <?php
        }

        /**
         * @Sort by Page
         * @return {}
         */
        public function triathlon_woocommerce_sort_by_page($count) {
            if (isset($_COOKIE['shop_pageResults'])) { // if normal page load with cookie
                $count = $_COOKIE['shop_pageResults'];
            }
            if (isset($_POST['woocommerce-sort-by-columns'])) { //if form submitted
                setcookie('shop_pageResults', $_POST['woocommerce-sort-by-columns'], time() + 1209600, '/', 'www.your-domain-goes-here.com', false); //this will fail if any
                $count = $_POST['woocommerce-sort-by-columns'];
            }
            return $count;
        }

        /**
         * @Sort by Page
         * @return {}
         */
        public function triathlon_render_quick_view() {
            global $product, $woocommerce_loop, $post;
            ?>
            <div class="modal fade bs-example-modal-sm-<?php echo intval($post->ID); ?>" tabindex="-1" role="dialog">
                <div class="modal-dialog newsletter product">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="icon-close" aria-hidden="true"></span></button>
                        <div class="content-box">
                            <div class="product">
                                <div class="product-img">
                                    <div class="box">
                                        <?php echo get_the_post_thumbnail($post->ID, 'shop_catalog') ?>
                                    </div>

                                </div>
                                <span class="product-name"><?php echo the_title(); ?></span>
                                <?php if (isset($product->sku) && !empty($product->sku)) { ?>
                                    <span class="product-code"><?php esc_html_e('Code: ', 'triathlon'); ?><?php echo esc_attr($product->get_sku()); ?></span>
                                <?php } ?>
                                <?php if ($price_html = $product->get_price_html()) : ?> <span class="product-price"><?php esc_html_e('Our Price: ', 'triathlon'); ?><?php echo force_balance_tags($price_html); ?></span><?php endif; ?>
                                <?php
                                $_product = wc_get_product($post->ID);
                                if ($_product->is_type('simple')) {
                                    ?>
                                    <form class="product-form">
                                        <fieldset>
                                            <div class="form-group">
                                                <label><?php esc_html_e('Qty:', 'triathlon'); ?></label>
                                                <span class="quantity-sapn">
                                                    <input type="text" class="result quantity_variation" data-product_id="<?php echo intval($post->ID); ?>" value="1" id="quantity1"  name="quantity">
                                                    <em class="plus fa fa-caret-down"></em>
                                                    <em class="minus fa fa-caret-up"></em>

                                                </span>
                                            </div>
                                        </fieldset>
                                    </form>
                                    <a href="javascript:;" class="btn-theme black btn-addtocart">
                                        <span class="txt"><?php esc_html_e('add to cart', 'triathlon'); ?></span>
                                        <span class="round">
                                            <i class="icon-arrow-right-latest-races"></i>
                                        </span>
                                    </a>
                                <?php } elseif ($_product->is_type('variable')) { ?>
                                    <a class="btn-theme" href="<?php the_permalink(); ?>">
                                        <span class="txt"><?php esc_html_e('Select Options', 'triathlon'); ?></span>
                                        <span class="round">
                                            <i class="icon-arrow-right-latest-races"></i>
                                        </span>
                                    </a>
                                <?php } ?>

                                <span class="add-to-cart-message" style="display:none"></span>
                                <a class="btn-moredetail " href="<?php the_permalink(); ?>">
                                    <em><?php esc_html_e('See More Details', 'triathlon'); ?></em>
                                    <i class="icon-arrow-right-latest-races"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }

    }

    new triathlon_woocommerce();
}