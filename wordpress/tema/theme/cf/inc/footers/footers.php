<?php
/**
 * @Headers
 *
 */
if (!function_exists('triathlong_do_init_footers')) {
	add_action('triathlong_do_init_footers','triathlong_do_init_footers');
	function triathlong_do_init_footers() {
		$copyright			 = '&copy; 2015 | All Rights Reserved';
		$bootom_image	 	 = '';
		$enable_back	 	 = 'on';
		$enable_newslatter	 = '';
		$newslatter_title	 = '';
		if(function_exists('fw_get_db_settings_option')) {
			$copyright = fw_get_db_settings_option('footer_copyright', $default_value = null);
			$bootom_image = fw_get_db_settings_option('payment_image', $default_value = null);
			$enable_back  = fw_get_db_settings_option('enable_back', $default_value = null);
			$enable_newslatter = fw_get_db_settings_option('enable_newslatter', $default_value = null);
			$newslatter_title = fw_get_db_settings_option('newslatter_title', $default_value = null);
			
			$footer_bg_color 	  = fw_get_db_post_option(get_the_ID(), 'footer_bg_color', true);
			$enable_sub_footer 	  = fw_get_db_post_option(get_the_ID(), 'enable_sub_footer', true);
			$contact_title 		  = fw_get_db_post_option(get_the_ID(), 'footer_contact_title', true);
			$footer_contact_image = fw_get_db_post_option(get_the_ID(), 'footer_contact_image', true);
			// Theme Option Settings
			$form_enable  = fw_get_db_settings_option('form_enable', $default_value = null);
			$bg_image 	  = fw_get_db_settings_option('bg_image', $default_value = null);
			$form_heading = fw_get_db_settings_option('form_heading', $default_value = null);
		
			if( isset( $contact_title ) && ( empty( $contact_title ) || $contact_title == 1 ) ){
				$contact_title	= $form_heading;
			}elseif (class_exists('woocommerce')) {
				 if( is_shop() || is_cart() ){
					$contact_title	= $form_heading;
				 } else{
					$contact_title	= $contact_title;	
				 }
			} elseif( is_single() || is_home() ){
				$contact_title	= $form_heading;
			}
			
			if( isset( $footer_contact_image ) && !empty( $footer_contact_image['url'] ) ){
				$footer_image	= $footer_contact_image['url'];
				$form_div		= 'col-sm-5';
				$image_on		= 'yes';
			}else if( isset( $footer_contact_image ) && !empty( $bg_image['url'] ) && $form_enable == 'enable' ){
				$footer_image	= $bg_image['url'];
				$form_div		= 'col-sm-5';
				$image_on		= 'yes';
				
			} else{
				$form_div		= 'col-sm-12';
				$image_on		= 'no';
				$footer_image	= '';
			}
			
		}
		?>
		</main>
		<!-- #main -->
		<?php if (isset($enable_sub_footer) && $enable_sub_footer == 'enable' && !is_404() && !is_category() && !is_tag() && !is_search() && !is_single() && !is_author()) { ?>
		
		<div class="competitor-section haslayout">
			<div class="container">
				<div class="row">
					<div class="competitor">
						<div class="<?php echo esc_attr($form_div); ?> col-xs-12 pull-right">
							<?php if (isset($contact_title) && !empty($contact_title) && $contact_title != 1 ) { ?>
								<div class="border-title">
									<h2><?php echo esc_attr($contact_title); ?></h2>
								</div>
								<?php } ?>
							<div id="form-messages"></div>
							<form id="ajax-contact" method="post"  class="competitor-form">
								<div id="form-messages"></div>
								<div id="error-message"></div>
								<fieldset>
									<div class="form-group">
										<input name="name" type="text" class="form-control name" placeholder="<?php esc_attr_e('Name', 'triathlon'); ?>">
									</div>
									<div class="form-group">
										<input name="phone" type="text" class="form-control phone" placeholder="<?php esc_attr_e('Phone', 'triathlon'); ?>">
									</div>
									<div class="form-group">
										<input name="email" type="email" class="form-control email" placeholder="<?php esc_attr_e('Email', 'triathlon'); ?>">
									</div>
									<div class="form-group">
										<textarea name="message" class="message" placeholder="<?php esc_attr_e('How Can We Help?', 'triathlon'); ?>"></textarea>
									</div>
									<div class="form-group loading-icons">
										<?php
											if (function_exists('fw_get_db_settings_option')) {
												$submit_button = fw_get_db_settings_option('submit_button');
											}
											?>
										<button class="btn-theme red btn-submit submit" type="submit"> <span class="txt"><?php echo (isset($submit_button) && !empty($submit_button)) ? esc_attr($submit_button) : esc_html_e('Contact Us', 'triathlon'); ?></span> <span class="round"> <i class="icon-arrow-right-latest-races"></i> </span> </button>
									</div>
								</fieldset>
							</form>
							<script type="text/javascript" >
									jQuery(function () {
										// Get the form.
										var form = jQuery('#ajax-contact');
										// Get the messages div.
										var formMessages = jQuery('#form-messages');
										
										// Set up an event listener for the contact form.
										jQuery(form).submit(function (e) {
											// Stop the browser from submitting the form.
											e.preventDefault();
											// Serialize the form data.
											jQuery('.loading-icons').append('<i class="fa fa-refresh fa-spin"></i>');
											var formData = jQuery(form).serialize() + '&action=triathlon_mail_script';
											var ajax_url = '<?php echo admin_url('admin-ajax.php'); ?>';
											// Submit the form using AJAX.
											jQuery.ajax({
												type: "POST",
												url: ajax_url,
												data: formData
											})
											.done(function (response) {
												jQuery('.loading-icons').find('i.fa-spin').remove();
												// Make sure that the formMessages div has the 'success' class.
												jQuery(formMessages).removeClass('error');
												jQuery(formMessages).addClass('success');
		
												// Set the message text.
												jQuery(formMessages).text(response);
		
												// Clear the form.
												jQuery('#name').val('');
												jQuery('#email').val('');
												jQuery('#message').val('');
											})
											.fail(function (data) {
												jQuery('.loading-icons').find('i.fa-spin').remove();
												// Make sure that the formMessages div has the 'error' class.
												jQuery(formMessages).removeClass('success');
												jQuery(formMessages).addClass('error');
		
												// Set the message text.
												if (data.responseText !== '') {
													jQuery(formMessages).text(data.responseText);
												} else {
													jQuery(formMessages).text('Oops! An error occured and your message could not be sent.');
												}
											});
		
										});
		
									});
		
								</script> 
						</div>
						<?php if (isset($image_on) && $image_on == 'yes' && $footer_image != '') { ?>
							<div class="col-sm-7 col-xs-12 pull-left competitor-img"> <img src="<?php echo esc_url($footer_image); ?>" alt="<?php esc_html_e('Contact Banner','triathlon');?>"> </div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<?php }?>
		<footer id="footer" class="haslayout">
			<?php if( isset( $enable_back ) && $enable_back == 'on' ) {?>
				<div class="container-fluid">
					<div class="row"> <a class="btn-backtotop" href="javascript:;"> <span>
						<?php esc_html_e('Back To Top','triathlon');?>
						</span> </a> </div>
				</div>
			<?php }?>
			<div class="container footer">
				<div class="row">
					<?php if( isset( $enable_newslatter ) && $enable_newslatter == 'on' ) {?>
					<?php if( isset( $newslatter_title ) && $newslatter_title !='' ) {?>
					<p><?php echo esc_attr( $newslatter_title ); ?></p>
					<?php }?>
					<?php 
						$mailchimp	= new Triathlon_MailChimp();
						$mailchimp->triathlon_mailchimp_form();
					?>
					<?php }?>
					<div class="bottom-strip">
						<?php if( isset( $copyright ) && $copyright !='' ) {?>
						<span class="copyright"><?php echo esc_attr( $copyright ); ?></span>
						<?php }?>
						<?php if( isset( $bootom_image['url'] ) && $bootom_image['url'] !='' ) {?>
						<span class="payment-card"> <img src="<?php echo esc_url( $bootom_image['url'] ); ?>" alt="<?php esc_html_e('Logo','triathlon');?>"> </span>
						<?php }?>
					</div>
				</div>
			</div>
		</footer>
		<!-- Footer End -->
		</div>
    <?php
	}
}