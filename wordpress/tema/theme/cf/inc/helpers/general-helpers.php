<?php
/**
 * Theme Helper Functions
 *
 * @package Triathlon
 */

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
if (!isset($content_width)) {
	$content_width = 640; /* pixels */
}


/**
 * @Theme Editor Style
 * 
 */
if (!function_exists('triathlon_add_editor_styles')) {
	function triathlon_add_editor_styles() {
		$theme_version = wp_get_theme();
		add_editor_style(  get_template_directory_uri() . '/core/css/triathlon-editor-style.css', array(), $theme_version->get( 'Version' ) );
	}
	add_action( 'admin_init', 'triathlon_add_editor_styles' );
}

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
if (!function_exists('triathlon_widgets_init')){
	function triathlon_widgets_init() {
	
		register_sidebar(array(
			'name' => esc_html__('Sidebar', 'triathlon'),
			'id' => 'sidebar-1',
			'description' => '',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		));
		register_sidebar( array(
			'name'          => esc_html__( 'Shop Sidebar', 'triathlon' ),
			'id'            => 'shop_sidebar',
			'description'   => '',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
	}

	add_action('widgets_init', 'triathlon_widgets_init');
}
/**
 * Enqueue scripts and styles.
 */
if (!function_exists('triathlon_scripts')){
	function triathlon_scripts() {
		wp_enqueue_script('jquery','jquery-ui-slide');

        $theme_info = wp_get_theme();
		
		//css
		wp_enqueue_style('bootstrap.min', get_template_directory_uri() . '/css/bootstrap.min.css', array(), $theme_info->get( 'Version' ));
		wp_enqueue_style('fontawesome', get_template_directory_uri() . '/css/font-awesome.css', array(), $theme_info->get( 'Version' ));
		wp_enqueue_style('icomoon', get_template_directory_uri() . '/css/icomoon.css', array(), $theme_info->get( 'Version' ));
		wp_enqueue_style('owl.carousel', get_template_directory_uri() . '/css/owl.carousel.css', array(), $theme_info->get( 'Version' ));
		wp_enqueue_style('triathlon_owl_theme_style', get_template_directory_uri() . '/css/owl.theme.css', array(), $theme_info->get( 'Version' ));
		wp_enqueue_style('prettyPhoto', get_template_directory_uri() . '/css/prettyPhoto.css', array(), $theme_info->get( 'Version' ));
		
		if (class_exists('woocommerce')) {
			wp_enqueue_style('triathlon_woocommerce_styling', get_template_directory_uri() . '/css/woocommerce.css');
		}
		
		wp_enqueue_style('triathlon_theme_style', get_stylesheet_directory_uri() . '/style.css', array(), $theme_info->get( 'Version' ));
		wp_enqueue_style('triathlon_typo_style', get_template_directory_uri() . '/css/typo.css', array(), $theme_info->get( 'Version' ));
		wp_enqueue_style('triathlon_responsive_style', get_template_directory_uri() . '/css/responsive.css', array(), $theme_info->get( 'Version' ));
		
	
		//script
		wp_enqueue_script('bootstrap.min', get_template_directory_uri() . '/js/bootstrap.min.js', array(), $theme_info->get( 'Version' ), true);
		wp_enqueue_script('triathlon_owl.owl.carousel', get_template_directory_uri() . '/js/owl.carousel.js', array(), $theme_info->get( 'Version' ), true);
		wp_enqueue_script('isotope', get_template_directory_uri() . '/js/jquery.isotope.min.js', array(), $theme_info->get( 'Version' ), true);
		wp_enqueue_script('triathlon_jcf', get_template_directory_uri() . '/js/jcf.js', array(), $theme_info->get( 'Version' ), true);
		wp_enqueue_script('jcf.select', get_template_directory_uri() . '/js/jcf.select.js', array(), $theme_info->get( 'Version' ), true);
		wp_enqueue_script('jcf.radio', get_template_directory_uri() . '/js/jcf.radio.js', array(), $theme_info->get( 'Version' ), true);
		wp_enqueue_script('jcf.checkbox', get_template_directory_uri() . '/js/jcf.checkbox.js', array(), $theme_info->get( 'Version' ), true);
		wp_enqueue_script('prettyPhoto', get_template_directory_uri() . '/js/jquery.prettyPhoto.js', array(), $theme_info->get( 'Version' ), true);
		
		wp_enqueue_script('triathlon_functions', get_template_directory_uri() . '/js/tg_functions.js', array(), $theme_info->get( 'Version' ), true);
		wp_enqueue_script('triathlon_modernizr_js', 'http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js', '', '', true);

		wp_localize_script( 'triathlon_functions', 'scripts_vars', array(
				'ajaxurl' => admin_url( 'admin-ajax.php' ),
		));

		
		
		$theme_skin	= '';
		if(function_exists('fw_get_db_settings_option')) {
			$theme_skin = fw_get_db_settings_option('theme_type');
		}
		
		if( isset( $theme_skin ) && $theme_skin == 'gym' ){
			wp_enqueue_style('triathlon_color_style', get_template_directory_uri() . '/css/gym_skin.css', array(), $theme_info->get( 'Version' ));
		} else{
			wp_enqueue_style('triathlon_color_style', get_template_directory_uri() . '/css/sports_skin.css', array(), $theme_info->get( 'Version' ));
		}
		
		if (is_singular() && comments_open() && get_option('thread_comments')) {
			wp_enqueue_script('comment-reply');
		}
		
		if(function_exists('fw_get_framework_directory_uri')) :
			if (!is_admin()) {
				wp_enqueue_script(
					'fw-form-helpers',
					fw_get_framework_directory_uri('/static/js/fw-form-helpers.js')
				);
			}
		endif;
	}
	
	add_action('wp_enqueue_scripts', 'triathlon_scripts');
}

/**
 * @Init Sharing Script
 * @return 
 */
if (!function_exists('triathlon_init_share_script')) {
    function triathlon_init_share_script() {
        wp_enqueue_script('triathlon_addthis', 'http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4e4412d954dccc64', '', '', true);
    }
}

if (!function_exists('triathlon_enque_map_library')) {
    function triathlon_enque_map_library() {
		 $theme_version = wp_get_theme();
		 
		 $protocol = is_ssl() ? "https" : "http";
		 $google_key = '';
		 if (function_exists('fw_get_db_settings_option')) {
			$google_key = fw_get_db_settings_option('google_key');
		 }

		if( !empty( $google_key ) ) {
        	wp_enqueue_script('jquery.googleapis' , $protocol.'://maps.googleapis.com/maps/api/js?key='.$google_key, '' , '' , true);
		} else{
			wp_enqueue_script('jquery.googleapis' , $protocol.'://maps.googleapis.com/maps/api/js?sensor=false&libraries=places' , '' , '' , true);
		}
		
        wp_enqueue_script('jquery.gmap.api3', get_template_directory_uri() . '/js/gmap3.min.js', '', '', true);
        
    }

}

/**
 * Enqueue scripts and styles.
 */
if (!function_exists('triathlon_admin_scripts')) {
    function triathlon_admin_scripts() {
        $theme_version = wp_get_theme();
        wp_enqueue_media();
        wp_enqueue_script('triathlon_admin_functions' , get_template_directory_uri() . '/core/js/triathlon_admin_functions.js' , array () , $theme_version->get('Version') , true);

        //Styles
        wp_enqueue_style('fontawesome' , get_template_directory_uri() . '/css/font-awesome.css' , array ('jquery') , $theme_version->get('Version'));
        wp_enqueue_style('triathlon_admin_style' , get_template_directory_uri() . '/core/css/admin_styles.css' , array () , $theme_version->get('Version'));
    }
    add_action('admin_enqueue_scripts' , 'triathlon_admin_scripts');
}

/**
 * Load Dynamic Styles for Theme
 */


if (!function_exists('triathlon_print_css')) {
	function triathlon_print_css(){
		require_once (get_template_directory()  . '/inc/theme-styling/dynamic-styles.php');
	}
	add_action('wp_head', 'triathlon_print_css');
}

/*Custom Fonts Icons*/

/*Pagination Code Start*/
if (!function_exists('triathlon_prepare_pagination')) {
	function triathlon_prepare_pagination($pages = '', $range = 4) {
		global $paged;
		
		$showitems = ($range * 2) + 1;
		
		if (empty($paged)) {
			$current_page = 1;
		}
		
		$current_page	=  $paged;
		
		if ($pages == '') {
			global $wp_query;
			$pages = $wp_query->max_num_pages;
			if (!$pages) {
				$pages = 1;
			}
		} else {
			$pages = ceil($pages / $range);
		}
		
		if (1 != $pages) {
			echo "<nav class='pagination-area'><ul class='tg-pagination'>";
			
			if ($current_page > 2 && $current_page > $range + 1 && $showitems < $pages) {
				//echo "<a href='" . get_pagenum_link(1) . "'>&laquo; First</a>";
			}
			
			if ( $current_page > 1 ) {
				echo "<li><a aria-label='Previous' href='" . get_pagenum_link($current_page - 1) . "'><span class='icon-arrow-left' aria-hidden='true'></span></a></li>";
			}
	
			for ($i = 1; $i <= $pages; $i++) {
				if (1 != $pages && (!($i >= $current_page + $range + 1 || $i <= $current_page - $range - 1) || $pages <= $showitems )) {
					echo ($paged == $i) ? "<li class=\"active\"><a href='javascript:;'>" . $i . "</a></li>" : "<li><a href='" . get_pagenum_link($i) . "' class=\"inactive\">" . $i . "</a></li>";
				}
			}
	
			if ($current_page < $pages ) {
				echo "<li><a aria-label='Next' href=\"" . get_pagenum_link($current_page + 1) . "\"><span class='icon-arrow-right-latest-races' aria-hidden='true'></span></a></li>";
			}
			
			if ($current_page < $pages - 1 && $current_page + $range - 1 < $pages && $showitems < $pages) {
				//echo "<a href='" . get_pagenum_link($pages) . "'>Last &raquo;</a>";
			}
			echo "</ul></nav>";
		}
	}
}


/**
 * @get post thumbnail
 * @return thumbnail url
 */
if (!function_exists('triathlon_prepare_thumbnail')) {
	function triathlon_prepare_thumbnail($post_id, $width, $height) {
		global $post;
		if (has_post_thumbnail()) {
			$thumb_id  = get_post_thumbnail_id($post_id);
			$thumb_url = wp_get_attachment_image_src($thumb_id, array($width, $height), true);
			if ($thumb_url[1] == $width and $thumb_url[2] == $height) {
				return $thumb_url[0];
			} else {
				$thumb_url = wp_get_attachment_image_src($thumb_id, "full", true);
				return $thumb_url[0];
			}
		}
	}
}

/**
 * @Hook Favicon
 * @return favicon
 */
if (!function_exists('triathlon_get_favicon')) {
	function triathlon_get_favicon() {
		
		if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
			if ( ! function_exists('fw_get_db_settings_option') ) {
				return;
			} else {
				$tg_favicaon	= fw_get_db_settings_option('favicon');
				if( isset( $tg_favicaon['url'] ) ) {
					echo '<link rel="shortcut icon" href="'.esc_url( $tg_favicaon['url'] ).'">';
				}
			}
		} else{
			triathlon_wp_favicon();
		}
		
	}
}

/**
 * @get Categories
 * @return categories
 */
if (!function_exists('triathlon_prepare_categories')) {
	function triathlon_prepare_categories( $tg_post_cat ) {             
		 global $post,$wpdb;                                 
		 if ( isset( $tg_post_cat ) && $tg_post_cat !='' && $tg_post_cat !='0' ){ 
			$tg_current_category = $wpdb->get_row($wpdb->prepare("SELECT * from $wpdb->terms WHERE slug = %s", $tg_post_cat ));
			echo '<a href="'.site_url().'?cat='.$tg_current_category->term_id.'">'.$tg_current_category->name.'</a>';
		 } else {
			  $before_cat = "";
			  echo get_the_term_list ( get_the_id(), 'category', $before_cat , ', ', '' );
		 }
	}
}

/**
 * @Favicon Fallback
 * @return favicon
 */
function triathlon_wp_favicon(){
	
	if ( ! has_site_icon() && ! is_customize_preview() ) {
		return;
	}
		
	$meta_tags = array(
        sprintf( '<link rel="icon" href="%s" sizes="32x32" />', esc_url( get_site_icon_url( 32 ) ) ),
        sprintf( '<link rel="icon" href="%s" sizes="192x192" />', esc_url( get_site_icon_url( 192 ) ) ),
        sprintf( '<link rel="apple-touch-icon-precomposed" href="%s">', esc_url( get_site_icon_url( 180 ) ) ),
        sprintf( '<meta name="msapplication-TileImage" content="%s">', esc_url( get_site_icon_url( 270 ) ) ),
    );
 
    /**
     * Filter the site icon meta tags, so Plugins can add their own.
     *
     * @since 4.3.0
     *
     * @param array $meta_tags Site Icon meta elements.
     */
    $meta_tags = apply_filters( 'site_icon_meta_tags', $meta_tags );
    $meta_tags = array_filter( $meta_tags );
 
    foreach ( $meta_tags as $meta_tag ) {
        echo "$meta_tag\n";
    }
}

/**
 * @get next post
 * @return link
 */
if (!function_exists('triathlon_next_post')) {

    function triathlon_next_post($format) {
        $format = str_replace('href=', 'class="pix-nextpost" href=', $format);
        return $format;
    }

    add_filter('next_post_link', 'triathlon_next_post');
}

/**
 * @get previous post
 * @return link
 */
if (!function_exists('triathlon_previous')) {

    function triathlon_previous($format) {
        $format = str_replace('href=', 'class="pix-prevpost" href=', $format);
        return $format;
    }

    add_filter('previous_post_link', 'triathlon_previous');
}

/**
 * @get custom Excerpt
 * @return link
 */
if (!function_exists('triathlon_prepare_custom_excerpt')) {
	function triathlon_prepare_custom_excerpt($more = '...') {
		return '....';
	}
	
	add_filter('excerpt_more', 'triathlon_prepare_custom_excerpt');
}

/**
 * @get Excerpt
 * @return link
 */
if (!function_exists('triathlon_prepare_excerpt')) {

    function triathlon_prepare_excerpt($charlength = '255', $more = 'true', $text = 'Read More') {
        global $post;
        $excerpt = trim(preg_replace('/<a[^>]*>(.*)<\/a>/iU', '', get_the_excerpt()));
        if (strlen($excerpt) > $charlength) {
            if ($charlength > 0) {
                $excerpt = substr($excerpt, 0, $charlength);
            } else {
                $excerpt = $excerpt;
            }
            if ($more == 'true') {
                $link = '<a href="' . esc_url(get_permalink()) . '" class="btn-theme black">
							<span class="txt">' . esc_attr($text) . '</span>
							<span class="round">
								<i class="icon-arrow-right-latest-races"></i>
							</span>
						</a>';
            } else {
                $link = '...';
            }
            echo force_balance_tags( $excerpt . $link );
        } else {
            echo force_balance_tags( $excerpt );
        }
    }
}

/**
 * @Esc Data
 * @return categories
 */
 function triathlon_esc_specialchars($data=''){
 	return $data;
 }
 
/**
 * @Prepare social sharing links
 * @return sizes
 */
if (!function_exists('triathlon_prepare_social_sharing')) {

    function triathlon_prepare_social_sharing( $default_icon = 'false', $title = 'true') {
        triathlon_init_share_script();
		$facebook	= esc_html__('Facebook','triathlon');
		$twitter	= esc_html__('Twitter','triathlon');
		$email		= esc_html__('E-mail','triathlon');
		$tumblr2		= esc_html__('Tumblr','triathlon');
		$dribbble2		= esc_html__('Dribbble','triathlon');
		$instagram		= esc_html__('Instagram','triathlon');
		$youtube		= esc_html__('Youtube','triathlon');
		
		$social_facebook  = fw_get_db_settings_option('social_facebook');
		$social_twitter   = fw_get_db_settings_option('social_twitter');
		$social_email	  = fw_get_db_settings_option('social_email');
		$social_tumbler   = fw_get_db_settings_option('social_tumbler');
		$social_dribble   = fw_get_db_settings_option('social_dribble');
		$social_instagram = fw_get_db_settings_option('social_instagram');
		$social_youtube   = fw_get_db_settings_option('social_youtube');
		
		$output = '';
		$output .='<div class="share-button">';
			if( isset( $social_facebook ) && $social_facebook == 'enable' ){
				$output .='<a class="addthis_button_facebook" data-original-title="Facebook">'.$facebook.'</a>';
			}
			if( isset( $social_twitter ) && $social_twitter == 'enable' ){
				$output .='<a class="addthis_button_twitter" data-original-title="twitter">'.$twitter.'</a>';
			}
			if( isset( $social_email ) && $social_email == 'enable' ){
				$output .='<a class="addthis_button_google" data-original-title="google-plus">'.$email.'</a>';
			}
			if( isset( $social_tumbler ) && $social_tumbler == 'enable' ){
				$output .='<a class="addthis_button_tumblr" data-original-title="Tumblr">'.$tumblr2.'</a>';
			}
			if( isset( $social_dribble ) && $social_dribble == 'enable' ){
				$output .='<a class="addthis_button_dribbble" data-original-title="Dribbble">'.$dribbble2.'</a>';
			}
			if( isset( $social_instagram ) && $social_instagram == 'enable' ){
				$output .='<a class="addthis_button_instagram" data-original-title="Instagram">'.$instagram.'</a>';
			}
			if( isset( $social_youtube ) && $social_youtube == 'enable' ){
				$output .='<a class="addthis_button_youtube" data-original-title="Youtube">'.$youtube.'</a>';
			}
		$output .='</div>';
	echo balanceTags($output, true);
    }

}

/**
 * @Add User meta fields
 * @return []
 */
if ( ! function_exists( 'triathlon_prepare_custom_meta' ) ) {
	
	function triathlon_prepare_custom_meta( $userid ) {
		$userfields['designation']		= 'Designation';	
		return $userfields;
	}
}

/**
 * @User Public Profile Save
 * @return {}
 */
if (!function_exists('triathlon_get_post_name')) {

    function triathlon_get_post_name() {
        global $post;
        if (isset($post)) {
            $post_name = $post->post_name;
        } else {
            $post_name = '';
        }
        return $post_name;
    }

}

/**
 * @Add Images Sizes
 * @return sizes
 */
 //Blog Large, blog listing
add_image_size('triathlon_media_size_1', 1140, 289, true);
//Blog 2 column
add_image_size('triathlon_media_size_2', 430, 200, true);
//Blog 3 column
add_image_size('triathlon_media_size_3', 370, 175, true);
//News Events
add_image_size('triathlon_media_size_4', 320, 320, true);
//Shop listing
add_image_size('triathlon_media_size_5', 180, 119, true);

/**
 * @Shop Images Dimentions
 * @return {}
 */
add_action("after_switch_theme", "triathlon_woocommerce_image_settings", 10 , 2);
function triathlon_woocommerce_image_settings() {
	update_option('shop_catalog_image_size', array('width'=>150, 'height'=>150, 'crop' => false) );
	update_option('shop_single_image_size', array('width'=>405, 'height'=>420, 'crop' => true) );
	update_option('shop_thumbnail_image_size', array('width'=>80, 'height'=>80, 'crop' => true) );
}