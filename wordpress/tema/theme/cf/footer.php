<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Triathlon
 */
?>
<?php do_action('triathlong_do_init_footers');?>
<?php wp_footer(); ?>
</body></html>