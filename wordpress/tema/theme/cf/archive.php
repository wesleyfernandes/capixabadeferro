<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Triathlon
 */
get_header();
?>

<div class="container">
    <div class="row">
        <div class="tg-inner-content">
            <div class="col-md-9">
			<?php
            global $paged;
			$tg_get_excerpt	= get_option('rss_use_excerpt');
            if (is_author()) {
                global $author;
                $userdata = get_userdata($author);
            }
            if (category_description() || is_tag() || (is_author() && isset($userdata->description) && !empty($userdata->description))) {
                echo '<div class="widget orgnizer">';
                if (is_author()) {
                    ?>
                    <figure>
                        <a><?php echo get_avatar($userdata->user_email, 70); ?></a>
                    </figure>
                    <div class="left-sp">
                        <h5><a><?php echo esc_attr($userdata->display_name); ?></a></h5>
                        <p><?php echo balanceTags($userdata->description, true); ?></p>
                    </div>
                    <?php
                } elseif (is_category()) {
                    $category_description = category_description();
                    if (!empty($category_description)) {
                        ?>
                        <div class="left-sp">
                            <p><?php echo category_description(); ?></p>
                        </div>
                    <?php } ?>
                    <?php
                } elseif (is_tag()) {
                    $tag_description = tag_description();
                    if (!empty($tag_description)) {
                        ?>
                        <div class="left-sp">
                            <p><?php echo apply_filters('tag_archive_meta', $tag_description); ?></p>
                        </div>
                        <?php
                    }
                }
                echo '</div>';
            }

            if (empty($paged)) {
                $paged = 1;
            }

            if (!isset($_GET["s"])) {
                $_GET["s"] = '';
            }

            $taxonomy = 'category';
            $taxonomy_tag = 'post_tag';
            $args_cat = array();

            if (is_author()) {
                $args_cat = array('author' => $wp_query->query_vars['author']);
                $post_type = array('post');
            } elseif (is_date()) {
                if (is_month() || is_year() || is_day() || is_time()) {
                    $args_cat = array('m' => $wp_query->query_vars['m'], 'year' => $wp_query->query_vars['year'], 'day' => $wp_query->query_vars['day'], 'hour' => $wp_query->query_vars['hour'], 'minute' => $wp_query->query_vars['minute'], 'second' => $wp_query->query_vars['second']);
                }
                $post_type = array('post');
            } else if ((isset($wp_query->query_vars['taxonomy']) && !empty($wp_query->query_vars['taxonomy']))) {
                $taxonomy = $wp_query->query_vars['taxonomy'];
                $taxonomy_category = '';
                $taxonomy_category = $wp_query->query_vars[$taxonomy];

                $taxonomy = 'category';
                $args_cat = array();
                $post_type = 'post';
            } else if (is_category()) {
                $taxonomy = 'category';
                $args_cat = array();
                $category_blog = $wp_query->query_vars['cat'];
                $post_type = 'post';
                $args_cat = array('cat' => "$category_blog");
            } else if (is_tag()) {
                $taxonomy = 'category';
                $args_cat = array();
                $tag_blog = $wp_query->query_vars['tag'];
                $post_type = 'post';
                $args_cat = array('tag' => "$tag_blog");
            } else {
                $taxonomy = 'category';
                $args_cat = array();
                $post_type = 'post';
            }
            $args = array(
                'post_type' => $post_type,
                'paged' => $paged,
                'post_status' => 'publish',
                'order' => 'ASC',
            );
            ?>

           
                <?php
                $args = array_merge($args_cat, $args);
                $custom_query = new WP_Query($args);
                if ($custom_query->have_posts()):
                    while ($custom_query->have_posts()) : $custom_query->the_post();
                        $width = '300';
                        $height = '169';
                        $title_limit = 1000;
                        $thumbnail = triathlon_prepare_thumbnail($post->ID, $width, $height);
                        ?> 

                        <article id="post-<?php the_ID(); ?>" class="tg-landing-page">

                            <?php
                            if (isset($thumbnail) && $thumbnail != '') {
                                ?>
                                <div class="tg-thumbnail">
                                    <figure><a href="<?php esc_url(the_permalink()); ?>"><img src="<?php echo esc_url($thumbnail); ?>" alt="<?php echo esc_attr(get_the_title());?>"></a></figure>
                                </div> <?php
                            }
                            ?>
                            <h2><a href="<?php esc_url(the_permalink()); ?>"><?php the_title(); ?></a></h2>
                            <div class="tg-archive-meta">
							<?php 
								if( is_sticky() && !is_singular() ) :
									echo '<span class="sticky-post-wrap"><span class="sticky-txt"><i class="fa fa-bolt"></i>' . esc_html__("Featured", 'triathlon') .'</span></span>';
								endif;
							?>
							<?php echo get_the_term_list($post->ID, 'category', '<span class="tg-cats"><i class="fa fa-folder-open"></i>', ', ', '</span>'); ?>
							<?php the_tags( '<span class="tg-tags"><i class="fa fa-tags"></i>', ', ', '</span>' ); ?>
							</div>
                            <div class="post-description">
                                 <p><?php echo triathlon_prepare_excerpt(255, 'false', 'Read More'); ?></p> 
								 <a href="<?php esc_url(the_permalink()); ?>" class="btn-theme black archive-readmore">
									<span class="txt"><?php esc_html_e('Read more','triathlon');?></span>
									<span class="round">
										<i class="icon-arrow-right-latest-races"></i>
									</span>
								</a>
                            </div>
                        </article>

                        <?php
                    endwhile;
                else:
                    esc_html_e('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'triathlon');
                endif;

                $qrystr = '';
                if ($wp_query->found_posts > get_option('posts_per_page')) {
                    if (function_exists('triathlon_prepare_pagination')) {
                        echo triathlon_prepare_pagination(wp_count_posts()->publish, get_option('posts_per_page'));
                    }
                }
                ?>
            </div>
            <div class="col-md-3 tg-widgets">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
    <?php get_footer(); ?>
