"use strict";
jQuery(document).ready(function(e) {

    jQuery(document).ready(function($) {
        jQuery('.accordion_link').on('click', '.add_more_accordion', function(event) {
            //alert('asd');
            var widget = jQuery(this).data('widget');
            var $html = '';
            $html = '<p><label for="">Heading</label><input type="text" id="heading" name="widget-tg_accordion[' + widget + '][heading][]" value="" class="widefat" /></p> <p><label for="description">Description</label><textarea id="description"  rows="8" cols="10" name="widget-tg_accordion[' + widget + '][description][]" class="widefat"></textarea></p>';

            jQuery('.accordion_html').append($html);
        });
    });

    /* -------------------------------------
     Upload Avatar
     -------------------------------------- */
    jQuery('#upload-user-avatar').on('click', function() {
        "use strict";
        var $ = jQuery;
        var $this = jQuery(this);
        var custom_uploader = wp.media({
            title: 'Select File',
            button: {
                text: 'Add File'
            },
            multiple: false
        })
                .on('select', function() {
                    var attachment = custom_uploader.state().get('selection').first().toJSON();
                    jQuery('#user_avatar_display').val(attachment.url);
                    jQuery('#avatar-src').attr('src', attachment.url);
                    jQuery('#avatar-wrap').show();
                    $this.parents('tr').next('tr').find('.backgroud-image').show();
                    $this.parents('tr').next('tr').attr('class', '');
                }).open();

    });

    jQuery(document).on('click', '.delete-auhtor-media', function(e) {
        jQuery(this).parents('.backgroud-image').find('img').attr('src', '');
        jQuery(this).parents('tr').prev('tr').find('.media-image').val('');
        jQuery(this).parents('.backgroud-image').hide();
    });

    /* -------------------------------------
     Get Currency Symbols
     -------------------------------------- */
    jQuery(document).on('change', '.currency_symbol', function() {
        var code = jQuery(this).val();
        var dataString = 'code=' + code + '&action=charity_get_currency_symbol';
        var $this = jQuery(this);
        jQuery($this).parent('.fw-inner-option').append("<i class='fa fa-refresh fa-spin'></i>");
        jQuery.ajax({
            type: "POST",
            url: ajaxurl,
            data: dataString,
            success: function(response) {
                jQuery("#fw-option-currency_sign").val(response);
                jQuery($this).parent('.fw-inner-option').find('i').remove();
            }
        });

        return false;
    });
});