<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'bd_capixabadeferro' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'root' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'upLKoyY5{a2;Z*H^1D=7?!WD#0knW7|&](>9*i]qhi&OO$p3i3KKa(GGs7T9I(U=' );
define( 'SECURE_AUTH_KEY',  'Dca+VKH=%-kGAg*aldIZ.k_vFMP8UeDDH4u%mH3F,tX1i!p+,+h(MRF`IO`2g^pz' );
define( 'LOGGED_IN_KEY',    '|]YS8vF9B|LV282ZV/kCaUBm)&QvZeGlUq!L&,FE/C>B>1:PZavt-Ia,KL40zVTJ' );
define( 'NONCE_KEY',        '#]Pd%T,B.p&%_((LInDaFxDZ~JCto!1A@@Yy}*3)_WcPlq:-s?d+Z,fO:+HKstcB' );
define( 'AUTH_SALT',        '#-vb9y?[$QWXreVxd-;~y Pn@)Q[k{+r0_H8[`CLqG?&mb:;XM7bmTKa:Ee1T+>0' );
define( 'SECURE_AUTH_SALT', 'V2$2T428Nu~t3_A=|JUs[G<AO$|%O1pfvp^MO+Z HeU1E,RBlP#M[2uu:jT=>c ;' );
define( 'LOGGED_IN_SALT',   'kU&``A^j%]ijg*DoAaIKTJj{i||G/Ly5tfp+.Qe#r I!|R;2Pv/2DH[=XZU~R&M.' );
define( 'NONCE_SALT',       'mXQyc(rBHH^q6vVZ5`UX)x63-}).Go&80_v+u7AHqElatd;Z>6JUA?HJkP^7tded' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'cf_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

define('ALLOW_UNFILTERED_UPLOADS', true);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
